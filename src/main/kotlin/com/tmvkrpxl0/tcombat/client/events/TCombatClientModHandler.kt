package com.tmvkrpxl0.tcombat.client.events

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.client.renderers.GroundChunkRenderer
import com.tmvkrpxl0.tcombat.base.common.util.getCapabilityMust
import com.tmvkrpxl0.tcombat.client.StructureModelLoader
import com.tmvkrpxl0.tcombat.client.key.InGameOrInventory
import com.tmvkrpxl0.tcombat.client.key.KEYBINDINGS
import com.tmvkrpxl0.tcombat.client.key.MODIFIED_IN_GAME_OR_GUI
import com.tmvkrpxl0.tcombat.client.models.TNTArrowModel
import com.tmvkrpxl0.tcombat.client.renderers.ReflectiveArrowRenderer
import com.tmvkrpxl0.tcombat.client.renderers.SnipeArrowRenderer
import com.tmvkrpxl0.tcombat.client.renderers.TNTArrowRenderer
import com.tmvkrpxl0.tcombat.client.renderers.WorldAxeRenderer
import com.tmvkrpxl0.tcombat.common.capabilities.ENTITY_HOLDER
import com.tmvkrpxl0.tcombat.common.entities.TCombatEntityTypes
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.MOBILE_DISPENSER
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.WORLD_AXE
import com.tmvkrpxl0.tcombat.common.items.misc.MobileDispenserItem
import net.minecraft.client.color.item.ItemColor
import net.minecraft.client.renderer.entity.EntityRendererProvider
import net.minecraft.client.renderer.item.ItemProperties
import net.minecraft.resources.ResourceLocation
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.Mod
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent
import net.neoforged.neoforge.client.event.EntityRenderersEvent
import net.neoforged.neoforge.client.event.ModelEvent
import net.neoforged.neoforge.client.event.RegisterColorHandlersEvent
import net.neoforged.neoforge.client.event.RegisterKeyMappingsEvent


@Mod.EventBusSubscriber(modid = MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = [Dist.CLIENT])
object TCombatClientModHandler {

    @SubscribeEvent
    fun registerKeys(event: RegisterKeyMappingsEvent) {
        KEYBINDINGS.forEach { event.register(it) }
    }

    @SubscribeEvent
    fun onClientSetup(event: FMLClientSetupEvent) {
        event.enqueueWork {
            MODIFIED_IN_GAME_OR_GUI.forEach { modified ->
                modified.keyConflictContext = InGameOrInventory(modified.keyConflictContext)
            } //...Probably it would be better to reassign keymappings in main thread

            ItemProperties.register(
                WORLD_AXE, ResourceLocation(MODID, "cast")
            ) { itemStack, _, _, _ ->
                val holder = itemStack.getCapabilityMust(ENTITY_HOLDER)
                return@register if (holder.entity != null) 1.0F else 0.0F
            }

            ItemProperties.register(
                MOBILE_DISPENSER, ResourceLocation(MODID, "mb_firing")
            ) { _, _, livingEntity, _ ->
                if (livingEntity == null) return@register 0F
                val isFiring = livingEntity.isUsingItem && livingEntity.getItemInHand(livingEntity.usedItemHand).item is MobileDispenserItem
                return@register if (isFiring) 1F else 0F
            }

            ItemProperties.register(
                MOBILE_DISPENSER, ResourceLocation(MODID, "mb_front")
            ) { _, _, livingEntity, _ ->
                if (livingEntity == null) return@register 0F
                val isFiring = livingEntity.isUsingItem && livingEntity.getItemInHand(livingEntity.usedItemHand).item is MobileDispenserItem
                val time = livingEntity.level().gameTime
                val frontTorch = isFiring && time % 2 == 0L && time % 16 <= 7
                return@register if (frontTorch) 1F else 0F
            }

            ItemProperties.register(
                MOBILE_DISPENSER, ResourceLocation(MODID, "mb_back")
            ) { _, _, livingEntity, _ ->
                if (livingEntity == null) return@register 0F
                val isFiring = livingEntity.isUsingItem && livingEntity.getItemInHand(livingEntity.usedItemHand).item is MobileDispenserItem
                val time = livingEntity.level().gameTime
                val backTorch = isFiring && time % 2 == 0L && time % 16 > 7
                return@register if (backTorch) 1F else 0F
            }
        }
    }

    @SubscribeEvent
    fun onModelRegister(event: ModelEvent.RegisterAdditional) {
        event.register(WorldAxeRenderer.AXE)
    }

    @SubscribeEvent
    fun onRendererRegister(event: EntityRenderersEvent.RegisterRenderers) {
        event.registerEntityRenderer(TCombatEntityTypes.GROUND_CHUNK) { context: EntityRendererProvider.Context ->
            GroundChunkRenderer(context)
        }
        event.registerEntityRenderer(TCombatEntityTypes.tntArrow) { contextIn: EntityRendererProvider.Context ->
            TNTArrowRenderer(contextIn)
        }
        event.registerEntityRenderer(TCombatEntityTypes.snipeArrow) { context: EntityRendererProvider.Context ->
            SnipeArrowRenderer(context)
        }/*        event.registerEntityRenderer(TCombatEntityTypes.CUSTOMIZABLE_FLUID_ENTITY.get()) { context: EntityRendererProvider.Context ->
            FluidEntityRenderer(context)
        }*/
        event.registerEntityRenderer(TCombatEntityTypes.worldAxe) { context: EntityRendererProvider.Context ->
            WorldAxeRenderer(context)
        }
        event.registerEntityRenderer(TCombatEntityTypes.reflectiveArrow) { context: EntityRendererProvider.Context ->
            ReflectiveArrowRenderer(context)
        }
    }

    @SubscribeEvent
    fun onRegisterLayerDefinition(event: EntityRenderersEvent.RegisterLayerDefinitions) {
        event.registerLayerDefinition(TNTArrowModel.LAYER_LOCATION) { TNTArrowModel.createBodyLayer() }
    }

    @SubscribeEvent
    fun onRegisterModelLoaders(event: ModelEvent.RegisterGeometryLoaders) {
        event.register("structure", StructureModelLoader)
    }

    @SubscribeEvent
    fun onColorRegister(event: RegisterColorHandlersEvent.Item) {
        val itemColor =
            ItemColor { _, tintIndex -> if (tintIndex == 1) 16724480 else 4980736 } //16724480 is 15 power redstone color, the other is that of 0 power redstone
        event.register(itemColor, MOBILE_DISPENSER)
    }
}