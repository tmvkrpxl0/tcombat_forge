package com.tmvkrpxl0.tcombat.client.events

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.network.packets.RequestAutoExecutePacket
import com.tmvkrpxl0.tcombat.base.common.skill.SKILL_REGISTRY
import com.tmvkrpxl0.tcombat.base.common.skill.autoEnable
import com.tmvkrpxl0.tcombat.base.common.skill.startAutoExecuting
import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import com.tmvkrpxl0.tcombat.client.fadeLevel
import com.tmvkrpxl0.tcombat.client.isFading
import com.tmvkrpxl0.tcombat.client.key.MODIFIED_IN_GAME_OR_GUI
import com.tmvkrpxl0.tcombat.common.skill.TCombatCustomSkills.ARROW_SENSE
import com.tmvkrpxl0.tcombat.common.skill.TCombatCustomSkills.AUTO_BRIDGE
import com.tmvkrpxl0.tcombat.mixins.client.onRender
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.screens.inventory.InventoryScreen
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.Mod
import net.neoforged.neoforge.client.event.ScreenEvent
import net.neoforged.neoforge.event.TickEvent
import net.neoforged.neoforge.event.entity.player.PlayerEvent

@Mod.EventBusSubscriber(modid = MODID, value = [Dist.CLIENT], bus = Mod.EventBusSubscriber.Bus.FORGE)
object TCombatClientForgeHandler {

    @SubscribeEvent
    fun onClientTick(event: TickEvent.ClientTickEvent) {
        if (event.phase == TickEvent.Phase.START) {
            val minecraft = Minecraft.getInstance()
            minecraft.level ?: return
            val options = minecraft.options
            val isMoving = MODIFIED_IN_GAME_OR_GUI.any { kb -> kb != options.keySprint && kb.isDown }
            isFading = isMoving
            if (isMoving) {
                if (fadeLevel < 10) fadeLevel++
            } else {
                if (fadeLevel > 0) fadeLevel--
            }
        }
    }

    @SubscribeEvent
    fun handleAutoEnable(event: PlayerEvent.PlayerLoggedInEvent) {
        require(event.entity.level().isServerSide)

        SKILL_REGISTRY.forEach { skill -> if (skill.autoEnable) event.entity startAutoExecuting skill }
        RequestAutoExecutePacket(AUTO_BRIDGE, true)
        RequestAutoExecutePacket(ARROW_SENSE, true)
    }

    @SubscribeEvent
    fun beforeDrawingScreen(event: ScreenEvent.BackgroundRendered) {
        if (event.screen !is InventoryScreen) return
        onRender()
    }

    @SubscribeEvent
    fun onKeyPressedWithScreen(event: ScreenEvent.KeyPressed.Pre) {
        MODIFIED_IN_GAME_OR_GUI.find { it.key.value == event.keyCode }?.let {
            it.isDown = true
        }
    }

    @SubscribeEvent
    fun onKeyReleasedWithScreen(event: ScreenEvent.KeyReleased.Pre) {
        MODIFIED_IN_GAME_OR_GUI.find { it.key.value == event.keyCode }?.let {
            it.isDown = false
        }
    }
}