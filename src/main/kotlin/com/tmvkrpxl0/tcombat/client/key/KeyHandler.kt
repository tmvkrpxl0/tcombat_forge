package com.tmvkrpxl0.tcombat.client.key

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.capabilities.IPlayerTargetStatus
import com.tmvkrpxl0.tcombat.base.common.network.packets.RequestSkillExecutionPacket
import com.tmvkrpxl0.tcombat.base.common.network.packets.TargetRequestPacket
import com.tmvkrpxl0.tcombat.base.common.skill.cooldownOf
import com.tmvkrpxl0.tcombat.client.debug.TCombatDebugScreen
import com.tmvkrpxl0.tcombat.common.skill.TCombatCustomSkills.REFLECTION_BLAST
import com.tmvkrpxl0.tcombat.common.skill.TCombatCustomSkills.SHATTER_ARROW
import com.tmvkrpxl0.tcombat.common.skill.impl.ShatterArrow
import com.tmvkrpxl0.tcombat.mixins.bindings.common.ArrowAccessor
import net.minecraft.client.Minecraft
import net.minecraft.network.chat.Component
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.phys.AABB
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.Mod.EventBusSubscriber
import net.neoforged.neoforge.event.TickEvent
import net.neoforged.neoforge.event.TickEvent.ClientTickEvent

@EventBusSubscriber(Dist.CLIENT, modid = MODID)
object KeyHandler {
    private var requestType = IPlayerTargetStatus.RequestType.NEXT_PICK_MODE

    @SubscribeEvent
    fun onTick(event: ClientTickEvent) {
        if (event.phase != TickEvent.Phase.START) return
        val player = Minecraft.getInstance().player ?: return
        if (KB_SHATTER_ARROW.consumeClick()) {
            if (player cooldownOf SHATTER_ARROW > 0) return

            val level = player.clientLevel

            val entities = level.getEntities(
                player, AABB(-15.0, -15.0, -15.0, 15.0, 15.0, 15.0).move(player.position())
            ).asSequence()
                .filterIsInstance<AbstractArrow>()
                .filter { it.distanceToSqr(player) <= (5 * 5) }
                .filter { !(it as ArrowAccessor).isInGround || it.isPickable }
                .toList()

            if (entities.isEmpty()) {
                player.playNotifySound(SoundEvents.PLAYER_ATTACK_SWEEP, SoundSource.MASTER, 0.2f, 1f)
                player.sendSystemMessage(Component.translatable("${ShatterArrow.translationKey}.fail"))
            }

            entities.forEach { arrow ->
                level.playLocalSound(
                    arrow.x, arrow.y, arrow.z,
                    SoundEvents.SHIELD_BLOCK, SoundSource.PLAYERS,
                    0.8F, 0.8F + level.random.nextFloat() * 0.4F,
                    false
                )
            }
            RequestSkillExecutionPacket(SHATTER_ARROW).sendToServer()
        }
        if (KB_SET_TARGETS.consumeClick()) {
            TargetRequestPacket(requestType).sendToServer()
        }
        if (KB_SET_TARGET_MODE.consumeClick()) {
            var ordinal = requestType.ordinal + 1
            ordinal %= (IPlayerTargetStatus.RequestType.entries.size - 1)
            requestType = IPlayerTargetStatus.RequestType.entries.toTypedArray()[ordinal]
            player.sendSystemMessage(Component.nullToEmpty(requestType.name))
        }
        if (KB_REFLECTION_BLAST.consumeClick()) {
            RequestSkillExecutionPacket(REFLECTION_BLAST).sendToServer()
        }
        if (KB_TEST.consumeClick()) {
            Minecraft.getInstance().setScreen(TCombatDebugScreen())
        }
    }
}