package com.tmvkrpxl0.tcombat.client.debug

import com.tmvkrpxl0.tcombat.base.common.network.packets.RequestAutoExecutePacket
import com.tmvkrpxl0.tcombat.base.common.network.packets.RequestSkillExecutionPacket
import com.tmvkrpxl0.tcombat.base.common.skill.isUsingAuto
import com.tmvkrpxl0.tcombat.base.common.util.toComponent
import com.tmvkrpxl0.tcombat.common.skill.TCombatCustomSkills
import com.tmvkrpxl0.tcombat.common.skill.TCombatCustomSkills.ARROW_SENSE
import net.minecraft.client.gui.GuiGraphics
import net.minecraft.client.gui.components.Button
import net.minecraft.client.gui.screens.Screen
import net.minecraft.network.chat.Component

class TCombatDebugScreen : Screen("tcombat".toComponent()) {
    var shouldPause = false

    override fun isPauseScreen(): Boolean = shouldPause

    override fun render(guiGraphics: GuiGraphics, mouseX: Int, mouseY: Int, partialTicks: Float) {
        val player = minecraft?.player ?: return

        val pause = Button.builder(Component.literal(if (shouldPause) "Pausing..." else "Unpaused")) {
            shouldPause = !shouldPause
        }.build()

        val arrowSense = Button.builder("Toggle Arrow sense".toComponent()) {
            RequestAutoExecutePacket(ARROW_SENSE, !(player isUsingAuto ARROW_SENSE)).sendToServer()
        }.build()

        val send = Button.builder("Send test skill".toComponent()) {
            RequestSkillExecutionPacket(TCombatCustomSkills.TEST_SKILL).sendToServer()
        }.build()


        arrayOf(pause, arrowSense, send).forEach { button ->
            button.render(guiGraphics, mouseX, mouseY, partialTicks)
        }
    }
}