package com.tmvkrpxl0.tcombat.client.renderers

import com.mojang.blaze3d.vertex.PoseStack
import com.mojang.blaze3d.vertex.VertexConsumer
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.util.quaternion
import com.tmvkrpxl0.tcombat.base.common.util.toRadian
import com.tmvkrpxl0.tcombat.common.entities.projectile.WorldAxe
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.WORLD_AXE
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.MultiBufferSource
import net.minecraft.client.renderer.RenderType
import net.minecraft.client.renderer.entity.EntityRenderer
import net.minecraft.client.renderer.entity.EntityRendererProvider
import net.minecraft.client.renderer.texture.OverlayTexture
import net.minecraft.client.resources.model.BakedModel
import net.minecraft.resources.ResourceLocation
import net.minecraft.util.Mth
import net.minecraft.world.entity.HumanoidArm
import net.minecraft.world.inventory.InventoryMenu
import net.minecraft.world.item.ItemDisplayContext
import net.minecraft.world.item.ItemStack
import org.joml.Vector3f
import kotlin.math.PI
import kotlin.math.sqrt

private val forwards = Vector3f(0.0f, 0.0f, 1.0f)
private val up = Vector3f(0.0f, 1.0f, 0.0f)

class WorldAxeRenderer(context: EntityRendererProvider.Context) : EntityRenderer<WorldAxe>(context) {
    companion object {
        val AXE = ResourceLocation(MODID, "item/world_axe_axe")
    }

    override fun getTextureLocation(entity: WorldAxe): ResourceLocation = InventoryMenu.BLOCK_ATLAS

    override fun render(
        worldAxe: WorldAxe, yaw: Float, partialTicks: Float, poseStack: PoseStack, bufferSource: MultiBufferSource, packedLightIn: Int
    ) {
        val lookAngle = worldAxe.lookAngle
        val lookFloat = Vector3f(lookAngle.x.toFloat(), lookAngle.y.toFloat(), lookAngle.z.toFloat())
        val itemStack = worldAxe.baseAxe
        val owner = worldAxe.owner
        val itemRenderer = Minecraft.getInstance().itemRenderer ///////////////////////Render axe start
        poseStack.pushPose()
        poseStack.pushPose()

        poseStack.mulPose(lookFloat quaternion worldAxe.getzRot().toRadian())
        poseStack.mulPose(up quaternion (-90 - worldAxe.yRot).toRadian())
        poseStack.mulPose(forwards quaternion (-PI/4).toFloat())
        poseStack.translate(0.275, 0.125, 0.0)
        poseStack.scale(1.5f, 1.5f, 1.5f)

        val bakedModel: BakedModel = Minecraft.getInstance().modelManager.getModel(AXE)


        itemRenderer.render(
            itemStack, ItemDisplayContext.GROUND, false, poseStack, bufferSource, packedLightIn, OverlayTexture.NO_OVERLAY, bakedModel
        )
        poseStack.popPose() ///////////////////////Render axe stop


        var i = if (owner.mainArm == HumanoidArm.RIGHT) 1 else -1
        val mainHand: ItemStack = owner.mainHandItem
        if (mainHand.item != WORLD_AXE) {
            i = -i
        }

        val animaDuration: Float = owner.getAttackAnim(partialTicks)
        val animaRadian = Mth.sin(Mth.sqrt(animaDuration) * Math.PI.toFloat())
        val bodyRadian = Mth.lerp(partialTicks, owner.yBodyRotO, owner.yBodyRot) * (Math.PI.toFloat() / 180f)
        val animaX = Mth.sin(bodyRadian).toDouble()
        val animaY = Mth.cos(bodyRadian).toDouble()
        val animaZ = i.toDouble() * 0.35
        val rodX: Double
        val rodY: Double
        val rodZ: Double
        val yOffset: Float
        if ((entityRenderDispatcher.options.cameraType.isFirstPerson) && owner === Minecraft.getInstance().player) {
            val d7 = 960.0 / entityRenderDispatcher.options.fov().get()
            var vec3 = entityRenderDispatcher.camera.nearPlane.getPointOnPlane(i.toFloat() * 1, -0.333F)
            vec3 = vec3.scale(d7)
            vec3 = vec3.yRot(animaRadian * 0.5f)
            vec3 = vec3.xRot(-animaRadian * 0.7f)
            val down = (1F - owner.getAttackStrengthScale(partialTicks))
            rodX = Mth.lerp(partialTicks.toDouble(), owner.xo, owner.x) + vec3.x + (sqrt(down) / 2)
            rodY = Mth.lerp(partialTicks.toDouble(), owner.yo, owner.y) + vec3.y - sqrt(down)
            rodZ = Mth.lerp(partialTicks.toDouble(), owner.zo, owner.z) + vec3.z
            yOffset = owner.getEyeHeight()
        } else {
            rodX = Mth.lerp(partialTicks.toDouble(), owner.xo, owner.x) - animaY * animaZ - animaX * 0.8
            rodY = owner.yo + owner.eyeHeight.toDouble() + (owner.y - owner.yo) * partialTicks.toDouble() - 0.45
            rodZ = Mth.lerp(partialTicks.toDouble(), owner.zo, owner.z) - animaX * animaZ + animaY * 0.8
            yOffset = if (owner.isCrouching) -0.1875f else 0.0f
        }

        val entityX = Mth.lerp(partialTicks.toDouble(), worldAxe.xo, worldAxe.x)
        val entityY = Mth.lerp(partialTicks.toDouble(), worldAxe.yo, worldAxe.y) + 0.25
        val entityZ = Mth.lerp(partialTicks.toDouble(), worldAxe.zo, worldAxe.z)
        val diffX = (rodX - entityX).toFloat()
        val diffY = ((rodY - entityY).toFloat() + yOffset + 0.5F)
        val diffZ = (rodZ - entityZ).toFloat()
        val lineBuffer: VertexConsumer = bufferSource.getBuffer(RenderType.lineStrip())
        val linePose = poseStack.last()
        val j = 16

        poseStack.translate(0.0, -0.25, 0.0)
        for (k in 0..j) {
            stringVertex(diffX, diffY, diffZ, lineBuffer, linePose, fraction(k, 16), fraction(k + 1, 16))
        }
        poseStack.popPose()
        super.render(worldAxe, yaw, partialTicks, poseStack, bufferSource, packedLightIn)
    }

    private fun fraction(up: Int, down: Int): Float {
        return up.toFloat() / down.toFloat()
    }

    private fun stringVertex(
        diffX: Float, diffY: Float, diffZ: Float, vertexConsumer: VertexConsumer, pose: PoseStack.Pose, fraction1: Float, fraction2: Float
    ) {
        val x1: Float = diffX * fraction1
        val y1: Float = diffY * (fraction1 * fraction1 + fraction1) * 0.5f + 0.25f
        val z1: Float = diffZ * fraction1
        var x2: Float = diffX * fraction2 - x1
        var y2: Float = diffY * (fraction2 * fraction2 + fraction2) * 0.5f + 0.25f - y1
        var z2: Float = diffZ * fraction2 - z1
        val length = Mth.sqrt(x2 * x2 + y2 * y2 + z2 * z2)
        x2 /= length
        y2 /= length
        z2 /= length
        vertexConsumer.vertex(pose.pose(), x1, y1, z1).color(0, 0, 0, 255).normal(pose.normal(), x2, y2, z2).endVertex()
    }
}