package com.tmvkrpxl0.tcombat.client.renderers

import com.mojang.blaze3d.vertex.VertexConsumer

class ColorWrappingVertexConsumer(
    private val original: VertexConsumer,
    private val red: Int,
    private val green: Int,
    private val blue: Int,
    private val alpha: Int
) : VertexConsumer by original{
    override fun color(pRed: Int, pGreen: Int, pBlue: Int, pAlpha: Int): VertexConsumer = original.color(red, green, blue, alpha)
}