package com.tmvkrpxl0.tcombat.client.renderers

import com.tmvkrpxl0.tcombat.common.entities.projectile.ReflectiveArrow
import net.minecraft.client.renderer.entity.ArrowRenderer
import net.minecraft.client.renderer.entity.EntityRendererProvider
import net.minecraft.client.renderer.entity.TippableArrowRenderer
import net.minecraft.resources.ResourceLocation

class ReflectiveArrowRenderer(p_i46193_1_: EntityRendererProvider.Context) : ArrowRenderer<ReflectiveArrow>(p_i46193_1_) {
    override fun getTextureLocation(p_110775_1_: ReflectiveArrow): ResourceLocation = TippableArrowRenderer.NORMAL_ARROW_LOCATION
}