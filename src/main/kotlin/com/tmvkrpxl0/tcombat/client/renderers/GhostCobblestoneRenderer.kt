package com.tmvkrpxl0.tcombat.client.renderers

import com.mojang.blaze3d.vertex.PoseStack
import com.tmvkrpxl0.tcombat.common.skill.TCombatCustomSkills.AUTO_BRIDGE
import com.tmvkrpxl0.tcombat.common.skill.impl.AutoBridge
import net.minecraft.client.Minecraft
import net.minecraft.core.BlockPos
import net.minecraft.util.RandomSource
import net.minecraft.world.level.block.Blocks
import net.minecraft.world.phys.Vec3
import net.neoforged.neoforge.client.NeoForgeRenderTypes
import net.neoforged.neoforge.client.event.RenderLevelStageEvent
import net.neoforged.neoforge.client.model.data.ModelData
import net.neoforged.neoforge.event.TickEvent
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.minus

private var autoBridgePreview: BlockPos? = null

fun setAutoBridgePreview(event: TickEvent.ClientTickEvent) {
    if (event.phase != TickEvent.Phase.START) return

    val minecraft = Minecraft.getInstance()
    val player = minecraft.player?: return

    if (!AutoBridge.ON_GROUND.check(player, AUTO_BRIDGE)) return
    if (!AutoBridge.HOLDING_BUCKETS.check(player, AUTO_BRIDGE)) return

    autoBridgePreview = BlockPos.containing(AutoBridge.placeAtRelative(player, 2.0).add(player.position()))
}

fun autoBridgePreview(event: RenderLevelStageEvent) {
    if (autoBridgePreview == null) return
    if (event.stage != RenderLevelStageEvent.Stage.AFTER_PARTICLES) return

    val minecraft = Minecraft.getInstance()
    val player = minecraft.player?: return

    if (!AutoBridge.ON_GROUND.check(player, AUTO_BRIDGE)) return
    if (!AutoBridge.HOLDING_BUCKETS.check(player, AUTO_BRIDGE)) return

    renderGhostCobblestone(event.poseStack, autoBridgePreview!!)
}

private val COBBLESTONE = Blocks.COBBLESTONE.defaultBlockState()

private val blockRenderer by lazy { Minecraft.getInstance().blockRenderer }

private val cobblestoneModel by lazy { blockRenderer.getBlockModel(COBBLESTONE) }

private val RANDOM = RandomSource.create()

private fun renderGhostCobblestone(poseStack: PoseStack, blockPos: BlockPos) {
    val minecraft = Minecraft.getInstance()

    require(minecraft.level != null) { "Unable to render without level!" }

    val mainCamera = minecraft.gameRenderer.mainCamera
    val buffers = minecraft.renderBuffers().bufferSource()

    val ghostConsumer = ColorWrappingVertexConsumer(
        buffers.getBuffer(NeoForgeRenderTypes.TRANSLUCENT_ON_PARTICLES_TARGET.get()),
        0xFF,
        0xFF,
        0xFF,
        200
    )

    val offset = Vec3.atLowerCornerOf(blockPos) - mainCamera.position

    cobblestoneModel.getRenderTypes(COBBLESTONE, RANDOM, ModelData.EMPTY).forEach {
        poseStack.pushPose()
        poseStack.translate(offset.x, offset.y, offset.z)

        blockRenderer.renderBatched(COBBLESTONE, blockPos, minecraft.level!!, poseStack, ghostConsumer, false, RANDOM, ModelData.EMPTY, it)
    }

    buffers.endBatch(NeoForgeRenderTypes.TRANSLUCENT_ON_PARTICLES_TARGET.get())
}