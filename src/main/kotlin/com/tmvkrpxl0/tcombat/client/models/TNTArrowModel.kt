package com.tmvkrpxl0.tcombat.client.models

import com.mojang.blaze3d.vertex.PoseStack
import com.mojang.blaze3d.vertex.VertexConsumer
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.entities.projectile.TNTArrow
import net.minecraft.client.model.EntityModel
import net.minecraft.client.model.geom.ModelLayerLocation
import net.minecraft.client.model.geom.ModelPart
import net.minecraft.client.model.geom.PartPose
import net.minecraft.client.model.geom.builders.CubeDeformation
import net.minecraft.client.model.geom.builders.CubeListBuilder
import net.minecraft.client.model.geom.builders.LayerDefinition
import net.minecraft.client.model.geom.builders.MeshDefinition
import net.minecraft.resources.ResourceLocation

class TNTArrowModel(root: ModelPart) : EntityModel<TNTArrow>() {
    private val bbMain: ModelPart
    override fun setupAnim(
        entity: TNTArrow, limbSwing: Float, limbSwingAmount: Float, ageInTicks: Float, netHeadYaw: Float, headPitch: Float
    ) {
    }

    override fun renderToBuffer(
        poseStack: PoseStack, buffer: VertexConsumer, packedLight: Int, packedOverlay: Int, red: Float, green: Float, blue: Float, alpha: Float
    ) {
        bbMain.render(poseStack, buffer, packedLight, packedOverlay)
    }

    companion object {
        // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
        val LAYER_LOCATION = ModelLayerLocation(ResourceLocation(MODID, "tnt_arrow"), "main")
        fun createBodyLayer(): LayerDefinition {
            val meshdefinition = MeshDefinition()
            val partdefinition = meshdefinition.root
            partdefinition.addOrReplaceChild(
                "bb_main",
                CubeListBuilder.create().texOffs(0, 6).addBox(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 8.0f, CubeDeformation(0.0f)),
                PartPose.offsetAndRotation(0.0f, 0.0f, 0.0f, 0F, Math.toRadians(-90.0).toFloat(), 0F)
            )
            return LayerDefinition.create(meshdefinition, 64, 64)
        }
    }

    init {
        bbMain = root.getChild("bb_main")
    }
}