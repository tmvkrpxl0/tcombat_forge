package com.tmvkrpxl0.tcombat.client

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonObject
import com.mojang.math.Transformation
import net.minecraft.client.renderer.RenderType
import net.minecraft.client.renderer.block.model.BakedQuad
import net.minecraft.client.renderer.block.model.BlockModel
import net.minecraft.client.renderer.block.model.ItemOverrides
import net.minecraft.client.renderer.block.model.ItemTransforms
import net.minecraft.client.renderer.texture.TextureAtlasSprite
import net.minecraft.client.resources.model.*
import net.minecraft.core.Direction
import net.minecraft.resources.ResourceLocation
import net.minecraft.util.RandomSource
import net.minecraft.world.level.block.state.BlockState
import net.neoforged.neoforge.client.model.SimpleModelState
import net.neoforged.neoforge.client.model.data.ModelData
import net.neoforged.neoforge.client.model.geometry.IGeometryBakingContext
import net.neoforged.neoforge.client.model.geometry.IGeometryLoader
import net.neoforged.neoforge.client.model.geometry.IUnbakedGeometry
import java.util.*
import java.util.function.Function

//TODO This should handle situation where model is invalid, such as no "structure"
/**
 * This class can only use BlockModel as its parts
 */
object StructureModelLoader : IGeometryLoader<StructureModelLoader.StructureModelGeometry> {
    override fun read(
        modelContents: JsonObject,
        deserializationContext: JsonDeserializationContext,
    ): StructureModelGeometry {
        val array = modelContents.getAsJsonArray("structure")
        val structure = Array(array.size()) {
            val element = array[it].asJsonObject
            val transform = if (element.has("transform")) {
                deserializationContext.deserialize(
                    element.getAsJsonObject("transform"),
                    Transformation::class.java, //This can be null as it's unused
                )
            } else Transformation.identity()
            val blockModel = deserializationContext.deserialize<BlockModel>(element.getAsJsonObject("model"), BlockModel::class.java)
            return@Array StructureEntry(transform, blockModel)
        }
        return StructureModelGeometry(structure)
    }

    data class StructureEntry(val transform: Transformation, val model: BlockModel)

    class StructureModelGeometry(
        private val structure: Array<StructureEntry>,
    ) : IUnbakedGeometry<StructureModelGeometry> {
        override fun bake(
            context: IGeometryBakingContext,
            baker: ModelBaker,
            spriteGetter: Function<Material, TextureAtlasSprite>,
            modelTransform: ModelState,
            overrides: ItemOverrides,
            modelLocation: ResourceLocation
        ): BakedModel {
            val sideLit = context.useBlockLight()

            val baked = structure.map {
                val (transform, model) = it
                return@map model.bake(
                    baker, model, spriteGetter, SimpleModelState(transform), modelLocation, sideLit
                )
            }.toTypedArray()
            val mostCommonParticle = spriteGetter.apply(structure.groupBy { it.model }.maxByOrNull { it.value.size }!!.key.getMaterial("particle"))
            return BakedStructureModel(
                context.useAmbientOcclusion(), context.isGui3d, sideLit, mostCommonParticle, baked, overrides, context.transforms
            )
        }

        override fun resolveParents(
            modelGetter: Function<ResourceLocation, UnbakedModel>,
            context: IGeometryBakingContext?
        ) {
            structure.forEach { it.model.resolveParents(modelGetter) }
        }
    }

    class BakedStructureModel(
        private val useAmbient: Boolean,
        private val gui3d: Boolean,
        private val useBlockLight: Boolean,
        private val particle: TextureAtlasSprite,
        private val bakedBlockModels: Array<out BakedModel>,
        private val overrides: ItemOverrides,
        private val itemTransforms: ItemTransforms
    ) : BakedModel {
        companion object;

        @Suppress("DEPRECATION")
        @Deprecated("Deprecated in Java")
        override fun getQuads(blockState: BlockState?, direction: Direction?, random: RandomSource): List<BakedQuad> {
            return bakedBlockModels.flatMap { bakedModel ->
                bakedModel.getQuads(blockState, direction, random)
            }
        }

        override fun getQuads(
            blockState: BlockState?, direction: Direction?, random: RandomSource, data: ModelData, renderType: RenderType?
        ): MutableList<BakedQuad> {
            return bakedBlockModels.flatMapTo(LinkedList()) { bakedModel ->
                bakedModel.getQuads(blockState, direction, random, data, renderType)
            }
        }

        @Deprecated("Deprecated in Java")
        override fun getTransforms() = itemTransforms

        override fun useAmbientOcclusion(): Boolean = useAmbient

        @Deprecated("Deprecated in Java")
        override fun getParticleIcon(): TextureAtlasSprite = particle

        override fun isGui3d(): Boolean = gui3d

        override fun usesBlockLight(): Boolean = useBlockLight

        override fun isCustomRenderer(): Boolean = false

        override fun getOverrides(): ItemOverrides = overrides
    }
}
