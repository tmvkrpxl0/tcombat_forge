package com.tmvkrpxl0.tcombat.common.skill.impl

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.skill.COOLDOWN
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.skill.SkillPredicate
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import com.tmvkrpxl0.tcombat.common.entities.projectile.ReflectiveArrow
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.REFLECTIVE_ARROW
import net.minecraft.network.chat.Component
import net.minecraft.server.level.ServerPlayer
import net.minecraft.util.RandomSource
import net.minecraft.world.phys.Vec3

object ReflectionBlast : Skill() {
    private val COUNT_ARROW_NAME = "count_arrows".toResource()

    override val skillName = "reflection_blast".toResource()

    override val predicates: List<SkillPredicate> = listOf(COOLDOWN, SkillPredicate(COUNT_ARROW_NAME) { player, _ ->
        val count = player.inventory.countItem(REFLECTIVE_ARROW)
        return@SkillPredicate player.isCreative || count != 0
    })

    override val skillCooldownTick: Long = 200
    override fun execute(player: ServerPlayer): Boolean {
        val count = player.inventory.countItem(REFLECTIVE_ARROW)
        if (!player.isCreative && count == 0) return false

        val random = player.level().random
        val base = player.position().add(0.0, player.eyeHeight.toDouble(), 0.0).add(player.lookAngle)
        val delta = player.lookAngle.scale(1.5)
        for (i in 1..16) {
            val arrow = ReflectiveArrow(player, 25F, player.level())
            arrow.owner = player
            val perpendicular = getPerpendicularRandom(delta, random).scale(random.nextDouble() * 10 - 5)
            val combined = base.add(perpendicular)
            arrow.setPos(combined.x, combined.y, combined.z)
            arrow.deltaMovement = delta
            player.level().addFreshEntity(arrow)
            for (item in player.inventory.items) {
                if (player.isCreative) break
                if (item.item == REFLECTIVE_ARROW) {
                    item.count--
                    break
                }
            }
        }

        return true
    }

    //v1.x*v2.x + v1.y*v2.y + v1.z*v2.z = 0
    //v1.z*v2.z = - v1.x*v2.x - v1.y*v2.y
    //v2.z = (- v1.x*v2.x - v1.y*v2.y) / v1.z
    private fun getPerpendicularRandom(v1: Vec3, random: RandomSource): Vec3 {
        val v2x = random.nextDouble() * v1.x
        val v2y = random.nextDouble() * v1.y
        val v2z = (-v2x - v2y) / v1.z
        return Vec3(v2x, v2y, v2z).normalize()
    }

    override fun onPredicateFail(player: ServerPlayer, whatFailed: SkillPredicate) {
        when (whatFailed.name) {
            COOLDOWN.name -> {
                player.sendSystemMessage(Component.translatable("$MODID.skill.cooldown_fail", this))
            }
            COUNT_ARROW_NAME -> {
                player.sendSystemMessage(Component.translatable("$translationKey.count_arrow_fail"))
            }
        }
    }
}