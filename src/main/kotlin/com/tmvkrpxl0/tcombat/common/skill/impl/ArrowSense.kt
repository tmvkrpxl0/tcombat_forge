package com.tmvkrpxl0.tcombat.common.skill.impl

import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.skill.SkillPredicate
import com.tmvkrpxl0.tcombat.base.common.skill.autoEnable
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import com.tmvkrpxl0.tcombat.mixins.bindings.common.ArrowAccessor
import net.minecraft.server.level.ServerPlayer
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.Entity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.phys.AABB
import org.joml.Vector4f
import java.util.*

object ArrowSense : Skill() {
    override val skillName = "arrow_sense".toResource()
    override val predicates = emptyList<SkillPredicate>()

    val alerting = HashSet<UUID>()

    private val size = AABB(-15.0, -15.0, -15.0, 15.0, 15.0, 15.0)

    init {
        autoEnable = true
    }

    override fun execute(player: ServerPlayer): Boolean {
        val axisAlignedBB = size.move(player.position())
        val list = player.level().getEntities(
            player, axisAlignedBB
        ) { arrow: Entity ->
            return@getEntities arrow is AbstractArrow && arrow.owner != player && arrow.distanceToSqr(player) < 15 * 15 && player.hasLineOfSight(arrow) && !(arrow as ArrowAccessor).isInGround
        }
        if (list.isNotEmpty()) {
            if (!alerting.contains(player.uuid)) {
                alertPlayer(player, Vector4f(0.7F, 0.1F, 0.1F, 0.5F))
                alerting.add(player.uuid)
            }
            player.playNotifySound(SoundEvents.NOTE_BLOCK_BELL.value(), SoundSource.HOSTILE, 1f, 1.3f)
        } else {
            if (alerting.contains(player.uuid)) {
                alertPlayer(player, Vector4f(0F, 0F, 0F, 0F))
                alerting.remove(player.uuid)
            }
        }
        return true
    }
}