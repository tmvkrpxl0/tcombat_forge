package com.tmvkrpxl0.tcombat.common.skill

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import com.tmvkrpxl0.tcombat.common.skill.impl.*
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.registries.DeferredRegister
import thedarkcolour.kotlinforforge.neoforge.forge.registerObject

object TCombatCustomSkills {
    private val SKILLS: DeferredRegister<Skill> = DeferredRegister.create("skill_registry".toResource(), MODID)

    val ARROW_SENSE by SKILLS.registerObject(ArrowSense.skillName.path) { ArrowSense }
    val AUTO_BRIDGE by SKILLS.registerObject(AutoBridge.skillName.path) { AutoBridge }
    val REFLECTION_BLAST by SKILLS.registerObject(ReflectionBlast.skillName.path) { ReflectionBlast }
    val SHATTER_ARROW by SKILLS.registerObject(ShatterArrow.skillName.path) { ShatterArrow }
    val TEST_SKILL by SKILLS.registerObject(TestSkill.skillName.path) { TestSkill }

    fun register(bus: IEventBus) {
        SKILLS.register(bus)
    }
}