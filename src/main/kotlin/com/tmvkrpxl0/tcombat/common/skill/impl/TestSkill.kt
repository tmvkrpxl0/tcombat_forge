package com.tmvkrpxl0.tcombat.common.skill.impl

import com.tmvkrpxl0.tcombat.base.common.entities.miscs.GroundChunk
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.skill.SkillPredicate
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import net.minecraft.core.particles.BlockParticleOption
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.phys.AABB
import net.minecraft.world.phys.Vec3
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.plus

object TestSkill : Skill() {
    override val skillName = "test".toResource()

    override val predicates: List<SkillPredicate> = emptyList()

    override fun execute(player: ServerPlayer): Boolean {
        val playerPos = player.blockPosition()
        val start = playerPos.offset(-5, -5, -5)
        val end = playerPos.offset(5, -1, 5)
        val blocks = AABB(start, end)

        val ground = GroundChunk(player, blocks)
        ground.setPos(player.position().add(0.0, 10.0, 0.0))
        player.level().addFreshEntity(ground)
        ground.dissolve(4) { blockPos, state ->
            val pos = Vec3.atCenterOf(blockPos) + ground.position()
            player.serverLevel().sendParticles(
                BlockParticleOption(ParticleTypes.BLOCK, state),
                pos.x, pos.y, pos.z,
                10,
                0.0, 0.0, 0.0,
                10.0
            )
        }
        return true
    }

}