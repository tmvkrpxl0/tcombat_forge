package com.tmvkrpxl0.tcombat.common.skill.impl

import com.tmvkrpxl0.tcombat.base.common.skill.COOLDOWN
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.skill.setSkillCooldown
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import com.tmvkrpxl0.tcombat.mixins.bindings.common.ArrowAccessor
import net.minecraft.core.particles.ItemParticleOption
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.server.level.ServerPlayer
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.ItemStack
import net.minecraft.world.phys.AABB
import net.neoforged.fml.util.ObfuscationReflectionHelper

object ShatterArrow : Skill() {
    override val skillName = "shatter_arrow".toResource()

    override val predicates = listOf(COOLDOWN)
    override val skillCooldownTick = 2L

    private val scanRegion = AABB(-15.0, -15.0, -15.0, 15.0, 15.0, 15.0)

    val getPickupItem = ObfuscationReflectionHelper.findMethod(AbstractArrow::class.java, "getPickupItem").apply { isAccessible = true }
    override fun execute(player: ServerPlayer): Boolean {
        val arrows = player.level().getEntities(
            player, scanRegion.move(player.position())
        ).asSequence()
            .filterIsInstance<AbstractArrow>()
            .filter { it.distanceToSqr(player) <= (5 * 5) }
            .filter { !(it as ArrowAccessor).isInGround || it.isPickable }
            .toList()

        if (arrows.isEmpty()) {
            player.setSkillCooldown(this, 15L)
            return false
        }

        arrows.forEach { arrow ->
            val particleOption = ItemParticleOption(ParticleTypes.ITEM, getPickupItem.invoke(arrow) as ItemStack)
            val serverLevel = player.serverLevel()

            serverLevel.sendParticles(
                particleOption,
                arrow.x, arrow.y, arrow.z,
                16,
                0.0, 0.0, 0.0,
                0.04
            )
            serverLevel.sendParticles(
                ParticleTypes.SWEEP_ATTACK,
                arrow.x, arrow.y, arrow.z,
                1,
                0.0, 0.0, 0.0,
                0.2
            )

            serverLevel.playSound(
                player,
                arrow,
                SoundEvents.SHIELD_BLOCK, SoundSource.PLAYERS,
                0.8F, 0.8F + serverLevel.random.nextFloat() * 0.4F
            )
            arrow.kill()
        }

        return true
    }
}