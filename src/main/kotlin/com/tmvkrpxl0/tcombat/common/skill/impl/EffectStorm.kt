package com.tmvkrpxl0.tcombat.common.skill.impl

import com.tmvkrpxl0.tcombat.base.common.skill.COOLDOWN
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import net.minecraft.server.level.ServerPlayer

object EffectStorm : Skill() {
    override val skillName = "effect_storm".toResource()
    override val predicates = listOf(COOLDOWN)
    override val skillCooldownTick: Long = 20 * 60 * 5

    override fun execute(player: ServerPlayer): Boolean { // TODO Implement this, it's dummy now
        return true
    }
}