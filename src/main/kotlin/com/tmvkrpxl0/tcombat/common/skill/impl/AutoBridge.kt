package com.tmvkrpxl0.tcombat.common.skill.impl

import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.skill.SkillPredicate
import com.tmvkrpxl0.tcombat.base.common.skill.autoEnable
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import net.minecraft.core.BlockPos
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.player.Player
import net.minecraft.world.item.BucketItem
import net.minecraft.world.level.block.Blocks
import net.minecraft.world.level.material.Fluids
import net.minecraft.world.level.material.LavaFluid
import net.minecraft.world.level.material.WaterFluid
import net.minecraft.world.phys.Vec3

object AutoBridge : Skill() {
    override val skillName = "auto_bridge".toResource()
    val HOLDING_BUCKETS = SkillPredicate("holding_buckets".toResource()) { player, _ ->
        val mainHand = player.mainHandItem
        val offHand = player.offhandItem

        if (mainHand.item !is BucketItem) return@SkillPredicate false
        if (offHand.item !is BucketItem) return@SkillPredicate false

        val mainBucket = mainHand.item as BucketItem
        val offBucket = offHand.item as BucketItem

        val fluid1 = if (mainBucket.fluid is WaterFluid) 1 else if (mainBucket.fluid is LavaFluid) 2 else 0
        val fluid2 = if (offBucket.fluid is WaterFluid) 1 else if (offBucket.fluid is LavaFluid) 2 else 0

        return@SkillPredicate fluid1 + fluid2 == 3 /* Checks If player is holding water on one hand, and lava on other hand*/
    }
    val ON_GROUND = SkillPredicate("is_on_ground".toResource()) { player, _ ->
        return@SkillPredicate player.onGround()
    }

    override val predicates = listOf(ON_GROUND, HOLDING_BUCKETS)

    init {
        autoEnable = true
    }

    fun placeAtRelative(player: Player, scale: Double = 1.0): Vec3 {
        val horizontalLook = player.lookAngle.multiply(1.0, 0.0, 1.0).normalize().scale(scale)

        var yDiff = -1.0


        if (player.xRot < 5) { // UP
            yDiff += 1
        } else if (player.xRot > 45) { // DOWN
            yDiff -= 1
        }

        return horizontalLook.add(0.0, yDiff, 0.0)
    }

    override fun execute(player: ServerPlayer): Boolean {
        val level = player.serverLevel()

        val placingPos = BlockPos.containing(placeAtRelative(player).add(player.position()))
        val blockState = level.getBlockState(placingPos)

        val above = placingPos.above()
        val aboveState = level.getBlockState(above)

        if (blockState.canBeReplaced(Fluids.FLOWING_WATER) && aboveState.canBeReplaced(Fluids.FLOWING_WATER)) {
            level.setBlockAndUpdate(placingPos, Blocks.COBBLESTONE.defaultBlockState())
            level.levelEvent(1501, placingPos, 0)
        }
        return true
    }
}