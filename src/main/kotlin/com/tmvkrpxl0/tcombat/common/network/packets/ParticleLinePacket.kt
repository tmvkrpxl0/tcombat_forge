package com.tmvkrpxl0.tcombat.common.network.packets

import com.tmvkrpxl0.tcombat.base.common.network.packets.ITCombatPacket
import com.tmvkrpxl0.tcombat.base.common.network.packets.Packet
import net.minecraft.client.Minecraft
import net.minecraft.core.particles.ParticleOptions
import net.minecraft.core.particles.ParticleType
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.world.phys.Vec3
import net.neoforged.fml.LogicalSide
import net.neoforged.neoforge.network.NetworkEvent
import net.neoforged.neoforge.registries.ForgeRegistries
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.minus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.times

@Packet(receiveSide = LogicalSide.CLIENT)
class ParticleLinePacket(
    private val start: Vec3, private val end: Vec3, val type: ParticleType<out ParticleOptions>, private val repeat: Int
) : ITCombatPacket {
    companion object {
        fun decode(buffer: FriendlyByteBuf): ParticleLinePacket {
            val sX = buffer.readDouble()
            val sY = buffer.readDouble()
            val sZ = buffer.readDouble()
            val start = Vec3(sX, sY, sZ)
            val eX = buffer.readDouble()
            val eY = buffer.readDouble()
            val eZ = buffer.readDouble()
            val end = Vec3(eX, eY, eZ)
            val type = buffer.readRegistryIdSafe(ParticleType::class.java)
            val repeat = buffer.readInt()
            return ParticleLinePacket(start, end, type, repeat)
        }
    }

    override fun encode(buffer: FriendlyByteBuf) {
        buffer.writeDouble(start.x)
        buffer.writeDouble(start.y)
        buffer.writeDouble(start.z)
        buffer.writeDouble(end.x)
        buffer.writeDouble(end.y)
        buffer.writeDouble(end.z)
        buffer.writeRegistryId(ForgeRegistries.PARTICLE_TYPES, type)
        buffer.writeInt(repeat)
    }

    override fun handle(context: NetworkEvent.Context) {
        context.enqueueWork {
            val level = Minecraft.getInstance().level!!
            var increase = 0.0
            val distanceSq = start.distanceToSqr(end)
            val normal = (end - start).normalize()
            val add = normal * 0.2
            var toDraw = start
            while ((increase * increase) <= distanceSq) {
                for (i in 0 until repeat) {
                    level.addParticle(ParticleTypes.REVERSE_PORTAL, toDraw.x, toDraw.y, toDraw.z, 0.0, 0.0, 0.0)
                }
                increase += 0.2
                toDraw = toDraw.add(add)
            }
        }
    }
}