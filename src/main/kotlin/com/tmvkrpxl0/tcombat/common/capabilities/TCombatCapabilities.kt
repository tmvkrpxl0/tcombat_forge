package com.tmvkrpxl0.tcombat.common.capabilities

import net.neoforged.neoforge.common.capabilities.Capability
import net.neoforged.neoforge.common.capabilities.CapabilityManager
import net.neoforged.neoforge.common.capabilities.CapabilityToken

val ENTITY_HOLDER: Capability<IEntityHolder> = CapabilityManager.get(object : CapabilityToken<IEntityHolder>() {})