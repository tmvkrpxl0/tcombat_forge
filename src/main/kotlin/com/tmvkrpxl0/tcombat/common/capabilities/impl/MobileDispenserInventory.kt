package com.tmvkrpxl0.tcombat.common.capabilities.impl

import com.tmvkrpxl0.tcombat.common.items.misc.MobileDispenserItem
import net.minecraft.core.Direction
import net.minecraft.nbt.CompoundTag
import net.minecraft.nbt.Tag
import net.minecraft.world.item.ItemStack
import net.neoforged.neoforge.common.capabilities.Capabilities
import net.neoforged.neoforge.common.capabilities.Capability
import net.neoforged.neoforge.common.capabilities.ICapabilitySerializable
import net.neoforged.neoforge.common.util.LazyOptional
import net.neoforged.neoforge.items.ItemStackHandler


class MobileDispenserInventory(size: Int) : ItemStackHandler(size), Iterable<ItemStack> {

    override fun setStackInSlot(slot: Int, stack: ItemStack) {
        if (!stack.isEmpty && !isItemValid(slot, stack)) return
        super.setStackInSlot(slot, stack)
    }

    override fun isItemValid(slot: Int, stack: ItemStack): Boolean {
        return MobileDispenserItem.isProjectile(stack)
    }

    class Provider(size: Int) : ICapabilitySerializable<Tag> {
        private val inventory = MobileDispenserInventory(size)

        @Suppress("UNCHECKED_CAST")
        override fun <T : Any> getCapability(cap: Capability<T>, unused: Direction?): LazyOptional<T> {
            return if (Capabilities.ITEM_HANDLER === cap) {
                LazyOptional.of { return@of inventory as T }
            } else LazyOptional.empty()
        }

        override fun serializeNBT(): CompoundTag {
            return inventory.serializeNBT()
        }

        override fun deserializeNBT(nbt: Tag?) {
            inventory.deserializeNBT(nbt as CompoundTag?)
        }
    }

    override fun iterator() = stacks.iterator()
}