package com.tmvkrpxl0.tcombat.common.capabilities.impl

import com.tmvkrpxl0.tcombat.common.capabilities.ENTITY_HOLDER
import com.tmvkrpxl0.tcombat.common.capabilities.IEntityHolder
import net.minecraft.core.Direction
import net.minecraft.world.entity.Entity
import net.neoforged.neoforge.common.capabilities.Capability
import net.neoforged.neoforge.common.capabilities.ICapabilityProvider
import net.neoforged.neoforge.common.util.LazyOptional

class EntityHolder(entity: Entity?) : IEntityHolder {
    override var entity: Entity? = entity
        get() {
            if (field?.isAlive != true) field = null
            return field
        }
        set(value) {
            if (value?.isAlive == true) field = value
        }

    public class Provider(entity: Entity?) : ICapabilityProvider {
        private val entityHolder: IEntityHolder = EntityHolder(entity)

        @Suppress("UNCHECKED_CAST")
        override fun <T : Any> getCapability(cap: Capability<T>, side: Direction?): LazyOptional<T> {
            return if (cap === ENTITY_HOLDER) {
                LazyOptional.of { return@of entityHolder as T }
            } else LazyOptional.empty()
        }
    }
}