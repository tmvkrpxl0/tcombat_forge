package com.tmvkrpxl0.tcombat.common.capabilities

import net.minecraft.world.entity.Entity

interface IEntityHolder {
    var entity: Entity?
}