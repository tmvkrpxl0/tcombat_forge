package com.tmvkrpxl0.tcombat.common.entities.projectile

import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import com.tmvkrpxl0.tcombat.common.capabilities.ENTITY_HOLDER
import com.tmvkrpxl0.tcombat.common.entities.TCombatEntityTypes
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.WORLD_AXE
import net.minecraft.core.particles.BlockParticleOption
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.nbt.CompoundTag
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.network.protocol.Packet
import net.minecraft.network.protocol.game.ClientGamePacketListener
import net.minecraft.network.syncher.EntityDataAccessor
import net.minecraft.network.syncher.EntityDataSerializers
import net.minecraft.network.syncher.SynchedEntityData
import net.minecraft.world.entity.Entity
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.player.Player
import net.minecraft.world.entity.projectile.Projectile
import net.minecraft.world.item.ItemStack
import net.minecraft.world.item.enchantment.EnchantmentHelper
import net.minecraft.world.item.enchantment.Enchantments
import net.minecraft.world.level.Level
import net.minecraft.world.level.block.state.BlockState
import net.minecraft.world.phys.AABB
import net.minecraft.world.phys.BlockHitResult
import net.minecraft.world.phys.EntityHitResult
import net.neoforged.neoforge.entity.IEntityAdditionalSpawnData
import net.neoforged.neoforge.network.NetworkHooks
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.minus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.plus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.times
import java.lang.Double.isNaN

class WorldAxe : Projectile, IEntityAdditionalSpawnData {
    companion object {
        val HOOKED: EntityDataAccessor<Int> = SynchedEntityData.defineId(WorldAxe::class.java, EntityDataSerializers.INT)
        val ZROT: EntityDataAccessor<Float> = SynchedEntityData.defineId(WorldAxe::class.java, EntityDataSerializers.FLOAT)
    }

    var inGround = false
    private var lastState: BlockState? = null
        get() {
            return if (!inGround) null else field
        }
        set(value) {
            require(inGround)
            field = value
        }
    lateinit var baseAxe: ItemStack
        private set
    var hooked: LivingEntity? = null

    constructor(entityType: EntityType<out WorldAxe>, level: Level) : super(entityType, level) {
        throw AssertionError("This constructor should never be used")
    }

    constructor(player: Player, baseItem: ItemStack) : super(TCombatEntityTypes.worldAxe, player.level()) {
        this.owner = player
        require(baseItem.item == WORLD_AXE)
        this.baseAxe = baseItem
        val lookAngle = player.lookAngle
        this.absMoveTo(
            player.x + lookAngle.x, player.eyeY + lookAngle.y, player.z + lookAngle.z, player.yRot, 0F
        )
        this.yRot += 90
        this.isNoGravity = false
        this.setzRot(90 - player.xRot)
        val enchants = EnchantmentHelper.getEnchantments(this.baseAxe)
        if (enchants.containsKey(Enchantments.FIRE_ASPECT)) {
            this.setSecondsOnFire(100)
        }
    }

    override fun setOwner(owner: Entity?) {
        require(owner != null && owner is Player)
        super.setOwner(owner)
    }

    override fun getOwner(): Player {
        return super.getOwner() as Player
    }

    override fun readAdditionalSaveData(compound: CompoundTag) {
        this.baseAxe = ItemStack.of(compound.getCompound("BaseAxe"))
        this.inGround = compound.getBoolean("inGround")
        super.readAdditionalSaveData(compound)
    }

    override fun addAdditionalSaveData(compound: CompoundTag) {
        val c = CompoundTag()
        compound.put("BaseAxe", this.baseAxe.save(c))
        compound.putBoolean("inGround", this.inGround)
        super.addAdditionalSaveData(compound)
    }

    override fun getAddEntityPacket(): Packet<ClientGamePacketListener> = NetworkHooks.getEntitySpawningPacket(this)

    override fun defineSynchedData() {
        this.entityData.define(HOOKED, 0)
        this.entityData.define(ZROT, 0f)
    }

    override fun remove(reason: RemovalReason) {
        this.baseAxe.getCapability(ENTITY_HOLDER).resolve().get().entity = null
        super.remove(reason)
    }

    override fun tick() {
        if (shouldRemove(this.owner)) this.discard()
        val position = this.position()
        val blockPos = this.blockPosition()
        val blockState = this.level().getBlockState(blockPos)
        if (!blockState.isAir) {
            val voxelShape = blockState.getCollisionShape(this.level(), blockPos)
            if (!voxelShape.isEmpty) {
                for (shape in voxelShape.toAabbs()) {
                    if (shape.move(blockPos).contains(position)) {
                        this.inGround = true
                        break
                    }
                }
            }
        }

        if (this.isInWaterOrRain) {
            this.clearFire()
        }

        if (this.inGround) {
            if (blockState !== this.lastState && this.shouldFall()) {
                this.fall()
            }
        } else {
            val next = this.position() + this.deltaMovement
            this.setPos(next.x, next.y, next.z)
        }
        super.tick()
    }

    private fun shouldFall(): Boolean {
        return inGround && this.level().noCollision(AABB(this.position(), this.position()).inflate(0.06))
    }

    private fun fall() {
        inGround = false
        val vector3d = this.deltaMovement
        this.deltaMovement = vector3d.multiply(
            (random.nextFloat() * 0.2), (random.nextFloat() * 0.2), (random.nextFloat() * 0.2)
        )
    }

    fun getWaterInertia(): Float {
        return 0.6f
    }

    override fun onHitEntity(result: EntityHitResult) {
        super.onHitEntity(result)
        if (this.level().isServerSide) {
            if (result.entity is LivingEntity) {
                this.hooked = result.entity as LivingEntity
                this.setHookedEntity()
            }
        }
    }

    private fun setHookedEntity() {
        this.entityData.set(HOOKED, this.hooked!!.id)
    }

    override fun onHitBlock(result: BlockHitResult) {
        lastState = this.level().getBlockState(result.blockPos)
        super.onHitBlock(result)
        val diff = result.location - this.position()
        this.deltaMovement = diff
        val vector3d1 = diff.normalize() * 0.05
        setPosRaw(this.x - vector3d1.x, this.y - vector3d1.y, this.z - vector3d1.z)
        if (lastState != null) {
            this.playSound(lastState!!.soundType.breakSound, 1.0f, 0.8f)
            for (i in 1..10) {
                this.level().addParticle(
                    BlockParticleOption(ParticleTypes.BLOCK, lastState!!), this.x, this.y, this.z, 1.0, 1.0, 1.0
                )
            }
        }
        inGround = true
    }

    override fun writeSpawnData(buffer: FriendlyByteBuf) {
        buffer.writeVarInt(this.owner.id)
        buffer.writeItem(this.baseAxe)
    }

    override fun readSpawnData(additionalData: FriendlyByteBuf) {
    }

    override fun shouldRenderAtSqrDistance(sqrDistance: Double): Boolean {
        var size = this.boundingBox.size * 10.0
        if (isNaN(size)) {
            size = 1.0
        }

        size *= 64.0 * getViewScale()
        return sqrDistance < size * size
    }

    private fun shouldRemove(player: Player): Boolean {
        val mainHand = player.mainHandItem.item === WORLD_AXE
        val offHand = player.offhandItem.item === WORLD_AXE
        return if (player.isAlive && (mainHand || offHand) && this.distanceToSqr(player) <= 100 * 100) {
            false
        } else {
            this.discard()
            true
        }
    }

    fun setzRot(zRot: Float) {
        this.entityData.set(ZROT, zRot)
    }

    fun getzRot(): Float {
        return this.entityData.get(ZROT)
    }
}