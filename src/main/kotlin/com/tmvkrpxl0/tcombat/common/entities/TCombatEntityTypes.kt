package com.tmvkrpxl0.tcombat.common.entities

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.entities.miscs.GroundChunk
import com.tmvkrpxl0.tcombat.base.common.util.getCapabilityMust
import com.tmvkrpxl0.tcombat.common.capabilities.ENTITY_HOLDER
import com.tmvkrpxl0.tcombat.common.entities.projectile.ReflectiveArrow
import com.tmvkrpxl0.tcombat.common.entities.projectile.SnipeArrow
import com.tmvkrpxl0.tcombat.common.entities.projectile.TNTArrow
import com.tmvkrpxl0.tcombat.common.entities.projectile.WorldAxe
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.WORLD_AXE
import net.minecraft.client.player.LocalPlayer
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.MobCategory
import net.minecraft.world.level.Level
import net.minecraft.world.phys.Vec3
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.registries.DeferredRegister
import net.neoforged.neoforge.registries.ForgeRegistries
import thedarkcolour.kotlinforforge.neoforge.forge.registerObject

object TCombatEntityTypes {
    private val ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.ENTITY_TYPES, MODID)

    val GROUND_CHUNK: EntityType<GroundChunk> by ENTITY_TYPES.registerObject("ground_chunk") {
        EntityType.Builder.of({ type: EntityType<GroundChunk>, level: Level ->
            GroundChunk(type, level)
        }, MobCategory.MISC).noSave().build("ground_chunk")
    }

    val tntArrow: EntityType<TNTArrow> by ENTITY_TYPES.registerObject("tnt_arrow") {
        EntityType.Builder.of({ tntArrowType: EntityType<TNTArrow>, level: Level ->
            TNTArrow(tntArrowType, level)
        }, MobCategory.MISC).sized(0.5f, 0.5f).build("tnt_arrow")
    }

    val snipeArrow: EntityType<SnipeArrow> by ENTITY_TYPES.registerObject("snipe_arrow") {
        EntityType.Builder.of({ snipeArrowType: EntityType<SnipeArrow>, level: Level ->
            SnipeArrow(snipeArrowType, level)
        }, MobCategory.MISC).sized(0.5f, 0.5f).build("snipe_arrow")
    }

    val worldAxe: EntityType<WorldAxe> by ENTITY_TYPES.registerObject("world_axe") {
        EntityType.Builder.of({ worldAxeType: EntityType<WorldAxe>, level: Level ->
            WorldAxe(worldAxeType, level)
        }, MobCategory.MISC).setCustomClientFactory { spawnEntity, level ->
            val player = level.getEntity(spawnEntity.additionalData.readVarInt()) as LocalPlayer
            var item = player.mainHandItem
            val compare = spawnEntity.additionalData.readItem()

            if (!item.equals(compare, true)) item = player.offhandItem
            require(item.item == WORLD_AXE)

            val entityHolder = item.getCapabilityMust(ENTITY_HOLDER)
            val worldAxe = WorldAxe(player, item)
            entityHolder.entity = worldAxe
            return@setCustomClientFactory worldAxe
        }.sized(0.05f, 0.05f).noSummon().noSave().fireImmune().updateInterval(1).build("world_axe")
    }

    val reflectiveArrow: EntityType<ReflectiveArrow> by ENTITY_TYPES.registerObject("reflective_arrow") {
        EntityType.Builder.of({ entityType: EntityType<ReflectiveArrow>, level: Level ->
            ReflectiveArrow(entityType, level)
        }, MobCategory.MISC).setCustomClientFactory { spawnEntity, level ->
            ReflectiveArrow(
                Vec3(spawnEntity.posX, spawnEntity.posY, spawnEntity.posZ), spawnEntity.additionalData.readFloat(), level
            )
        }.sized(0.5f, 0.5f).build("reflective_arrow")
    }

    fun register(bus: IEventBus) {
        ENTITY_TYPES.register(bus)
    }
}