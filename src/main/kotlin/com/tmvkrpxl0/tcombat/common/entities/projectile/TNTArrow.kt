package com.tmvkrpxl0.tcombat.common.entities.projectile

import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import com.tmvkrpxl0.tcombat.base.explosionImmune
import com.tmvkrpxl0.tcombat.common.entities.TCombatEntityTypes
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.TNT_ARROW

import net.minecraft.core.particles.ParticleTypes
import net.minecraft.network.protocol.Packet
import net.minecraft.network.protocol.game.ClientGamePacketListener
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.Level
import net.minecraft.world.phys.EntityHitResult
import net.minecraft.world.phys.HitResult
import net.minecraft.world.phys.Vec3
import net.neoforged.neoforge.network.NetworkHooks

class TNTArrow : AbstractArrow {
    private var explode = true
    private var explodeOn: Vec3? = null

    constructor(level: Level, x: Double, y: Double, z: Double) : super(
        TCombatEntityTypes.tntArrow, x, y, z, level
    )

    constructor(entityType: EntityType<TNTArrow>, level: Level) : super(entityType, level)

    constructor(entityType: EntityType<TNTArrow>, level: Level, shooter: LivingEntity) : super(
        entityType, shooter, level
    )


    override fun tick() {
        val level = level()
        if (level.isServerSide && explodeOn != null) {
            val location = explodeOn!!
            if (this.owner is LivingEntity) explosionImmune.add(this.owner as LivingEntity)
            level.explode(
                this, location.x, location.y, location.z, (if (this.isCritArrow) 4 else 2).toFloat(), this.isOnFire, Level.ExplosionInteraction.BLOCK
            )
            this.explodeOn = null
            this.discard()
        }
        super.tick()
        if (this.isInWaterRainOrBubble) explode = false
        if (explode) {
            if (this.level().isClientSide) {
                val lookVec = this.lookAngle.normalize().scale(0.35)
                level.addParticle(
                    ParticleTypes.SMOKE, this.x - lookVec.x, this.y - lookVec.y + 0.15, this.z - lookVec.z, 0.0, 0.0, 0.0
                )
            }
        }
    }

    override fun onHit(result: HitResult) {
        if (!(result.type == HitResult.Type.ENTITY && this.owner != null && this.owner == (result as EntityHitResult).entity)) {
            if (!level().isClientSide && explode && !this.isInWaterRainOrBubble) {
                explodeOn = result.location
            }
        }
        super.onHit(result)
    }

    override fun remove(pReason: RemovalReason) {
        if (this.explodeOn == null) super.remove(pReason)
    }

    override fun getPickupItem(): ItemStack {
        return ItemStack(TNT_ARROW)
    }

    override fun getAddEntityPacket(): Packet<ClientGamePacketListener> {
        return NetworkHooks.getEntitySpawningPacket(this)
    }
}