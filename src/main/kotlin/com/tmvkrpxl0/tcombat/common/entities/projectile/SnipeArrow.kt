package com.tmvkrpxl0.tcombat.common.entities.projectile


import com.tmvkrpxl0.tcombat.common.items.TCombatItems.TNT_ARROW
import net.minecraft.core.particles.BlockParticleOption
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.network.protocol.Packet
import net.minecraft.network.protocol.game.ClientGamePacketListener
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.ClipContext
import net.minecraft.world.level.Level
import net.minecraft.world.phys.BlockHitResult
import net.minecraft.world.phys.EntityHitResult
import net.minecraft.world.phys.HitResult
import net.minecraft.world.phys.Vec3
import net.neoforged.neoforge.entity.IEntityAdditionalSpawnData
import net.neoforged.neoforge.network.NetworkHooks
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.plus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.times

//TODO Do something about it being funky
class SnipeArrow : AbstractArrow, IEntityAdditionalSpawnData {
    constructor(entityType: EntityType<SnipeArrow>, level: Level) : super(entityType, level)
    constructor(entityType: EntityType<SnipeArrow>, shooter: LivingEntity, level: Level) : super(
        entityType, shooter, level
    )

    init {
        pickup = Pickup.CREATIVE_ONLY
    }

    private lateinit var originalMotion: Vec3

    override fun getPickupItem(): ItemStack {
        return ItemStack(TNT_ARROW)
    }

    override fun getAddEntityPacket(): Packet<ClientGamePacketListener> {
        return NetworkHooks.getEntitySpawningPacket(this)
    }

    override fun shoot(x: Double, y: Double, z: Double, velocity: Float, inaccuracy: Float) {
        originalMotion = Vec3(x, y, z) * velocity.toDouble()
        deltaMovement = Vec3(x, y, z) * 100.0

        val current = position()
        val end = current + deltaMovement
        val clipContext = ClipContext(current, end, ClipContext.Block.COLLIDER, ClipContext.Fluid.NONE, this)

        while (true) {
            val result = level().clip(clipContext)
            if (result.type != HitResult.Type.MISS && level().getBlockState(result.blockPos).block.defaultDestroyTime() <= 0.3) {
                level().destroyBlock(result.blockPos, false, this)
            } else break
        }

        this.markHurt()
    }

    override fun onHitEntity(result: EntityHitResult) {
        this.deltaMovement = originalMotion
        this.markHurt()
        super.onHitEntity(result)
    }

    override fun onHitBlock(result: BlockHitResult) {
        val state = this.level().getBlockState(result.blockPos)
        val location = result.location
        val normal = result.direction.opposite.normal

        val effectLocation = location + Vec3.atLowerCornerOf(normal)
        val level = level()

        level.playSound(
            null, effectLocation.x, effectLocation.y, effectLocation.z, state.soundType.breakSound, SoundSource.MASTER, 1F, 1F
        )

        if (level.isClientSide) {
            val particleDirection = deltaMovement.normalize().scale(2.0)
            val blockParticle = BlockParticleOption(ParticleTypes.BLOCK, state)
            repeat(128) {
                level.addParticle(
                    blockParticle,
                    true,
                    effectLocation.x,
                    effectLocation.y,
                    effectLocation.z,
                    particleDirection.x,
                    particleDirection.y,
                    particleDirection.z
                )
            }
        }

        super.onHitBlock(result)
    }

    override fun writeSpawnData(buffer: FriendlyByteBuf) {
        buffer.writeVec3(originalMotion)
    }

    override fun readSpawnData(additionalData: FriendlyByteBuf) {
        this.originalMotion = additionalData.readVec3()
    }
}
