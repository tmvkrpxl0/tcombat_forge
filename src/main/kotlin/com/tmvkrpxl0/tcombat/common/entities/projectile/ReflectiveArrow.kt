package com.tmvkrpxl0.tcombat.common.entities.projectile

import com.tmvkrpxl0.tcombat.LOGGER
import com.tmvkrpxl0.tcombat.base.common.util.ignoreNoDamageTicks
import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import com.tmvkrpxl0.tcombat.base.common.util.reflect
import com.tmvkrpxl0.tcombat.common.entities.TCombatEntityTypes
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.REFLECTIVE_ARROW
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.network.protocol.Packet
import net.minecraft.network.protocol.game.ClientGamePacketListener
import net.minecraft.network.syncher.EntityDataAccessor
import net.minecraft.network.syncher.EntityDataSerializers
import net.minecraft.network.syncher.SynchedEntityData
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.ClipContext
import net.minecraft.world.level.Level
import net.minecraft.world.phys.BlockHitResult
import net.minecraft.world.phys.EntityHitResult
import net.minecraft.world.phys.HitResult
import net.minecraft.world.phys.Vec3
import net.neoforged.neoforge.entity.IEntityAdditionalSpawnData
import net.neoforged.neoforge.network.NetworkHooks

class ReflectiveArrow : AbstractArrow, IEntityAdditionalSpawnData {
    private var totalDistance = 0.0
    var reflectCount = 0
        private set
    private var maxDistance: Float = -1F
        set(value) {
            require(field == -1F)
            field = value
        }
    private var from: Vec3 = Vec3.ZERO

    companion object {
        val STOP: EntityDataAccessor<Boolean> = SynchedEntityData.defineId(ReflectiveArrow::class.java, EntityDataSerializers.BOOLEAN)
    }

    constructor (entityType: EntityType<out ReflectiveArrow>, level: Level) : super(entityType, level)

    constructor(from: Vec3, maxDistance: Float, level: Level) : super(
        TCombatEntityTypes.reflectiveArrow, from.x, from.y, from.z, level
    ) {
        this.maxDistance = maxDistance
        this.from = from
    }

    constructor(
        shooter: LivingEntity, maxDistance: Float, level: Level
    ) : super(
        TCombatEntityTypes.reflectiveArrow, shooter, level
    ) {
        this.maxDistance = maxDistance
        this.from = shooter.eyePosition
    }

    init {
        if (this.level().isServerSide) this.ignoreNoDamageTicks = true
    }

    override fun defineSynchedData() {
        this.entityData.define(STOP, false)
        super.defineSynchedData()
    }

    private fun isStop(): Boolean = this.entityData.get(STOP)

    private fun stop() {
        require(this.level().isServerSide) { "Reflective Arrow should never be stopped on client side!" }
        if (!this.isStop()) LOGGER.info("Total: ${this.totalDistance}")
        this.entityData.set(STOP, true)
        this.isNoGravity = false
    }

    override fun tick() {
        if (this.level().isServerSide) {
            if (maxDistance == -1F) this.stop()
            if (this.isInWater) this.stop()
            this.totalDistance += from.distanceTo(this.position())
            from = this.position()
            if (this.totalDistance > maxDistance) this.stop()
        }
        if (!this.isStop()) {
            this.deltaMovement = this.deltaMovement.scale(1 / 0.99)
        }
        super.tick()
    }

    override fun onHitBlock(result: BlockHitResult) {
        if (!this.isStop() && this.level().isServerSide) {
            if (!this.isInWater) {
                if (this.totalDistance <= maxDistance) {
                    reflect(result)
                    return
                }
            }
        }
        super.onHitBlock(result)
    }

    override fun onHitEntity(result: EntityHitResult) {
        if (!this.isStop() && this.level().isServerSide) {
            if (result.entity == owner || (result.entity is LivingEntity && (result.entity as LivingEntity).isBlocking)) {
                this.deltaMovement = this.deltaMovement.scale(-1.0)
                return
            }
        }
        super.onHitEntity(result)
    }

    override fun getPickupItem(): ItemStack = ItemStack(REFLECTIVE_ARROW)

    override fun getAddEntityPacket(): Packet<ClientGamePacketListener> {
        return NetworkHooks.getEntitySpawningPacket(this)
    }

    private fun reflect(blockHitResult: BlockHitResult) {
        var tempResult = blockHitResult
        while (tempResult.type == HitResult.Type.BLOCK && this.totalDistance <= this.maxDistance) {
            reflectCount++
            this.level().playSound(null, this, SoundEvents.ARROW_HIT, SoundSource.NEUTRAL, 1F, 1F)
            val normal = tempResult.direction.normal
            val surface = Vec3(normal.x.toDouble(), normal.y.toDouble(), normal.z.toDouble())
            val reflected = this.deltaMovement.reflect(surface)
            this.deltaMovement = reflected
            this.totalDistance += from.distanceTo(tempResult.location)
            from = tempResult.location
            this.setPos(tempResult.location)

            val entityHitResult = findHitEntity(this.position(), this.position().add(this.deltaMovement))
            if (entityHitResult?.entity != null) {
                onHitEntity(entityHitResult)
                break
            }
            tempResult = this.level().clip(
                ClipContext(
                    this.position(), this.position().add(this.deltaMovement), ClipContext.Block.COLLIDER, ClipContext.Fluid.NONE, this
                )
            )
        }
        this.markHurt()
    }

    override fun writeSpawnData(buffer: FriendlyByteBuf) {
        buffer.writeFloat(maxDistance)
    }

    override fun readSpawnData(additionalData: FriendlyByteBuf?) { //Already being read on Custom Client factory
    }
}
