package com.tmvkrpxl0.tcombat.common.events

import net.minecraft.core.BlockPos
import net.minecraft.world.entity.player.Player
import net.minecraft.world.phys.Vec3
import net.neoforged.neoforge.event.entity.player.PlayerEvent

/**
 *
 * This even doesn't get fired if living entity slide/glide into wall
 * Doesn't work yet though
 */
class PlayerWalkingIntoWallEvent(
    player: Player, private val velocity: Vec3, //This cannot be received from server side
    private val blocks: ArrayList<BlockPos>
) : PlayerEvent(player) {
    fun getVelocity() = this.velocity
    fun getBlocks() = this.blocks
}