package com.tmvkrpxl0.tcombat.common.events

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.enchants.TCombatEnchants.AUTO_RELOAD
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.MOBILE_DISPENSER
import com.tmvkrpxl0.tcombat.common.items.misc.MobileDispenserItem
import net.minecraft.world.effect.MobEffects
import net.minecraft.world.item.BowItem
import net.minecraft.world.item.CrossbowItem
import net.minecraft.world.item.enchantment.EnchantmentHelper
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.Mod
import net.neoforged.neoforge.event.ItemAttributeModifierEvent
import net.neoforged.neoforge.event.entity.living.LivingEntityUseItemEvent
import java.lang.Integer.min

@Mod.EventBusSubscriber(
    modid = MODID, bus = Mod.EventBusSubscriber.Bus.FORGE, value = [Dist.CLIENT, Dist.DEDICATED_SERVER]
)
object TCombatCommonItemEventListener {
    @SubscribeEvent
    fun onUseTick(event: LivingEntityUseItemEvent.Start) {
        if (event.item.item is CrossbowItem || event.item.item is BowItem) {
            if (event.entity.hasEffect(MobEffects.DAMAGE_BOOST)) {
                val effectInstance = event.entity.getEffect(MobEffects.DAMAGE_BOOST)
                val amplifier = min(effectInstance!!.amplifier, 4)
                event.duration = event.duration - 3 * amplifier
            }
            if (event.entity.hasEffect(MobEffects.DIG_SPEED)) {
                val effectInstance = event.entity.getEffect(MobEffects.DIG_SPEED)
                val amplifier = min(effectInstance!!.amplifier, 4)
                event.duration = event.duration - 3 * amplifier
            }
        }
    }

    @SubscribeEvent
    fun onUse(event: LivingEntityUseItemEvent.Tick) {
        val pulledMax = when (event.item.item) {
            is CrossbowItem -> {
                event.duration <= 0
            }
            is BowItem -> {
                BowItem.getPowerForTime(event.item.useDuration - event.duration) >= 1.0F
            }
            else -> false
        }
        if (pulledMax) {
            val enchants = EnchantmentHelper.getEnchantments(event.item)
            if (enchants.containsKey(AUTO_RELOAD)) {
                event.isCanceled = true
                event.item.releaseUsing(event.entity.level(), event.entity, 0)
                event.entity.stopUsingItem()
            }
        }
    }

    @SubscribeEvent
    fun onAttributeModify(event: ItemAttributeModifierEvent) {
        when (event.itemStack.item) {
            MOBILE_DISPENSER -> MobileDispenserItem.onAttributeModify(event)
        }
    }
}