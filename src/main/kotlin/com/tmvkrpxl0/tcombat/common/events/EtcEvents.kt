package com.tmvkrpxl0.tcombat.common.events

import com.tmvkrpxl0.tcombat.base.explosionImmune
import com.tmvkrpxl0.tcombat.common.capabilities.IEntityHolder
import com.tmvkrpxl0.tcombat.common.entities.projectile.TNTArrow
import com.tmvkrpxl0.tcombat.common.skill.impl.ArrowSense
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.item.BucketItem
import net.minecraft.world.level.material.FlowingFluid
import net.neoforged.fml.LogicalSide
import net.neoforged.neoforge.common.SoundActions
import net.neoforged.neoforge.common.capabilities.RegisterCapabilitiesEvent
import net.neoforged.neoforge.event.entity.player.PlayerEvent
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent
import net.neoforged.neoforge.event.level.ExplosionEvent

fun registerCaps(event: RegisterCapabilitiesEvent) {
    event.register(IEntityHolder::class.java)
}

fun onLeakFluid(event: PlayerInteractEvent.LeftClickBlock) {
    if (event.side == LogicalSide.CLIENT) return
    val stack = event.itemStack
    if (stack.item is BucketItem) {
        if (event.face != null) {
            val level = event.level
            val blockPos = event.pos.offset(event.face!!.normal)
            val blockState = level.getBlockState(blockPos)
            val bucketItem = stack.item as BucketItem
            var fluid = bucketItem.fluid
            if (fluid is FlowingFluid) {

                fluid = fluid.flowing
                if (!event.entity.isShiftKeyDown) {
                    if (blockState.isAir || blockState.canBeReplaced(fluid)) {
                        level.setBlockAndUpdate(blockPos, fluid.defaultFluidState().createLegacyBlock())

                        val soundEvent = fluid.fluidType.getSound(event.entity, level, blockPos, SoundActions.BUCKET_EMPTY)
                            ?: if (fluid.fluidType.temperature >= 1000) SoundEvents.BUCKET_EMPTY_LAVA else SoundEvents.BOTTLE_EMPTY
                        level.playSound(event.entity, blockPos, soundEvent, SoundSource.PLAYERS, 1F, 1F)
                    }
                    event.isCanceled = true
                }
            }

        }
    }
}

fun excludeFromAffectedList(event: ExplosionEvent.Detonate) {
    event.affectedEntities.removeIf { entity -> entity is TNTArrow || explosionImmune.contains(entity) }
}

fun removeAlertWhenLeaving(event: PlayerEvent.PlayerLoggedOutEvent) {
    ArrowSense.alerting.remove(event.entity.uuid)
}