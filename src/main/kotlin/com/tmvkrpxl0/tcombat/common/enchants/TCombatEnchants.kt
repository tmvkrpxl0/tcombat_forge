package com.tmvkrpxl0.tcombat.common.enchants

import com.tmvkrpxl0.tcombat.MODID
import net.minecraft.world.item.enchantment.Enchantment
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.registries.DeferredRegister
import net.neoforged.neoforge.registries.ForgeRegistries
import thedarkcolour.kotlinforforge.neoforge.forge.registerObject

object TCombatEnchants {
    //TODO Maybe re-adjust enchant levels, and costs of those enchants?
    fun register(bus: IEventBus) {

        ENCHANTMENTS.register(bus)
    }

    private val ENCHANTMENTS = DeferredRegister.create(ForgeRegistries.ENCHANTMENTS, MODID)!!

    val FOCUS: Enchantment by ENCHANTMENTS.registerObject("focus") { FocusEnchantment }
    val CROSSBOW_FLAME: Enchantment by ENCHANTMENTS.registerObject("crossbow_flame") { CrossbowFlameEnchantment }
    val AUTO_RELOAD: Enchantment by ENCHANTMENTS.registerObject("auto_reload") { AutoReloadEnchantment }
}