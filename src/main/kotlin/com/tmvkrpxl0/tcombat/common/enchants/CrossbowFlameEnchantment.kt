package com.tmvkrpxl0.tcombat.common.enchants

import net.minecraft.world.entity.EquipmentSlot
import net.minecraft.world.item.enchantment.Enchantment
import net.minecraft.world.item.enchantment.EnchantmentCategory

object CrossbowFlameEnchantment : Enchantment(Rarity.RARE, EnchantmentCategory.CROSSBOW, arrayOf(EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND)) {
    override fun getMinCost(enchantmentLevel: Int): Int {
        return 20
    }

    override fun getMaxCost(enchantmentLevel: Int): Int {
        return 50
    }

    override fun getMaxLevel(): Int {
        return 1
    }
}