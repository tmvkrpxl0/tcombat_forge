package com.tmvkrpxl0.tcombat.common.enchants

import com.tmvkrpxl0.tcombat.mixins.bindings.common.CrossbowItemBinding
import net.minecraft.world.item.BowItem
import net.minecraft.world.item.CrossbowItem
import net.minecraft.world.item.enchantment.EnchantmentCategory

object TCombatEnchantmentCategories {
    val CROSSBOW_AND_BOW: EnchantmentCategory = EnchantmentCategory.create("CROSSBOW_AND_BOW") {
        if (it is CrossbowItem) {
            val mixin = it as CrossbowItemBinding
            mixin.test
        }
        return@create it is CrossbowItem || it is BowItem
    }
}