package com.tmvkrpxl0.tcombat.common.enchants

import net.minecraft.world.entity.EquipmentSlot
import net.minecraft.world.item.enchantment.Enchantment

object FocusEnchantment : Enchantment(
    Rarity.RARE,
    TCombatEnchantmentCategories.CROSSBOW_AND_BOW,
    arrayOf(EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND)
) {
    override fun getMinCost(enchantmentLevel: Int): Int {
        return 1 + 10 * (enchantmentLevel - 1)
    }

    override fun getMaxCost(enchantmentLevel: Int): Int {
        return super.getMinCost(enchantmentLevel) + 50
    }

    override fun getMaxLevel(): Int {
        return 1
    }
}