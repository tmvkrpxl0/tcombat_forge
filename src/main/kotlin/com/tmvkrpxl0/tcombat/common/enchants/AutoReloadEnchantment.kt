package com.tmvkrpxl0.tcombat.common.enchants

import net.minecraft.world.entity.EquipmentSlot
import net.minecraft.world.item.enchantment.Enchantment
import net.minecraft.world.item.enchantment.EnchantmentCategory

object AutoReloadEnchantment : Enchantment(Rarity.UNCOMMON, EnchantmentCategory.CROSSBOW, arrayOf(EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND)) {

    override fun getMaxLevel(): Int = 1
}