package com.tmvkrpxl0.tcombat.common.datagen

import net.minecraft.data.tags.VanillaBlockTagsProvider
import net.neoforged.neoforge.data.event.GatherDataEvent

fun setupDatagen(event: GatherDataEvent) {
    val fileHelper = event.existingFileHelper
    val generator = event.generator
    val output = generator.packOutput
    val lookupProvider = event.lookupProvider

    // Server
    generator.addProvider(event.includeServer(), ArrowTagsProvider(output, lookupProvider, VanillaBlockTagsProvider(output, lookupProvider).contentsGetter(), fileHelper))
    generator.addProvider(event.includeServer(), ItemRecipeProvider(output, lookupProvider))

    // Client
    generator.addProvider(event.includeClient(), MobileDispenserModelProvider(output, fileHelper))
}