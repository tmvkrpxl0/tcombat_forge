package com.tmvkrpxl0.tcombat.common.datagen

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.datagen.StructureModelBuilder.TransformEntry
import net.minecraft.data.PackOutput
import net.minecraft.resources.ResourceLocation
import net.minecraft.world.item.ItemDisplayContext
import net.neoforged.neoforge.client.model.generators.ItemModelBuilder.OverrideBuilder
import net.neoforged.neoforge.client.model.generators.ItemModelProvider
import net.neoforged.neoforge.client.model.generators.ModelFile
import net.neoforged.neoforge.common.data.ExistingFileHelper
import org.joml.Vector3f

class MobileDispenserModelProvider(
    output: PackOutput,
    existingFileHelper: ExistingFileHelper,
) : ItemModelProvider(
    output,
    MODID,
    existingFileHelper
) {
    private val unit = 1F
    private val startX = 0.0F
    private val startY = 0.0F
    private val startZ = 0.0F

    override fun registerModels() { //variant numbering = <firing><front_torch><back_torch><left>
        mobileDispenser(isFiring = true, frontTorch = true, backTorch = false, isLeft = false)
        mobileDispenser(isFiring = true, frontTorch = false, backTorch = false, isLeft = false)
        mobileDispenser(isFiring = true, frontTorch = false, backTorch = true, isLeft = false)

        mobileDispenser(isFiring = true, frontTorch = true, backTorch = false, isLeft = true)
        mobileDispenser(isFiring = true, frontTorch = false, backTorch = false, isLeft = true)
        mobileDispenser(isFiring = true, frontTorch = false, backTorch = true, isLeft = true)
        mobileDispenser(isFiring = false, frontTorch = false, backTorch = false, isLeft = true)

        mobileDispenser(isFiring = false, frontTorch = false, backTorch = false, isLeft = false, isMain = true)
    }

    private fun mobileDispenser(
        isFiring: Boolean, frontTorch: Boolean, backTorch: Boolean, isLeft: Boolean, isMain: Boolean = false
    ) {
        val builder =
            getBuilder(if (isMain) "mobile_dispenser" else "mobile_dispenser_variant${isFiring.char}${frontTorch.char}${backTorch.char}${isLeft.char}")
        val redstone = !isFiring || frontTorch || backTorch

        builder.transforms()

        val customLoader = builder.customLoader(StructureModelBuilder.Companion::begin)
        customLoader.apply { //back stone
            val transform = TransformEntry(
                translation = Vector3f(0F, 0F, 2F).fitTranslation(isLeft),
            )

            add(transform, nested().parent("minecraft:block/stone".resource().modelFile()))
        }.apply { //middle stone
            val transform = TransformEntry(
                translation = Vector3f(-1F, 0F, 1F).fitTranslation(isLeft),
            )
            add(transform, nested().parent("minecraft:block/stone".resource().modelFile()))
        }.apply { //dispenser
            val transform = TransformEntry(
                translation = Vector3f(0F, 0F, 0F).fitTranslation(isLeft),
            )
            add(transform, nested().parent("minecraft:block/dispenser".resource().modelFile()))
        }.apply { //redstone dust
            val transform = TransformEntry(
                translation = Vector3f(-0.00001F, 0F, 1F).fitTranslation(isLeft),
                rotation = Vector3f(0F, 0F, if (isLeft) 90F else -90F),
                origin = Vector3f(0.5F, 0.5F, 1.5F)
            )
            add(
                transform, nested().parent(
                    (if (!redstone) "minecraft:block/redstone_dust_dot"
                    else "tcombat:item/redstone_dust_dot_tint1").resource().modelFile()
                )
            )
        }.apply { //front torch
            val transform = TransformEntry(
                translation = Vector3f(-1F, 0F, 0F).fitTranslation(isLeft),
                rotation = Vector3f(-90F, if (isLeft) 0F else 180F, 90F),
                origin = Vector3f(ORIGIN_CENTER)
            )
            add(
                transform, nested().parent(
                    "minecraft:block/redstone_wall_torch${if (frontTorch) "" else "_off"}".resource().modelFile()
                )
            )
        }.apply { //back torch
            val transform = TransformEntry(
                translation = Vector3f(-1F, 0F, 2F).fitTranslation(isLeft),
                rotation = Vector3f(90F, if (isLeft) 0F else 180F, 90F),
                origin = Vector3f(ORIGIN_CENTER)
            )
            add(
                transform, nested().parent(
                    "minecraft:block/redstone_wall_torch${if (backTorch) "" else "_off"}".resource().modelFile()
                )
            )
        }.apply { //slab
            val transform = TransformEntry(
                translation = Vector3f(0F, 1F, 0F).fitTranslation(isLeft)
            )
            add(transform, nested().parent("minecraft:block/stone_slab".resource().modelFile()))
        }.apply { //"fence post but it's horizontal
            val transform = TransformEntry(
                translation = Vector3f(1F, 0.75F, 0F).fitTranslation(isLeft),
                rotation = Vector3f(0F, 0F, if (isLeft) 90F else -90F),
                origin = Vector3f(ORIGIN_CENTER)
            )
            add(transform, nested().parent("minecraft:block/oak_fence_post".resource().modelFile()))
        }.apply { //lever
            val transform = TransformEntry(
                translation = Vector3f(-1F, 1F, 1F).fitTranslation(isLeft)
            )
            add(
                transform, nested().parent("minecraft:block/lever${if (isFiring) "" else "_on"}".resource().modelFile())
            ) //You know, lever should be turned on in order to stop repeating redstone, thus not firing. so above if statement is valid
        }.end().apply { //item overrides
            if (isMain) {
                registerVariant(override(), isFiring = false, frontTorch = false, backTorch = false, isLeft = true)
                registerVariant(override(), isFiring = true, frontTorch = false, backTorch = false, isLeft = false)
                registerVariant(override(), isFiring = true, frontTorch = false, backTorch = false, isLeft = true)
                registerVariant(override(), isFiring = true, frontTorch = false, backTorch = true, isLeft = false)
                registerVariant(override(), isFiring = true, frontTorch = false, backTorch = true, isLeft = true)
                registerVariant(override(), isFiring = true, frontTorch = true, backTorch = false, isLeft = false)
                registerVariant(override(), isFiring = true, frontTorch = true, backTorch = false, isLeft = true) //Possible values for variants are:
                //1101
                //1100
                //1011
                //1010
                //1001
                //1000
                //0001
                //0000 (main)
            }
        }.transforms().apply {
            transform(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND).rotation(35F, 0F, 0F).scale(0.5F, 0.5F, 0.5F).translation(-8F, -7.5F, -3F)
                .end()

                .transform(ItemDisplayContext.THIRD_PERSON_LEFT_HAND).translation(-8F, -7.5F, -3F).rotation(35F, 0F, 0F)
                .scale(0.5F, 0.5F, 0.5F).end()

                .transform(ItemDisplayContext.FIXED).rotation(0F, -90F, 90F).translation(8F, 4F, -4F).scale(0.5F, 0.5F, 0.5F).end()

                .transform(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND).translation(0F, -16F, -9F).scale(0.75F, 0.75F, 0.75F).end()
                .transform(ItemDisplayContext.FIRST_PERSON_LEFT_HAND).translation(0F, -16F, -9F).scale(0.75F, 0.75F, 0.75F).end()

                .transform(ItemDisplayContext.GROUND).scale(0.4F, 0.4F, 0.4F).end()

                .transform(ItemDisplayContext.GUI).rotation(-139F, -29.25F, -180F).translation(1.35F, -3.2F, 0F).scale(0.34F, 0.34F, 0.34F)
                .end()
        }.end()
    }

    private fun registerVariant(
        builder: OverrideBuilder,
        isFiring: Boolean,
        frontTorch: Boolean,
        backTorch: Boolean,
        isLeft: Boolean,
    ) {
        var value = 0

        if (isLeft) value += 1
        if (backTorch) value += 10
        if (frontTorch) value += 100
        if (isFiring) value += 1000

        val formatted = String.format("%04d", value)
        builder.predicate("tcombat:mb_firing".resource(), if (isFiring) 1F else 0F)
            .predicate("tcombat:mb_front".resource(), if (frontTorch) 1F else 0F)
            .predicate("tcombat:mb_back".resource(), if (backTorch) 1F else 0F).predicate("lefthanded".resource(), if (isLeft) 1F else 0F)
            .model("tcombat:item/mobile_dispenser_variant$formatted".resource().modelFile()).end()
    }

    private fun String.resource(): ResourceLocation = ResourceLocation(this)

    private fun ResourceLocation.modelFile(): ModelFile = getExistingFile(this)

    private val Boolean.char: Char
        get() {
            return if (this) '1' else '0'
        }

    private fun Vector3f.fitTranslation(flipX: Boolean): Vector3f {
        val flipper = if (flipX) -1 else 1
        x = startX + x() * flipper * unit
        y = startY + y() * unit
        z = startZ + z() * unit
        return this
    }

    companion object {
        private val ORIGIN_CENTER = Vector3f(.5F, .5F, .5F)
    }
}