package com.tmvkrpxl0.tcombat.common.datagen

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.REFLECTIVE_ARROW
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.SNIPE_ARROW
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.TNT_ARROW
import net.minecraft.core.HolderLookup
import net.minecraft.data.PackOutput
import net.minecraft.data.tags.ItemTagsProvider
import net.minecraft.tags.ItemTags
import net.minecraft.world.level.block.Block
import net.neoforged.neoforge.common.data.ExistingFileHelper
import java.util.concurrent.CompletableFuture

class ArrowTagsProvider(
    output: PackOutput,
    holders: CompletableFuture<HolderLookup.Provider>,
    tags: CompletableFuture<TagLookup<Block>>,
    existingFileHelper: ExistingFileHelper?
) : ItemTagsProvider(output, holders, tags, MODID, existingFileHelper) {

    override fun addTags(lookupProvider: HolderLookup.Provider) {
        this.tag(ItemTags.ARROWS).add(TNT_ARROW, REFLECTIVE_ARROW, SNIPE_ARROW)
    }
}