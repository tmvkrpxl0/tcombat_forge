package com.tmvkrpxl0.tcombat.common.datagen

import com.tmvkrpxl0.tcombat.common.items.TCombatItems.MOBILE_DISPENSER
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.REFLECTIVE_ARROW
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.SNIPE_ARROW
import com.tmvkrpxl0.tcombat.common.items.TCombatItems.TNT_ARROW
import net.minecraft.core.HolderLookup
import net.minecraft.data.PackOutput
import net.minecraft.data.recipes.RecipeCategory
import net.minecraft.data.recipes.RecipeOutput
import net.minecraft.data.recipes.RecipeProvider
import net.minecraft.data.recipes.ShapedRecipeBuilder
import net.minecraft.tags.ItemTags
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.Item
import net.minecraft.world.item.Items
import java.util.concurrent.CompletableFuture

class ItemRecipeProvider(output: PackOutput, lookupProvider: CompletableFuture<HolderLookup.Provider>) : RecipeProvider(output, lookupProvider) {
    override fun buildRecipes(finishedRecipeConsumer: RecipeOutput) {
        arrow(finishedRecipeConsumer, TNT_ARROW as ArrowItem, Items.TNT)
        arrow(finishedRecipeConsumer, SNIPE_ARROW as ArrowItem, Items.REDSTONE)
        arrow(finishedRecipeConsumer, REFLECTIVE_ARROW as ArrowItem, Items.SLIME_BALL)

        ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, MOBILE_DISPENSER).define('s', Items.STONE).define('d', Items.DISPENSER).define('r', Items.REDSTONE)
            .define('t', Items.REDSTONE_TORCH).define('l', Items.STONE_SLAB)
            .define('f', ItemTags.WOODEN_FENCES).define('v', Items.LEVER).unlockedBy("has_dispenser", has(Items.DISPENSER))

            .pattern("lfv").pattern("drs").pattern("tst").save(finishedRecipeConsumer)
    }

    private fun arrow(finishedRecipeConsumer: RecipeOutput, resultArrow: ArrowItem, center: Item) {
        ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, resultArrow, 8)
            .define('a', Items.ARROW)
            .define('c', center)
            .pattern("aaa")
            .pattern("aca")
            .pattern("aaa")
            .unlockedBy("has_arrow", has(ItemTags.ARROWS))
            .save(finishedRecipeConsumer)
    }
}