package com.tmvkrpxl0.tcombat.common.items

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.items.misc.MobileDispenserItem
import com.tmvkrpxl0.tcombat.common.items.projectile.ReflectiveArrowItem
import com.tmvkrpxl0.tcombat.common.items.projectile.SnipeArrowItem
import com.tmvkrpxl0.tcombat.common.items.projectile.TNTArrowItem
import com.tmvkrpxl0.tcombat.common.items.weapon.WorldAxeItem
import net.minecraft.world.item.CreativeModeTabs
import net.minecraft.world.item.Item
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent
import net.neoforged.neoforge.registries.DeferredRegister
import net.neoforged.neoforge.registries.ForgeRegistries
import thedarkcolour.kotlinforforge.neoforge.forge.registerObject

object TCombatItems {
    private val ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MODID)

    val TNT_ARROW: Item by ITEMS.registerObject("tnt_arrow") { TNTArrowItem }
    val SNIPE_ARROW: Item by ITEMS.registerObject("snipe_arrow") { SnipeArrowItem }
    val REFLECTIVE_ARROW: Item by ITEMS.registerObject("reflective_arrow") { ReflectiveArrowItem }
    val WORLD_AXE: Item by ITEMS.registerObject("world_axe") { WorldAxeItem }
    val MOBILE_DISPENSER: Item by ITEMS.registerObject("mobile_dispenser") { MobileDispenserItem }

    fun register(bus: IEventBus) {
        ITEMS.register(bus)
    }

    fun buildContents(event: BuildCreativeModeTabContentsEvent) {
        when(event.tab) {
            CreativeModeTabs.COMBAT -> {
                event.accept(MOBILE_DISPENSER)
                event.accept(REFLECTIVE_ARROW)
                event.accept(SNIPE_ARROW)
                event.accept(TNT_ARROW)
                event.accept(WORLD_AXE)
            }
        }
    }
}
