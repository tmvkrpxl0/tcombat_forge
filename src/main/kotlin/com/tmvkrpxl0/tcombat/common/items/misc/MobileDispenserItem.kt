package com.tmvkrpxl0.tcombat.common.items.misc

import com.mojang.blaze3d.vertex.PoseStack
import com.tmvkrpxl0.tcombat.base.common.util.getCapabilityMust
import com.tmvkrpxl0.tcombat.base.common.util.ignoreNoDamageTicks
import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import com.tmvkrpxl0.tcombat.common.capabilities.impl.MobileDispenserInventory
import com.tmvkrpxl0.tcombat.common.containers.MobileDispenserMenu
import com.tmvkrpxl0.tcombat.mixins.bindings.common.DispenserAccessor
import net.minecraft.client.model.HumanoidModel
import net.minecraft.client.multiplayer.ClientLevel
import net.minecraft.client.player.LocalPlayer
import net.minecraft.core.Position
import net.minecraft.core.dispenser.AbstractProjectileDispenseBehavior
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.nbt.CompoundTag
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.InteractionHand
import net.minecraft.world.InteractionResultHolder
import net.minecraft.world.MenuProvider
import net.minecraft.world.entity.EquipmentSlot
import net.minecraft.world.entity.HumanoidArm
import net.minecraft.world.entity.HumanoidArm.LEFT
import net.minecraft.world.entity.HumanoidArm.RIGHT
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.ai.attributes.AttributeModifier
import net.minecraft.world.entity.ai.attributes.Attributes
import net.minecraft.world.entity.player.Inventory
import net.minecraft.world.entity.player.Player
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.inventory.AbstractContainerMenu
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.Item
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.ClipContext
import net.minecraft.world.level.Level
import net.minecraft.world.level.block.Blocks
import net.neoforged.fml.util.ObfuscationReflectionHelper
import net.neoforged.neoforge.client.extensions.common.IClientItemExtensions
import net.neoforged.neoforge.common.capabilities.Capabilities
import net.neoforged.neoforge.common.capabilities.ICapabilityProvider
import net.neoforged.neoforge.event.ItemAttributeModifierEvent
import java.lang.reflect.Method
import java.util.*
import java.util.function.Consumer

object MobileDispenserItem : Item(Properties().stacksTo(1)) {

    override fun onUseTick(level: Level, livingEntity: LivingEntity, stack: ItemStack, tick: Int) {
        val spawnAt = livingEntity.eyePosition.add(livingEntity.lookAngle.multiply(2.0, 2.0, 2.0))
        val clipContext =
            ClipContext(livingEntity.eyePosition, spawnAt, ClipContext.Block.COLLIDER, ClipContext.Fluid.NONE, null)
        val result = level.clip(clipContext)
        if (spawnAt != result.location) {
            if (level is ClientLevel) {
                level.playLocalSound(
                    result.location.x, result.location.y, result.location.z,
                    SoundEvents.DISPENSER_FAIL,
                    SoundSource.PLAYERS,
                    1F, 1.2F,
                    true
                )
            }
            return
        }

        val handler = stack.getCapability(Capabilities.ITEM_HANDLER).resolve().get() as MobileDispenserInventory
        val toDispense = handler
            .mapIndexed { index, itemStack -> index to itemStack }
            .filter { !it.second.isEmpty && isProjectile(it.second) }
            .randomOrNull()
        if (toDispense == null) {
            livingEntity.releaseUsingItem()
            return
        }

        if (level.isServerSide) {
            val behavior = (Blocks.DISPENSER as DispenserAccessor).getBehavior(toDispense.second)
            val copied = toDispense.second.copy()

            val projectileEntity = getProjectile(behavior, level, spawnAt, copied) as AbstractArrow
            val projectilePower = 2F * getPower(behavior) as Float
            val lookAngle = livingEntity.lookAngle
            projectileEntity.shoot(
                lookAngle.x, lookAngle.y, lookAngle.z, projectilePower, 0F
            )

            projectileEntity.owner = livingEntity
            projectileEntity.ignoreNoDamageTicks = true

            level.addFreshEntity(projectileEntity)

            if (!(livingEntity is Player && livingEntity.abilities.instabuild)) {
                copied.shrink(1)
                handler.setStackInSlot(toDispense.first, copied)
            }
        } else {
            val clientLevel = level as ClientLevel
            val pos = livingEntity.eyePosition.add(livingEntity.lookAngle)

            clientLevel.playLocalSound(
                pos.x,
                pos.y,
                pos.z,
                SoundEvents.DISPENSER_LAUNCH,
                SoundSource.PLAYERS,
                1F,
                1F,
                true
            )
            clientLevel.playLocalSound(
                pos.x,
                pos.y,
                pos.z,
                SoundEvents.REDSTONE_TORCH_BURNOUT,
                SoundSource.BLOCKS,
                0.5f,
                2.6f,
                true
            )
            clientLevel.addParticle(ParticleTypes.SMOKE, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0)
        }

    }

    override fun use(level: Level, player: Player, usedHand: InteractionHand): InteractionResultHolder<ItemStack> {
        val stack = player.getItemInHand(usedHand)
        val dispenserInventory = stack.getCapabilityMust(Capabilities.ITEM_HANDLER) as MobileDispenserInventory
        if (player.isUsingItem) return InteractionResultHolder.pass(stack)

        if (player.isShiftKeyDown) {
            if (level.isServerSide) player.openMenu(object : MenuProvider {
                override fun createMenu(
                    windowId: Int,
                    inventory: Inventory,
                    player: Player
                ): AbstractContainerMenu {
                    val container = MobileDispenserMenu(windowId, inventory, dispenserInventory)
                    if (player.level().isServerSide) {
                        dispenserInventory.forEachIndexed { index, itemStack ->
                            container.setItem(index, container.stateId, itemStack)
                        }
                    }
                    return container
                }

                override fun getDisplayName() = stack.displayName

            })
            return InteractionResultHolder.pass(stack)
        }

        val found = dispenserInventory.any { !it.isEmpty && isProjectile(it) }

        return if (found) {
            player.startUsingItem(usedHand)
            level.playSound(null, player, SoundEvents.LEVER_CLICK, SoundSource.PLAYERS, 0.5F, 0.6F)
            InteractionResultHolder.consume(stack)
        } else {
            level.playSound(null, player, SoundEvents.DISPENSER_FAIL, SoundSource.PLAYERS, 1F, 1.2F)
            InteractionResultHolder.fail(stack)
        }
    }

    override fun getUseDuration(mobileDispenser: ItemStack): Int {
        return 72000
    }

    override fun initCapabilities(stack: ItemStack, nbt: CompoundTag?): ICapabilityProvider {
        return MobileDispenserInventory.Provider(9)
    }

    private val getProjectile: Method = ObfuscationReflectionHelper.findMethod(
        AbstractProjectileDispenseBehavior::class.java,
        "getProjectile",
        Level::class.java,
        Position::class.java,
        ItemStack::class.java
    )
    private val getPower: Method = ObfuscationReflectionHelper.findMethod(
        AbstractProjectileDispenseBehavior::class.java, "getPower"
    )

    override fun releaseUsing(pStack: ItemStack, level: Level, entity: LivingEntity, timeCharged: Int) {
        if (level.isClientSide) {
            val pos = entity.position()
            entity.level().playLocalSound(
                pos.x, pos.y, pos.z, SoundEvents.LEVER_CLICK, SoundSource.PLAYERS, 0.5F, 0.5F, true
            )
        }
    }

    override fun initializeClient(consumer: Consumer<IClientItemExtensions>) {
        consumer.accept(object: IClientItemExtensions {
            private val pose: HumanoidModel.ArmPose = HumanoidModel.ArmPose.create("TCOMBAT_MOBILE_DISPENSER", true) { model, entity, arm ->
                if (entity.mainArm != arm) return@create
                when(arm!!) {
                    LEFT -> {
                        val firing = entity.isUsingItem
                        model.rightArm.xRot = if (firing) -0.7853982F else 0.7853982F //if (firing) -5 degree else 45 degree
                        model.rightArm.yRot = if (firing) 0.6981317F else -0.6981317F
                        model.leftArm.xRot = model.head.xRot - 0.7853982F
                        model.leftArm.yRot = 0F
                    }
                    RIGHT -> {
                        val firing = entity.isUsingItem
                        model.leftArm.xRot = if (firing) -0.7853982F else 0.7853982F //-45 or 45
                        model.leftArm.yRot = if (firing) -0.6981317F else 0.6981317F //-40 or 40
                        model.rightArm.xRot = model.head.xRot - 0.7853982F
                        model.rightArm.yRot = 0F
                    }
                }
            }

            override fun getArmPose(
                entityLiving: LivingEntity?,
                hand: InteractionHand?,
                itemStack: ItemStack?
            ) = pose

            override fun applyForgeHandTransform(
                poseStack: PoseStack?,
                player: LocalPlayer?,
                arm: HumanoidArm?,
                itemInHand: ItemStack?,
                partialTick: Float,
                equipProcess: Float,
                swingProcess: Float
            ): Boolean {
                return true
            }
        })
    }

    private val SLOW_MODIFIER_UUID = UUID.fromString("B6C7411F-15E2-48F6-B53F-876EC0577E82") // :) easter egg

    fun onAttributeModify(event: ItemAttributeModifierEvent) {
        if (event.slotType == EquipmentSlot.MAINHAND || event.slotType == EquipmentSlot.OFFHAND) {
            val slow = AttributeModifier(SLOW_MODIFIER_UUID, "Mobile dispenser slow", -0.04, AttributeModifier.Operation.ADDITION)
            event.addModifier(Attributes.MOVEMENT_SPEED, slow)
        }
    }

    fun isProjectile(projectile: ItemStack): Boolean = projectile.item is ArrowItem
}