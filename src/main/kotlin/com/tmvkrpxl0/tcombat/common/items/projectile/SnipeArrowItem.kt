package com.tmvkrpxl0.tcombat.common.items.projectile

import com.tmvkrpxl0.tcombat.common.entities.TCombatEntityTypes
import com.tmvkrpxl0.tcombat.common.entities.projectile.SnipeArrow
import net.minecraft.core.Position
import net.minecraft.core.dispenser.AbstractProjectileDispenseBehavior
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.entity.projectile.Projectile
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.Level
import net.minecraft.world.level.block.DispenserBlock

object SnipeArrowItem : ArrowItem(Properties()) {
    override fun createArrow(level: Level, stack: ItemStack, shooter: LivingEntity): AbstractArrow =
        SnipeArrow(TCombatEntityTypes.snipeArrow, shooter, level)

    init {
        DispenserBlock.registerBehavior(this, object : AbstractProjectileDispenseBehavior() {
            override fun getProjectile(level: Level, position: Position, itemStack: ItemStack): Projectile {
                val snipe = SnipeArrow(TCombatEntityTypes.snipeArrow, level)
                snipe.setPos(position.x(), position.y(), position.z())
                return snipe
            }
        })
    }
}
