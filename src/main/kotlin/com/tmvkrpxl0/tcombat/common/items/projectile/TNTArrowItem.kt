package com.tmvkrpxl0.tcombat.common.items.projectile

import com.tmvkrpxl0.tcombat.common.entities.TCombatEntityTypes
import com.tmvkrpxl0.tcombat.common.entities.projectile.TNTArrow
import net.minecraft.core.Position
import net.minecraft.core.dispenser.AbstractProjectileDispenseBehavior
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.entity.projectile.Projectile
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.Level
import net.minecraft.world.level.block.DispenserBlock

object TNTArrowItem : ArrowItem(Properties()) {

    override fun createArrow(level: Level, stack: ItemStack, shooter: LivingEntity): AbstractArrow =
        TNTArrow(TCombatEntityTypes.tntArrow, level, shooter)

    init {
        DispenserBlock.registerBehavior(this, object : AbstractProjectileDispenseBehavior() {
            override fun getProjectile(worldIn: Level, position: Position, stackIn: ItemStack): Projectile =
                TNTArrow(worldIn, position.x(), position.y(), position.z())
        })
    }
}