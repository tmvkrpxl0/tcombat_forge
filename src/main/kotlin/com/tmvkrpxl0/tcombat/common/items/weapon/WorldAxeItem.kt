package com.tmvkrpxl0.tcombat.common.items.weapon

import com.tmvkrpxl0.tcombat.base.common.util.getCapabilityMust
import com.tmvkrpxl0.tcombat.common.capabilities.ENTITY_HOLDER
import com.tmvkrpxl0.tcombat.common.capabilities.impl.EntityHolder
import com.tmvkrpxl0.tcombat.common.entities.projectile.WorldAxe
import net.minecraft.nbt.CompoundTag
import net.minecraft.sounds.SoundEvents
import net.minecraft.world.InteractionHand
import net.minecraft.world.InteractionResultHolder
import net.minecraft.world.entity.player.Player
import net.minecraft.world.item.AxeItem
import net.minecraft.world.item.ItemStack
import net.minecraft.world.item.Tiers
import net.minecraft.world.level.Level
import net.neoforged.neoforge.common.capabilities.ICapabilityProvider

object WorldAxeItem : AxeItem(
    Tiers.NETHERITE, 5.0f, -3.0f, Properties().fireResistant()
) {
    override fun use(level: Level, player: Player, hand: InteractionHand): InteractionResultHolder<ItemStack> {
        val itemStack = player.getItemInHand(hand)
        val entityHolder = itemStack.getCapabilityMust(ENTITY_HOLDER)
        val entity: WorldAxe? = entityHolder.entity as WorldAxe?
        if (entity != null) {
            entity.discard()
            entityHolder.entity = null
            player.playSound(SoundEvents.FISHING_BOBBER_RETRIEVE, 1f, 1f)

            //TODO Pull here
            if (entity.inGround || entity.hooked != null) {
            } else {

            }
        } else {
            if (!level.isClientSide) {
                val worldAxe = WorldAxe(player, itemStack)
                worldAxe.isNoGravity = true
                worldAxe.deltaMovement = player.lookAngle.scale(0.5)
                worldAxe.hurtMarked = true
                entityHolder.entity = worldAxe
                player.level().addFreshEntity(worldAxe)
            }
        }
        return InteractionResultHolder.success(itemStack)
    }

    override fun initCapabilities(stack: ItemStack, nbt: CompoundTag?): ICapabilityProvider {
        return EntityHolder.Provider(null)
    }
}