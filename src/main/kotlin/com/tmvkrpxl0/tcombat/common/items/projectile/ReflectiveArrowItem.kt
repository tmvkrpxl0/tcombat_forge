package com.tmvkrpxl0.tcombat.common.items.projectile

import com.tmvkrpxl0.tcombat.common.entities.projectile.ReflectiveArrow
import net.minecraft.core.Position
import net.minecraft.core.dispenser.AbstractProjectileDispenseBehavior
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.entity.projectile.Projectile
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.Level
import net.minecraft.world.level.block.DispenserBlock
import net.minecraft.world.phys.Vec3

object ReflectiveArrowItem : ArrowItem(Properties()) {
    override fun createArrow(level: Level, stack: ItemStack, shooter: LivingEntity): AbstractArrow = ReflectiveArrow(shooter, 50F, level)

    init {
        DispenserBlock.registerBehavior(this, object : AbstractProjectileDispenseBehavior() {
            override fun getProjectile(level: Level, position: Position, itemStack: ItemStack): Projectile {
                val reflect = ReflectiveArrow(Vec3(position.x(), position.y(), position.z()), 50F, level)
                reflect.setPos(position.x(), position.y(), position.z())
                return reflect
            }
        })
    }
}