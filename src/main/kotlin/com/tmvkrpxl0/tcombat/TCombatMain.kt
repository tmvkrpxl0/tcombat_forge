package com.tmvkrpxl0.tcombat

import com.tmvkrpxl0.tcombat.base.common.events.attachSkillData
import com.tmvkrpxl0.tcombat.base.common.events.registerBaseCaps
import com.tmvkrpxl0.tcombat.base.common.network.TCombatPacketHandler
import com.tmvkrpxl0.tcombat.base.common.skill.createSkillRegistry
import com.tmvkrpxl0.tcombat.base.common.skill.tickAutoExecution
import com.tmvkrpxl0.tcombat.base.common.util.TCombatDataSerializers
import com.tmvkrpxl0.tcombat.client.renderers.autoBridgePreview
import com.tmvkrpxl0.tcombat.client.renderers.setAutoBridgePreview
import com.tmvkrpxl0.tcombat.common.datagen.setupDatagen
import com.tmvkrpxl0.tcombat.common.enchants.TCombatEnchants
import com.tmvkrpxl0.tcombat.common.entities.TCombatEntityTypes
import com.tmvkrpxl0.tcombat.common.events.excludeFromAffectedList
import com.tmvkrpxl0.tcombat.common.events.onLeakFluid
import com.tmvkrpxl0.tcombat.common.events.registerCaps
import com.tmvkrpxl0.tcombat.common.events.removeAlertWhenLeaving
import com.tmvkrpxl0.tcombat.common.items.TCombatItems
import com.tmvkrpxl0.tcombat.common.network.packets.ParticleLinePacket
import com.tmvkrpxl0.tcombat.common.skill.TCombatCustomSkills
import net.minecraft.network.syncher.EntityDataSerializers
import net.minecraft.server.network.ServerGamePacketListenerImpl
import net.neoforged.fml.common.Mod
import net.neoforged.fml.loading.FMLEnvironment
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import thedarkcolour.kotlinforforge.neoforge.forge.FORGE_BUS
import thedarkcolour.kotlinforforge.neoforge.forge.MOD_BUS

// The value here should match an entry in the META-INF/mods.toml file
const val MODID = "tcombat"
val LOGGER: Logger = LogManager.getLogger("tmvkrpxl0's combat mod")
val trackPackets = !FMLEnvironment.production

@Mod(MODID)
object TCombatMain {

    init {
        TCombatPacketHandler.register(listOf { buffer -> ParticleLinePacket.decode(buffer) })

        createSkillRegistry(MOD_BUS)
        TCombatCustomSkills.register(MOD_BUS)
        TCombatEntityTypes.register(MOD_BUS)
        TCombatItems.register(MOD_BUS)
        TCombatEnchants.register(MOD_BUS)

        EntityDataSerializers.registerSerializer(TCombatDataSerializers.UNIQUE_ID)
        EntityDataSerializers.registerSerializer(TCombatDataSerializers.BLOCK_STATE)
        EntityDataSerializers.registerSerializer(TCombatDataSerializers.FLUID_STATE)

        MOD_BUS.addListener(::setupDatagen)
        FORGE_BUS.addListener(::tickAutoExecution)
        FORGE_BUS.addListener(::attachSkillData)
        FORGE_BUS.addListener(::registerBaseCaps)
        FORGE_BUS.addListener(::registerCaps)
        FORGE_BUS.addListener(::onLeakFluid)
        FORGE_BUS.addListener(::excludeFromAffectedList)
        FORGE_BUS.addListener(::removeAlertWhenLeaving)
        FORGE_BUS.addListener(::autoBridgePreview)
        FORGE_BUS.addListener(::setAutoBridgePreview)
        FORGE_BUS.addListener(TCombatItems::buildContents)

        LOGGER.info("TCombat activated!")

        ServerGamePacketListenerImpl::class.java
    }
}