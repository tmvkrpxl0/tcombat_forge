package com.tmvkrpxl0.tcombat.mixins.client

import com.mojang.blaze3d.platform.GlStateManager
import com.mojang.blaze3d.systems.RenderSystem
import com.tmvkrpxl0.tcombat.client.fadeLevel
import org.lwjgl.opengl.GL14

fun onRender() {
    RenderSystem.enableBlend()
    RenderSystem.blendFunc(
        GlStateManager.SourceFactor.CONSTANT_ALPHA,
        GlStateManager.DestFactor.ONE_MINUS_CONSTANT_ALPHA
    )
    var alpha = 1f - (fadeLevel / 10f)
    if (alpha < 0.2f) alpha = 0.2f
    if (alpha > 1f) alpha = 1f
    alpha *= alpha //square it
    GL14.glBlendColor(0f, 0f, 0f, alpha) //I don't think I should be using opengl function directly...
}