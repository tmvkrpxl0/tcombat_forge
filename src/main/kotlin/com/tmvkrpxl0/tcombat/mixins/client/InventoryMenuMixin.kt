package com.tmvkrpxl0.tcombat.mixins.client

import com.tmvkrpxl0.tcombat.common.items.misc.MobileDispenserItem
import net.minecraft.world.item.ItemStack
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable

fun addCondition(itemStack: ItemStack, ci: CallbackInfoReturnable<Boolean>) {
    if (itemStack.item is MobileDispenserItem) {
        ci.returnValue = false //Implicit canceling
    }
}