package com.tmvkrpxl0.tcombat.mixins.client

import com.tmvkrpxl0.tcombat.client.fadeLevel
import net.minecraft.client.gui.screens.Screen
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen
import net.minecraft.util.FastColor

fun modifyRenderBackground(screen: Screen, original: Int): Int {
    return if (screen is AbstractContainerScreen<*>) {
        val multiplier = 1f - fadeLevel / 10f
        if (original == -1072689136) {
            FastColor.ARGB32.color((multiplier * 192).toInt(), 16, 16, 16)
        } else {
            FastColor.ARGB32.color((multiplier * 208).toInt(), 16, 16, 16)
        }
    } else original
}