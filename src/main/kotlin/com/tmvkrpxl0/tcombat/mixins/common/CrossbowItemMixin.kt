package com.tmvkrpxl0.tcombat.mixins.common

import com.tmvkrpxl0.tcombat.common.enchants.TCombatEnchants
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.ItemStack


fun narrowMultiShot(
    angle: Float,
    crossbow: ItemStack,
): Float {
    return if (crossbow.getEnchantmentLevel(TCombatEnchants.FOCUS) != 0) 0.0F else angle
}

fun setArrowOnFire(
    crossbowStack: ItemStack,
    arrow: AbstractArrow
) {
    if (crossbowStack.getEnchantmentLevel(TCombatEnchants.CROSSBOW_FLAME) != 0) arrow.setSecondsOnFire(100)
}