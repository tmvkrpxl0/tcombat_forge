package com.tmvkrpxl0.tcombat.mixins.common

import com.tmvkrpxl0.tcombat.common.items.misc.MobileDispenserItem
import net.minecraft.network.protocol.game.ServerboundPlayerActionPacket
import net.minecraft.server.level.ServerPlayer
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo

fun disallowMB(ci: CallbackInfo, player: ServerPlayer, action: ServerboundPlayerActionPacket.Action) {
    if (action != ServerboundPlayerActionPacket.Action.SWAP_ITEM_WITH_OFFHAND) return
    if (player.isSpectator) return
    val main = player.mainHandItem
    if (main.item !is MobileDispenserItem) return
    ci.cancel()
}