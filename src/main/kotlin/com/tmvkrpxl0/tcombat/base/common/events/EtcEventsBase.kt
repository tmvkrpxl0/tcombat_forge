package com.tmvkrpxl0.tcombat.base.common.events

import com.tmvkrpxl0.tcombat.base.common.capabilities.IPlayerTargetStatus
import com.tmvkrpxl0.tcombat.base.common.saveddata.SkillData.Companion.skillData
import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import net.neoforged.neoforge.common.capabilities.RegisterCapabilitiesEvent
import net.neoforged.neoforge.event.entity.player.PlayerEvent

fun attachSkillData(event: PlayerEvent.PlayerLoggedInEvent) {
    require(event.entity.level().isServerSide)

    skillData
}

fun registerBaseCaps(event: RegisterCapabilitiesEvent) {
    event.register(IPlayerTargetStatus::class.java)
}