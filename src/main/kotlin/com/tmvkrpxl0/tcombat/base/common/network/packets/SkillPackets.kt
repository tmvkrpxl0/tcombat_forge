package com.tmvkrpxl0.tcombat.base.common.network.packets

import com.tmvkrpxl0.tcombat.base.common.network.packets.ITCombatPacket.Companion.readVector4f
import com.tmvkrpxl0.tcombat.base.common.network.packets.ITCombatPacket.Companion.writeVector4f
import com.tmvkrpxl0.tcombat.base.common.skill.*
import net.minecraft.client.Minecraft
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.server.level.ServerPlayer
import net.neoforged.fml.LogicalSide
import net.neoforged.neoforge.network.NetworkEvent
import org.joml.Vector4f

@Packet(receiveSide = LogicalSide.SERVER)
class RequestSkillExecutionPacket(val skill: Skill) : ITCombatPacket {
    companion object {
        fun decode(buffer: FriendlyByteBuf) = RequestSkillExecutionPacket(buffer.readRegistryId())
    }

    override fun encode(buffer: FriendlyByteBuf) {
        buffer.writeRegistryId(SKILL_REGISTRY, skill)
    }

    override fun handle(context: NetworkEvent.Context) {
        context.enqueueWork {
            require(context.sender is ServerPlayer)
            val player = context.sender!!
            skill.tryExecute(player)
        }
    }
}

@Packet(receiveSide = LogicalSide.CLIENT)
class ReceiveSkillAutoExecutionPacket(private val skill: Skill, private val isEnable: Boolean) : ITCombatPacket {
    companion object {
        fun decode(buffer: FriendlyByteBuf): ReceiveSkillAutoExecutionPacket {
            return ReceiveSkillAutoExecutionPacket(buffer.readRegistryId(), buffer.readBoolean())
        }
    }

    override fun encode(buffer: FriendlyByteBuf) {
        buffer.writeRegistryId(SKILL_REGISTRY, skill)
        buffer.writeBoolean(isEnable)
    }

    override fun handle(context: NetworkEvent.Context) {
        context.enqueueWork {
            val player = Minecraft.getInstance().player!!
            if (isEnable) player startAutoExecuting skill
            else player stopAutoExecuting skill
        }
    }
}

@Packet(receiveSide = LogicalSide.SERVER)
class RequestAutoExecutePacket(private val skill: Skill, private val isEnable: Boolean) : ITCombatPacket {
    companion object {
        fun decode(buffer: FriendlyByteBuf): RequestAutoExecutePacket {
            return RequestAutoExecutePacket(buffer.readRegistryId(), buffer.readBoolean())
        }
    }

    override fun encode(buffer: FriendlyByteBuf) {
        buffer.writeRegistryId(SKILL_REGISTRY, skill)
        buffer.writeBoolean(isEnable)
    }

    override fun handle(context: NetworkEvent.Context) {
        context.enqueueWork {
            if (isEnable) context.sender!! startAutoExecuting skill
            else context.sender!! stopAutoExecuting skill
        }
    }
}

// When name of Long contains `end`, it means system time in ticks when the cooldown will end
// If it doesn't, it means how many ticks left before cooldown ends
@Packet(receiveSide = LogicalSide.CLIENT)
class ReceiveCooldownEndPacket(private val skill: Skill, private val endTime: CooldownEndTime) : ITCombatPacket {
    companion object {
        fun decode(buffer: FriendlyByteBuf): ReceiveCooldownEndPacket {
            return ReceiveCooldownEndPacket(buffer.readRegistryIdSafe(Skill::class.java), buffer.readLong().endTime())
        }
    }

    override fun encode(buffer: FriendlyByteBuf) {
        buffer.writeRegistryId(SKILL_REGISTRY, skill)
        buffer.writeLong(endTime.endTime)
    }

    override fun handle(context: NetworkEvent.Context) {
        context.enqueueWork {
            val player = Minecraft.getInstance().player!!
            skill.playerSkillCooldownEndTimes[player] = if (endTime.endTime != -1L) endTime else null
        }
    }
}

@Packet(receiveSide = LogicalSide.CLIENT)
class SkillAlertPacket(private val skill: Skill, private val tint: Vector4f?) : ITCombatPacket {
    companion object {
        fun decode(buffer: FriendlyByteBuf): SkillAlertPacket {
            val skill = buffer.readRegistryIdSafe(Skill::class.java)

            var tint: Vector4f? = buffer.readVector4f()
            if (tint == Vector4f(-1F, -1F, -1F, -1F)) tint = null

            return SkillAlertPacket(skill, tint)
        }
    }

    override fun encode(buffer: FriendlyByteBuf) {
        buffer.writeRegistryId(SKILL_REGISTRY, skill)
        buffer.writeVector4f(tint ?: Vector4f(-1F, -1F, -1F, -1F))
    }

    override fun handle(context: NetworkEvent.Context) {
        context.enqueueWork {
            skill.alertColor = tint
        }
    }
}