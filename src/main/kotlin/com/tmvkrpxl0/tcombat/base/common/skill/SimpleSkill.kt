package com.tmvkrpxl0.tcombat.base.common.skill

import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.player.Player

abstract class SimpleSkill: Skill<Unit>() {
    override fun canExecute(player: Player): Unit? {
        if (!checkCooldown(player, this)) {
            return null
        }
        return Unit
    }

    override fun execute(player: ServerPlayer, extra: Unit) {
        execute(player)
    }

    abstract fun execute(player: ServerPlayer)
}