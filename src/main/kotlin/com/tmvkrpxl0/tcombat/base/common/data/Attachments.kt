package com.tmvkrpxl0.tcombat.base.common.data

import com.mojang.serialization.Codec
import com.mojang.serialization.DataResult
import com.tmvkrpxl0.tcombat.base.common.skill.SKILL_REGISTRY
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.attachment.AttachmentType
import net.neoforged.neoforge.registries.DeferredRegister
import net.neoforged.neoforge.registries.NeoForgeRegistries
import java.util.function.Supplier

private val ATTACHMENTS = DeferredRegister.create(NeoForgeRegistries.ATTACHMENT_TYPES, com.tmvkrpxl0.tcombat.base.MODID)

val TARGET: Supplier<AttachmentType<Target>> = ATTACHMENTS.register("target", Supplier {
    AttachmentType.builder(::Target).build()
})

val AUTO_EXECUTE: Supplier<AttachmentType<ObjectOpenHashSet<Skill<*>>>> = ATTACHMENTS.register("auto_execute", Supplier {
    AttachmentType.builder(Supplier { ObjectOpenHashSet<Skill<*>>() })
        .serialize(SKILL_REGISTRY.byNameCodec().listOf().comapFlatMap(
            { DataResult.success(ObjectOpenHashSet(it)) },
            { it.toList() }
        ))
        .build()
})

val COOLDOWNS: Supplier<AttachmentType<HashMap<Skill<*>, Long>>> = ATTACHMENTS.register("cooldowns", Supplier {
    AttachmentType.builder(Supplier { HashMap<Skill<*>, Long>() })
        .serialize(
            Codec.unboundedMap(SKILL_REGISTRY.byNameCodec(), Codec.LONG)
                .xmap(
                    { HashMap(it) },
                    { it }
                )
        ).build()
})

fun registerAttachments(bus: IEventBus) {
    ATTACHMENTS.register(bus)
}