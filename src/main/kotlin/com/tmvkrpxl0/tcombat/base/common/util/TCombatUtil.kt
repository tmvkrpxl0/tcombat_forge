package com.tmvkrpxl0.tcombat.base.common.util

import com.google.common.primitives.Doubles
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.capabilities.IPlayerTargetStatus
import net.minecraft.core.Direction
import net.minecraft.network.chat.Component
import net.minecraft.resources.ResourceLocation
import net.minecraft.world.entity.Entity
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.player.Player
import net.minecraft.world.level.Level
import net.minecraft.world.phys.AABB
import net.minecraft.world.phys.Vec3
import net.neoforged.neoforge.common.capabilities.Capability
import net.neoforged.neoforge.common.capabilities.ICapabilityProvider
import org.joml.*
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.minus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.times
import java.lang.Math
import kotlin.math.acos

//Note for myself
//The way it works is simple: It is server that does all sort of skill, avoid, defend, etc...
//Client will also hold some information about those, but it's only for displaying information to player
//Which means, it is not required for clients to hold any sort of information


fun Entity.getAngleTo(to: Entity): Double {
    val sourcePosition = this.eyePosition
    val targetPosition = to.position()
    val difference = targetPosition - sourcePosition
    return Math.toDegrees(this.lookAngle.angle(difference).toDouble())
}

fun Vec3.reflect(surface: Vec3): Vec3 {
    val normal = surface.normalize()
    val dot2 = this.dot(normal) * 2
    return this - normal * dot2
}

var Player.pickMode: IPlayerTargetStatus.PickMode
    get() = this.targetInfo.pickMode
    set(value) {
        this.targetInfo.pickMode = value
    }

var Player.focus: LivingEntity?
    get() = this.targetInfo.focused
    set(value) {
        this.targetInfo.focused = value
    }

fun Vec3.angle(other: Vec3): Float {
    val dot = Doubles.constrainToRange(this.dot(other) / (this.length() * other.length()), -1.0, 1.0)
    return acos(dot).toFloat()
}

val Level.isServerSide: Boolean
    get() = !this.isClientSide

/**
 * Test collision between 2 AABB using swept aabb method
 * Passed AABBs should not be at origin
 *
 * @param moving AABB that is moving
 * @param static AABB that is static
 * @param velocity velocity of moving AABB
 * @return Value between 0 and 1. Returns 1 when there wasn't collision
 *
 */
fun testSweptAABB(moving: AABB, static: AABB, velocity: Vec3): Double {
    val mp = moving.run { Vec3(minX, minY, minZ) }
    val sp = static.run { Vec3(minX, minY, minZ) }
    val m = moving.move(mp.scale(-1.0))
    val s = static.move(sp.scale(-1.0))
    return testSweptAABB(m, s, velocity, mp, sp)
}

/**
 * Test collision between 2 AABB using swept aabb method
 * Passed AABBs must be at origin
 *
 * @param moving AABB that is moving
 * @param static AABB that is static
 * @param velocity velocity of moving AABB
 * @param movingPos initial position of moving AABB
 * @param staticPos initial position of static AABB
 * @return Value between 0 and 1. Returns 1 when there wasn't collision
 *
 */
fun testSweptAABB(moving: AABB, static: AABB, velocity: Vec3, movingPos: Vec3, staticPos: Vec3): Double {

    val dxEntry = staticPos.x - movingPos.x + (if (velocity.x > 0) -moving.xsize else static.xsize)
    val dxExit = staticPos.x - movingPos.x + (if (velocity.x > 0) static.xsize else -moving.xsize)

    val dyEntry = staticPos.y - movingPos.y + (if (velocity.y > 0) -moving.ysize else static.ysize)
    val dyExit = staticPos.y - movingPos.y + (if (velocity.y > 0) static.ysize else -moving.ysize)

    val dzEntry = staticPos.z - movingPos.z + (if (velocity.z > 0) -moving.zsize else static.zsize)
    val dzExit = staticPos.z - movingPos.z + (if (velocity.z > 0) static.zsize else -moving.zsize)

    val txEntry = if (velocity.x == 0.0) Double.NEGATIVE_INFINITY else dxEntry / velocity.x
    val txExit = if (velocity.x == 0.0) Double.POSITIVE_INFINITY else dxExit / velocity.x

    val tyEntry = if (velocity.y == 0.0) Double.NEGATIVE_INFINITY else dyEntry / velocity.y
    val tyExit = if (velocity.y == 0.0) Double.POSITIVE_INFINITY else dyExit / velocity.y

    val tzEntry = if (velocity.z == 0.0) Double.NEGATIVE_INFINITY else dzEntry / velocity.z
    val tzExit = if (velocity.z == 0.0) Double.POSITIVE_INFINITY else dzExit / velocity.z

    val entryTime = maxOf(txEntry, tyEntry, tzEntry)
    val exitTime = minOf(txExit, tyExit, tzExit)

    if (entryTime > exitTime || (txEntry < 0.0 && tyEntry < 0.0 && tzEntry < 0.0) || txEntry > 1.0 || tyEntry > 1.0 || tzEntry > 1.0) {
        return 1.0
    }
    return entryTime
}

fun String.toResource(): ResourceLocation = ResourceLocation(MODID, this)

private const val DEGREE_TO_RADIAN = 0.017453292f
private const val RADIAN_TO_DEGREE = 57.29578f
fun Float.toRadian() = this * DEGREE_TO_RADIAN
fun Float.toDegree() = this * RADIAN_TO_DEGREE
fun Double.toRadian() = Math.toRadians(this)
fun Double.toDegree() = Math.toDegrees(this)

infix fun Vector3f.axisAngle(radian: Float) = AxisAngle4f(radian, this)
infix fun Vector3d.axisAngle(radian: Double) = AxisAngle4d(radian, this)
infix fun Vector3f.quaternion(radian: Float) = Quaternionf(this axisAngle radian)
infix fun Vector3d.quaternion(radian: Double) = Quaterniond(this axisAngle radian)

fun <T> ICapabilityProvider.getCapabilityMust(cap: Capability<T>, side: Direction? = null): T {
    val fetched = getCapability(cap, side)
    require(fetched.isPresent)
    return fetched.resolve().get()
}

fun String.toComponent(): Component {
    return Component.literal(this)
}
