package com.tmvkrpxl0.tcombat.base.common.network

import com.tmvkrpxl0.tcombat.LOGGER
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.network.packets.*
import com.tmvkrpxl0.tcombat.trackPackets
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.resources.ResourceLocation
import net.neoforged.neoforge.network.NetworkEvent
import net.neoforged.neoforge.network.NetworkRegistry
import net.neoforged.neoforge.network.simple.SimpleChannel
import java.lang.instrument.IllegalClassFormatException
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.hasAnnotation

object TCombatPacketHandler {
    private const val PROTOCOL_VERSION = "1"
    private const val CHANNEL_NAME = "tcombat_packet_handler"
    private var id = 0
    val INSTANCE: SimpleChannel = NetworkRegistry.newSimpleChannel(
        ResourceLocation(MODID, CHANNEL_NAME),
        { PROTOCOL_VERSION },
        { o: String -> PROTOCOL_VERSION == o }) { o: String ->
        PROTOCOL_VERSION == o
    }

    @Suppress("INACCESSIBLE_TYPE")
    internal inline fun <reified T : ITCombatPacket> registerPacket(noinline decoder: (buffer: FriendlyByteBuf) -> T) {
        val packetClass = T::class
        if (!packetClass.hasAnnotation<Packet>()) {
            throw IllegalClassFormatException("TCombat Packets must be annotated by Packet annotation!")
        }

        INSTANCE.registerMessage(id, packetClass.java,
            { p: T, buffer: FriendlyByteBuf -> p.encode(buffer) },
            decoder,
            { p: T, context: NetworkEvent.Context ->
                require(context.direction.receptionSide == packetClass.findAnnotation<Packet>()!!.receiveSide)
                { "${packetClass.simpleName} can't be received on ${context.direction.receptionSide}" }

                if (trackPackets) LOGGER.info("Received packet ${packetClass.simpleName}")
                p.handle(context)
                context.packetHandled = true
            }
        )
        id++
    }

    internal inline fun <reified T : ITCombatPacket> register(otherPackets: List<(buffer: FriendlyByteBuf) -> T>) {
        registerPacket { buffer -> RequestSkillExecutionPacket.decode(buffer) }
        registerPacket { buffer -> TargetRequestPacket.decode(buffer) }
        registerPacket { buffer -> TargetModifyNotifyPacket.Mode.decode(buffer) }
        registerPacket { buffer -> TargetModifyNotifyPacket.Targets.decode(buffer) }
        registerPacket { buffer -> TargetModifyNotifyPacket.Focus.decode(buffer) }
        registerPacket { buffer -> ReceiveSkillAutoExecutionPacket.decode(buffer) }
        registerPacket { buffer -> PlayerWalkingIntoWallPacket.decode(buffer) }
        registerPacket { buffer -> SkillAlertPacket.decode(buffer) }
        registerPacket { buffer -> ReceiveCooldownEndPacket.decode(buffer) }
        registerPacket { buffer -> RequestAutoExecutePacket.decode(buffer) }
        otherPackets.forEach { registerPacket(it) }
    }
}