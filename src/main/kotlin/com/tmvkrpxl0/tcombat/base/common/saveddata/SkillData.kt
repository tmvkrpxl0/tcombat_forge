package com.tmvkrpxl0.tcombat.base.common.saveddata

import com.tmvkrpxl0.tcombat.LOGGER
import com.tmvkrpxl0.tcombat.base.common.skill.CooldownEndTime
import com.tmvkrpxl0.tcombat.base.common.skill.SKILL_REGISTRY
import net.minecraft.nbt.CompoundTag
import net.minecraft.resources.ResourceLocation
import net.minecraft.server.level.ServerLevel
import net.minecraft.world.level.saveddata.SavedData
import net.neoforged.neoforge.server.ServerLifecycleHooks
import java.util.*

class SkillData : SavedData() {
    companion object {
        private val overworld: ServerLevel
            get() {
                return ServerLifecycleHooks.getCurrentServer().overworld()
            }

        val skillData: SkillData
            get() {
                return overworld.dataStorage.computeIfAbsent(Factory(::SkillData, ::deserialize), "skill_data")
            }

        private fun deserialize(tag: CompoundTag): SkillData {
            val data = SkillData()
            val currentTime = overworld.gameTime

            for (tagKey in tag.allKeys) {
                val skillName = ResourceLocation(tagKey)
                val entry = tag.getCompound(tagKey)

                val skill = SKILL_REGISTRY.getValue(skillName)
                if (skill == null) {
                    LOGGER.warn("Skill data for unknown skill $skillName detected! ignoring..")
                    continue
                }

                entry.allKeys.forEach { uuidString ->
                    val uuid = UUID.fromString(uuidString)
                    val endTime = entry.getLong(uuidString)

                    if (currentTime < endTime) {
                        skill.uuidToCooldownMap[uuid] = CooldownEndTime(entry.getLong(uuidString))
                    }
                }
            }
            return data
        }
    }

    override fun save(compoundTag: CompoundTag): CompoundTag {
        val currentTime = overworld.gameTime
        SKILL_REGISTRY.forEach { skill ->
            val entry = CompoundTag()
            skill.uuidToCooldownMap.forEach { (uuid, endTime) ->
                if (currentTime < endTime.endTime) {
                    entry.putLong(uuid.toString(), endTime.endTime)
                }
            }
            compoundTag.put(skill.skillName.toString(), entry)
        }
        return compoundTag
    }
}