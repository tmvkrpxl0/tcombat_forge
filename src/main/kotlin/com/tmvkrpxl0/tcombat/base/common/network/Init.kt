package com.tmvkrpxl0.tcombat.base.common.network

import com.tmvkrpxl0.tcombat.base.MODID
import com.tmvkrpxl0.tcombat.base.client.network.TargetNotifications
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import net.minecraft.network.RegistryFriendlyByteBuf
import net.minecraft.network.codec.StreamCodec
import net.minecraft.network.protocol.common.custom.CustomPacketPayload
import net.minecraft.resources.ResourceLocation
import net.neoforged.neoforge.network.event.RegisterPayloadHandlersEvent
import net.neoforged.neoforge.network.handling.IPayloadHandler

internal interface CompanionPayload<P: CustomPacketPayload> {
    val type: CustomPacketPayload.Type<P>
    val codec: StreamCodec<in RegistryFriendlyByteBuf, P>

    fun String.toPayloadType() = CustomPacketPayload.Type<P>(this.toResource())
}

internal fun registerPackets(event: RegisterPayloadHandlersEvent) {
    val registrar = event.registrar(MODID)

    ExecuteSkill.register(registrar::playToServer, ExecuteSkill::handle)
    AutoExecuteSkill.register(registrar::playBidirectional, AutoExecuteSkill::handle)
    SetCooldown.register(registrar::playToClient, SetCooldown::handle)
    Alert.register(registrar::playToClient, Alert::handle)
    TargetControl.register(registrar::playToServer, TargetControl::handle)
    TargetNotifications.Targets.register(registrar::playToClient, TargetNotifications.Targets::handle)
    TargetNotifications.Primary.register(registrar::playToClient, TargetNotifications.Primary::handle)
}

private fun <P: CustomPacketPayload> CompanionPayload<P>.register(f: (CustomPacketPayload.Type<P>, StreamCodec<in RegistryFriendlyByteBuf, P>, IPayloadHandler<P>) -> Unit, handler: IPayloadHandler<P>) {
    f(this.type, this.codec, handler)
}