package com.tmvkrpxl0.tcombat.base.common.util

import net.minecraft.world.phys.Vec3
import org.joml.Vector3d

fun Vec3.toVector3d() = Vector3d(x, y, z)