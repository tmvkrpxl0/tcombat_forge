package com.tmvkrpxl0.tcombat.base.common.util

import net.minecraft.world.entity.projectile.AbstractArrow
import net.neoforged.fml.LogicalSide

private val instantArrows = HashSet<AbstractArrow>()

/** Should only be invoked on logical server
 */
fun tickInstantArrow(logicalSide: LogicalSide) {
    require(logicalSide == LogicalSide.SERVER)
    instantArrows.removeIf { e: AbstractArrow -> e.onGround() || !e.isAlive }
}

/** Should only be used on logical server
 */
var AbstractArrow.ignoreNoDamageTicks: Boolean
    get() {
        require(this.level().isServerSide) { "This should not be used on client side!" }
        return instantArrows.contains(this)
    }
    set(value) {
        require(this.level().isServerSide) { "This should not be used on client side!" }
        if (value) instantArrows.add(this)
        else instantArrows.remove(this)
    }