package com.tmvkrpxl0.tcombat.base.common.network.packets

import com.tmvkrpxl0.tcombat.common.events.PlayerWalkingIntoWallEvent
import net.minecraft.core.BlockPos
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.world.entity.player.Player
import net.minecraft.world.phys.Vec3
import net.neoforged.fml.LogicalSide
import net.neoforged.neoforge.network.NetworkEvent
import net.neoforged.neoforge.server.ServerLifecycleHooks

//This packet is for PlayerWalkingIntoWallEvent, but it won't work yet
@Packet(receiveSide = LogicalSide.SERVER)
class PlayerWalkingIntoWallPacket(
    private val player: Player, private val originalVelocity: Vec3, private val blocks: ArrayList<BlockPos>
) : ITCombatPacket {


    companion object {
        fun decode(buffer: FriendlyByteBuf): PlayerWalkingIntoWallPacket {
            val player = ServerLifecycleHooks.getCurrentServer().playerList.getPlayer(buffer.readUUID())!!
            val originalVelocity = buffer.readVec3()
            val size = buffer.readVarInt()
            val blocks = ArrayList<BlockPos>()
            for (i in 0 until size) {
                blocks.add(buffer.readBlockPos())
            }
            return PlayerWalkingIntoWallPacket(player, originalVelocity, blocks)
        }
    }

    constructor(event: PlayerWalkingIntoWallEvent) : this(event.entity, event.entity.deltaMovement, event.getBlocks())

    override fun encode(buffer: FriendlyByteBuf) {
        buffer.writeUUID(this.player.uuid)
        buffer.writeVec3(this.originalVelocity)
        buffer.writeVarInt(this.blocks.size)
        this.blocks.forEach { buffer.writeBlockPos(it) }
    }

    override fun handle(context: NetworkEvent.Context) {/*
        val context = supplier.get()
        context.enqueueWork {
            //Validation, to prevent client lying about collision
            //It's just basically doing the same thing and seeing if it got same result as client
            val level = this.player.level as ServerLevel
            val origin = this.player.boundingBox
            val moved = origin.move(originalVelocity)
            val combined = origin.minmax(moved)
            val blocks = ArrayList<BlockPos>(result.count().toInt())
            result.forEach { blockPos ->
                blocks.add(blockPos)
            }
            if(blocks.eequals(this.blocks)){
                PlayerWalkingIntoWallEvent(this.player, this.originalVelocity, this.blocks)
            }
        }
         */
    }
}