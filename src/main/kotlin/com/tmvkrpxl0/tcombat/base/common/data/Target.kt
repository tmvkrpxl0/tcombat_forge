package com.tmvkrpxl0.tcombat.base.common.data

import net.minecraft.world.entity.LivingEntity
import java.util.*

data class Target(
    var primary: LivingEntity? = null,
    var targets: MutableList<LivingEntity> = LinkedList()
)