package com.tmvkrpxl0.tcombat.base.common.skill

import com.tmvkrpxl0.tcombat.base.LOGGER
import com.tmvkrpxl0.tcombat.base.MODID
import com.tmvkrpxl0.tcombat.base.common.network.Alert
import com.tmvkrpxl0.tcombat.base.common.util.Color
import com.tmvkrpxl0.tcombat.base.common.util.autoExecutes
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import net.minecraft.core.Registry
import net.minecraft.resources.ResourceLocation
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.player.Player
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.event.tick.ServerTickEvent
import net.neoforged.neoforge.registries.DeferredRegister
import java.util.*

private val SKILLS: DeferredRegister<Skill<*>> = DeferredRegister.create("skill_registry".toResource(),
    MODID
)


val SKILL_REGISTRY: Registry<Skill<*>> = SKILLS.makeRegistry {
    LOGGER.info("Creating tcombat registry...")
}

internal fun createSkillRegistry(bus: IEventBus) {
    SKILLS.register(bus)
}

fun tickAutoExecution(event: ServerTickEvent.Pre) {
    event.server.playerList.players.forEach { player ->
        player.autoExecutes.forEach { skill ->
            if (player cooldownOf skill == 0L) skill.autoExecute(player)
        }
    }
}

fun Skill<*>.alertPlayer(player: ServerPlayer, alertColor: Color) {
    player.connection.send(Alert(this, Optional.of(alertColor)))
}

abstract class Skill<T> {
    abstract val skillName: ResourceLocation

    val translationKey by lazy { "skill.${skillName.namespace}.${skillName.path}" }

    open val cooldownTick = 0L

    val skillTextureLocation: ResourceLocation
        get() = skillName

    open fun shouldShowInOverlay(player: Player) = true

    fun tryExecute(player: ServerPlayer): Boolean {
        val extra = canExecute(player) ?: return false

        execute(player, extra)
        player.setSkillCooldown(this, cooldownTick)
        return true
    }

    abstract fun canExecute(player: Player): T?

    open fun autoExecute(player: ServerPlayer): Boolean {
        return tryExecute(player)
    }

    abstract fun execute(player: ServerPlayer, extra: T)
}
