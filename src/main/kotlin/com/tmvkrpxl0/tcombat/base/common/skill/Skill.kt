package com.tmvkrpxl0.tcombat.base.common.skill

import com.tmvkrpxl0.tcombat.LOGGER
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.network.packets.ReceiveCooldownEndPacket
import com.tmvkrpxl0.tcombat.base.common.network.packets.ReceiveSkillAutoExecutionPacket
import com.tmvkrpxl0.tcombat.base.common.network.packets.SkillAlertPacket
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import net.minecraft.resources.ResourceLocation
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.player.Player
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.event.TickEvent
import net.neoforged.neoforge.network.PacketDistributor
import net.neoforged.neoforge.registries.DeferredRegister
import net.neoforged.neoforge.registries.IForgeRegistry
import net.neoforged.neoforge.registries.RegistryBuilder
import org.joml.Vector4f
import java.util.*

@JvmInline
value class CooldownEndTime(val endTime: Long)

fun Long.endTime() = CooldownEndTime(this)


private val SKILLS: DeferredRegister<Skill> = DeferredRegister.create("skill_registry".toResource(), MODID)


val SKILL_REGISTRY: IForgeRegistry<Skill> by SKILLS.makeRegistry {
    LOGGER.info("Making tcombat registry...")
    val builder = RegistryBuilder<Skill>()
    builder.onCreate { _, _ ->
        LOGGER.info("TCombat Skill registry has been created!")
    }
}.run { lazy { get() } }

internal fun createSkillRegistry(bus: IEventBus) {
    SKILLS.register(bus)
}

fun tickAutoExecution(event: TickEvent.ServerTickEvent) {
    if (event.phase != TickEvent.Phase.START) return

    SKILL_REGISTRY.forEach { skill ->
        skill.autoExecuteFor.forEach { player ->
            if (player cooldownOf skill <= 0) skill.autoExecute(player as ServerPlayer)
        }
    }
}

abstract class Skill {
    abstract val skillName: ResourceLocation

    val translationKey by lazy { "${skillName.namespace}.skill.${skillName.path}" }

    open val skillCooldownTick = 0L

    internal val uuidToCooldownMap = HashMap<UUID, CooldownEndTime>()

    internal val playerSkillCooldownEndTimes = object : PlayerToCooldownEndTimeMap {

        override operator fun get(player: Player): CooldownEndTime? {
            return uuidToCooldownMap[player.uuid]
        }

        override operator fun set(player: Player, endTime: CooldownEndTime?) {
            if (endTime == null) {
                uuidToCooldownMap.remove(player.uuid)
            } else {
                uuidToCooldownMap[player.uuid] = endTime
            }

            if (player is ServerPlayer) {
                ReceiveCooldownEndPacket(this@Skill, endTime ?: (-1L).endTime()).sendTo(player)
            }
        }

    }

    internal val autoExecuteFor = object : HashSet<Player>() {
        val skillAccess by lazy { this@Skill }
        override fun add(element: Player): Boolean {
            if (element is ServerPlayer) {
                ReceiveSkillAutoExecutionPacket(skillAccess, true).send(PacketDistributor.PLAYER.with { element })
            }
            return super.add(element)
        }

        override fun remove(element: Player): Boolean {
            if (element is ServerPlayer) {
                ReceiveSkillAutoExecutionPacket(skillAccess, false).send(PacketDistributor.PLAYER.with { element })
            }
            return super.remove(element)
        }
    }

    val skillTextureLocation: ResourceLocation
        get() = skillName

    // @OnlyIn(Dist.CLIENT)
    // Client specific value; each client will have different value for tint
    // This shouldn't be used in server
    var alertColor: Vector4f? = null

    open fun shouldShowInOverlay(player: Player) = true

    fun alertPlayer(player: ServerPlayer, alertColor: Vector4f?) {
        SkillAlertPacket(this, alertColor).sendTo(player)
    }

    abstract val predicates: List<SkillPredicate>
    fun tryExecute(player: ServerPlayer): Boolean {
        for (predicate in predicates) {
            if (!predicate.check(player, this)) {
                onPredicateFail(player, predicate)
                return false
            } else onPredicateSuccess(player, predicate)
        }

        return if (!execute(player)) {
            onExecutionFail(player)
            false
        } else {
            player.setSkillCooldown(this, skillCooldownTick)
            true
        }
    }

    open fun autoExecute(player: ServerPlayer): Boolean {
        return tryExecute(player)
    }

    open fun onPredicateFail(player: ServerPlayer, whatFailed: SkillPredicate) {

    }

    open fun onPredicateSuccess(player: ServerPlayer, whatSucceed: SkillPredicate) {

    }

    open fun onExecutionFail(player: ServerPlayer) {

    }

    abstract fun execute(player: ServerPlayer): Boolean

}

interface PlayerToCooldownEndTimeMap {
    operator fun get(player: Player): CooldownEndTime?

    operator fun set(player: Player, endTime: CooldownEndTime?)
}


