package com.tmvkrpxl0.tcombat.base.common.util

import com.tmvkrpxl0.tcombat.base.MODID
import net.minecraft.network.codec.ByteBufCodecs
import net.minecraft.network.codec.StreamCodec
import net.minecraft.network.syncher.EntityDataSerializer
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.registries.DeferredRegister
import net.neoforged.neoforge.registries.NeoForgeRegistries
import thedarkcolour.kotlinforforge.neoforge.forge.getValue
import java.util.function.Supplier
import kotlin.experimental.or

object TCombatDataSerializers {
    private val SERIALIZERS = DeferredRegister.create(NeoForgeRegistries.ENTITY_DATA_SERIALIZERS, MODID)

    val COLOR: EntityDataSerializer<Color> by SERIALIZERS.register("color", Supplier {
        object: EntityDataSerializer<Color> {
            val codec = StreamCodec.composite(
                ByteBufCodecs.BYTE, Color::r,
                ByteBufCodecs.BYTE, Color::g,
                ByteBufCodecs.BYTE, Color::b,
                ByteBufCodecs.BYTE, Color::a,
                ::Color
            )

            override fun copy(value: Color): Color {
                return value.copy()
            }

            override fun codec() = codec
        }
    })

    fun register(bus: IEventBus) {
        SERIALIZERS.register(bus)
    }
}

data class Color(val r: Byte, val g: Byte, val b: Byte, val a: Byte) {
    fun isEmpty() = r.or(g).or(b).or(a) == 0.toByte()
}
