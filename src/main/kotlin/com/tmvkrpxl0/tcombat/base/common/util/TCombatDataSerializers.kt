package com.tmvkrpxl0.tcombat.base.common.util

import net.minecraft.network.FriendlyByteBuf
import net.minecraft.network.syncher.EntityDataSerializer
import net.minecraft.world.level.block.Block
import net.minecraft.world.level.block.state.BlockState
import net.minecraft.world.level.material.FluidState
import java.util.*

object TCombatDataSerializers {
    val UNIQUE_ID: EntityDataSerializer<UUID> = object : EntityDataSerializer<UUID> {
        override fun write(buf: FriendlyByteBuf, value: UUID) {
            buf.writeUUID(value)
        }

        override fun read(buf: FriendlyByteBuf): UUID {
            return buf.readUUID()
        }

        override fun copy(value: UUID): UUID {
            return value
        }
    }
    val BLOCK_STATE: EntityDataSerializer<BlockState> = object : EntityDataSerializer<BlockState> {
        override fun write(buf: FriendlyByteBuf, value: BlockState) {
            buf.writeVarInt(Block.getId(value))
        }

        override fun read(buf: FriendlyByteBuf): BlockState {
            val i = buf.readVarInt()
            return Block.stateById(i)
        }

        override fun copy(value: BlockState): BlockState {
            return value
        }
    }
    val FLUID_STATE: EntityDataSerializer<FluidState> = object : EntityDataSerializer<FluidState> {
        override fun write(buf: FriendlyByteBuf, value: FluidState) {
            buf.writeVarInt(Block.getId(value.createLegacyBlock()))
        }

        override fun read(buf: FriendlyByteBuf): FluidState {
            val i = buf.readVarInt()
            return Block.stateById(i).fluidState
        }

        override fun copy(value: FluidState): FluidState {
            return value
        }
    }
}
