package com.tmvkrpxl0.tcombat.base.common.util

import com.tmvkrpxl0.tcombat.base.common.capabilities.IPlayerTargetStatus
import com.tmvkrpxl0.tcombat.base.common.capabilities.TARGET_HOLDER
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.player.Player
import net.minecraft.world.phys.AABB

val Player.targetInfo: IPlayerTargetStatus
    get() {
        require(this.isAlive) { "Cannot get Target Holder of dead player!" }
        return this.getCapability(TARGET_HOLDER).resolve().get()
    }
private val AABB_100: AABB = AABB(-50.0, -50.0, -50.0, 50.0, 50.0, 50.0)

fun setTargetsAuto(player: Player) {
    val degree = 60
    val targets =
        player.level().getEntities(player, AABB_100.move(player.position())).filterIsInstance<LivingEntity>().stream().limit(100).filter { entity ->
            player.hasLineOfSight(entity) && player.getAngleTo(entity) < degree
        }.toList()
    player.targetInfo.changeTargets(targets)
    if (player.level().isServerSide) player.targetInfo.sendChange(player as ServerPlayer)
}

fun Player.pickFocus() {
    when (this.pickMode) {
        IPlayerTargetStatus.PickMode.CLOSE -> {
            this.targetInfo.targets.reduce { acc, livingEntity ->
                if (acc.distanceToSqr(this) > livingEntity.distanceToSqr(this)) livingEntity else acc
            }
        }
        IPlayerTargetStatus.PickMode.LOOK -> {
            this.targetInfo.targets.reduce { acc, livingEntity ->
                if (this.getAngleTo(acc) > this.getAngleTo(livingEntity)) livingEntity else acc
            }
        }
        IPlayerTargetStatus.PickMode.RANDOM -> {
            this.targetInfo.targets.random()
        }
    }
    if (this.level().isServerSide) this.targetInfo.sendChange(this as ServerPlayer)
}