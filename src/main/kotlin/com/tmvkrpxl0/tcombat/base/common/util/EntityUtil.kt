package com.tmvkrpxl0.tcombat.base.common.util

import com.tmvkrpxl0.tcombat.base.client.network.TargetNotifications
import com.tmvkrpxl0.tcombat.base.common.data.AUTO_EXECUTE
import com.tmvkrpxl0.tcombat.base.common.data.COOLDOWNS
import com.tmvkrpxl0.tcombat.base.common.data.TARGET
import com.tmvkrpxl0.tcombat.base.common.data.Target
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.player.Player
import net.minecraft.world.phys.AABB

val Player.targetInfo: Target
    get() = getData(TARGET)

val Player.autoExecutes: MutableSet<Skill<*>>
    get() = getData(AUTO_EXECUTE)

val Player.skillCooldowns: MutableMap<Skill<*>, Long>
    get() = getData(COOLDOWNS)

private val AABB_100: AABB = AABB(-50.0, -50.0, -50.0, 50.0, 50.0, 50.0)

fun ServerPlayer.pickTargets() {
    val degree = 60
    val targets =
        this.level().getEntities(this, AABB_100.move(this.position())).filterIsInstance<LivingEntity>().take(100).filter { entity ->
            this.hasLineOfSight(entity) && this.getAngleTo(entity) < degree
        }.toMutableList()
    this.targetInfo.targets = targets
    
    this.connection.send(TargetNotifications.Targets(targets))
}

fun ServerPlayer.pickFocus() {
    val primary = targetInfo.targets.minBy { getAngleTo(it) }
    targetInfo.primary = primary
    connection.send(TargetNotifications.Primary(primary))
}