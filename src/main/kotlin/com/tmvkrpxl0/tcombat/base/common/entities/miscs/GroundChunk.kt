package com.tmvkrpxl0.tcombat.base.common.entities.miscs

import com.tmvkrpxl0.tcombat.base.client.renderers.FakeLevel
import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import com.tmvkrpxl0.tcombat.common.entities.TCombatEntityTypes
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.RenderType
import net.minecraft.client.renderer.block.model.BakedQuad
import net.minecraft.core.BlockPos
import net.minecraft.core.Direction
import net.minecraft.nbt.CompoundTag
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.network.protocol.Packet
import net.minecraft.network.protocol.game.ClientGamePacketListener
import net.minecraft.network.syncher.EntityDataSerializers
import net.minecraft.network.syncher.SynchedEntityData
import net.minecraft.server.level.ServerLevel
import net.minecraft.world.entity.Entity
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.player.Player
import net.minecraft.world.level.Level
import net.minecraft.world.level.block.Block
import net.minecraft.world.level.block.state.BlockState
import net.minecraft.world.phys.AABB
import net.minecraft.world.phys.shapes.BooleanOp
import net.minecraft.world.phys.shapes.Shapes
import net.minecraft.world.phys.shapes.VoxelShape
import net.neoforged.neoforge.client.model.data.ModelData
import net.neoforged.neoforge.common.util.LazyOptional
import net.neoforged.neoforge.entity.IEntityAdditionalSpawnData
import net.neoforged.neoforge.network.NetworkHooks
import java.util.*

class GroundChunk(entityType: EntityType<out GroundChunk>, level: Level) : Entity(entityType, level),
    IEntityAdditionalSpawnData {
    lateinit var blocks: Map<BlockPos, BlockState>
    var colors: Optional<Map<BlockPos, UInt>> = Optional.empty()
    lateinit var size: Triple<Int, Int, Int>
    lateinit var shape: VoxelShape // Leaving this for future...
    lateinit var sampled: AABB
    var cache: LazyOptional<Map<BlockPos, List<BakedQuad>>> = LazyOptional.empty()

    var dissolvedLayers: Int
        get() = entityData.get(DISSOLVED_LAYERS)
        set(value) = entityData.set(DISSOLVED_LAYERS, value)

    private var dissolveTimer = -1
    private var dissolveInterval = -1
    private var dissolveEffect: (BlockPos, BlockState) -> Unit = { _, _ -> }

    lateinit var owner: UUID

    constructor(owner: Player, area: AABB) : this(TCombatEntityTypes.GROUND_CHUNK, owner.level()) {
        this.sampled = area
        this.owner = owner.uuid
        val min = BlockPos.containing(area.minX, area.minY, area.minZ)
        val max = BlockPos.containing(area.maxX, area.maxY, area.maxZ)
        val size = (max.subtract(min)).offset(1, 1, 1)
        this.size = Triple(size.x, size.y, size.z)

        val blocks = mutableMapOf<BlockPos, BlockState>()
        var shape = Shapes.empty()
        BlockPos.betweenClosedStream(area).forEach {
            val state = level().getBlockState(it)
            if (!state.isAir) {
                blocks[it.subtract(min)] = state
                shape = Shapes.joinUnoptimized(shape, state.getShape(level(), it), BooleanOp.OR)
            }
        }

        this.blocks = blocks
        this.shape = shape
    }

    override fun tick() {
        super.tick()
        val level = level()

        xo = x
        yo = y
        zo = z

        if (level is ServerLevel) {
            val player = level().getPlayerByUUID(owner)
            if (player == null || player.isDeadOrDying) {
                this.isNoGravity = false
                return
            }
            if (player.level() != level) {
                this.isNoGravity = false
                return
            }

            if (dissolvedLayers < size.second && dissolveInterval != -1) {
                if (dissolveTimer == 0) {
                    if (level.isServerSide) {
                        blocks.filter { (pos, _) -> pos.y == (size.second - dissolvedLayers - 1) }
                            .forEach(dissolveEffect)
                    }

                    dissolveTimer = dissolveInterval
                    dissolvedLayers++

                    if (dissolvedLayers == size.second) discard()
                }
                dissolveTimer--
            }
        }
    }

    override fun defineSynchedData() {
        entityData.define(DISSOLVED_LAYERS, 0)
    }

    override fun readAdditionalSaveData(compound: CompoundTag) {}

    override fun addAdditionalSaveData(compound: CompoundTag) {}

    override fun isAttackable() = false

    override fun writeSpawnData(buffer: FriendlyByteBuf) {
        buffer.writeMap(blocks, { writing, key ->
            writing.writeBlockPos(key)
        }, { writing, value ->
            writing.writeInt(Block.getId(value))
        })
        buffer.writeInt(size.first)
        buffer.writeInt(size.second)
        buffer.writeInt(size.third)
        buffer.writeDouble(sampled.minX)
        buffer.writeDouble(sampled.minY)
        buffer.writeDouble(sampled.minZ)
        buffer.writeDouble(sampled.maxX)
        buffer.writeDouble(sampled.maxY)
        buffer.writeDouble(sampled.maxZ)
    }

    override fun readSpawnData(additionalData: FriendlyByteBuf) {
        val level = level()

        blocks = additionalData.readMap({
            it.readBlockPos()
        }, {
            Block.stateById(it.readInt())
        })
        size = Triple(additionalData.readInt(), additionalData.readInt(), additionalData.readInt())
        sampled = AABB(
            additionalData.readDouble(), additionalData.readDouble(), additionalData.readDouble(),
            additionalData.readDouble(), additionalData.readDouble(), additionalData.readDouble()
        )

        shape = blocks.map { (pos, state) ->
            state.getCollisionShape(level, pos).move(pos.x.toDouble(), pos.y.toDouble(), pos.z.toDouble())
        }.reduce { acc, voxelShape ->
            Shapes.joinUnoptimized(acc, voxelShape, BooleanOp.OR)
        }

        val min = BlockPos.containing(sampled.minX, sampled.minY, sampled.minZ)
        val blockColors = Minecraft.getInstance().blockColors
        colors = Optional.of(BlockPos.betweenClosedStream(sampled).map { pos ->
            val state = level().getBlockState(pos)
            if (!state.isAir) {
                val color = blockColors.getColor(state, level, pos).toUInt()
                pos.subtract(min) to color
            } else null
        }.toList().filterNotNull().toMap())

        val renderer = Minecraft.getInstance().blockRenderer
        testLevel.blocks = blocks
        cache = LazyOptional.of {
            blocks.map { (pos, state) ->
                val model = renderer.getBlockModel(state)
                val quads = Direction.entries
                    .filter { it == Direction.UP || Block.shouldRenderFace(state, testLevel, pos, it, pos.relative(it)) }
                    .flatMap { model.getQuads(state, it, random, ModelData.EMPTY, RenderType.cutout()) }
                pos to quads
            }.filter { it.second.isNotEmpty() }.toMap()
        }

        reapplyPosition()
    }

    override fun canBeCollidedWith(): Boolean {
        return true
    }

    override fun getAddEntityPacket(): Packet<ClientGamePacketListener> = NetworkHooks.getEntitySpawningPacket(this)

    override fun makeBoundingBox(): AABB {
        return if (!::blocks.isInitialized) super.makeBoundingBox() else shape.bounds().move(position())
    }

    fun dissolve(interval: Int, effect: (BlockPos, BlockState) -> Unit = { _, _ -> }) {
        this.dissolveInterval = interval
        this.dissolveEffect = effect
        this.dissolveTimer = interval
    }

    companion object {
        private val DISSOLVED_LAYERS = SynchedEntityData.defineId(GroundChunk::class.java, EntityDataSerializers.INT)
        private val testLevel = FakeLevel()
    }
}