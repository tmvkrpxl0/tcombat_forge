package com.tmvkrpxl0.tcombat.base.common.skill

import com.tmvkrpxl0.tcombat.base.common.network.SetCooldown
import com.tmvkrpxl0.tcombat.base.common.util.skillCooldowns
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.player.Player
import java.lang.Long.max

infix fun Player.cooldownOf(skill: Skill<*>): Long {
    val endTime: Long = skillCooldowns[skill] ?: 0L

    return max(endTime - level().gameTime, 0L)
}

fun checkCooldown(player: Player, skill: Skill<*>): Boolean {
    return player cooldownOf skill <= 0L
}

fun ServerPlayer.setSkillCooldown(skill: Skill<*>, cooldown: Long) {
    if (cooldown == 0L) return

    val currentTime = level().gameTime
    val ends = currentTime + cooldown
    skillCooldowns[skill] = ends

    connection.send(SetCooldown(skill, ends))
}
