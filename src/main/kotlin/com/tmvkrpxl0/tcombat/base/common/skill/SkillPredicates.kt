package com.tmvkrpxl0.tcombat.base.common.skill

import com.tmvkrpxl0.tcombat.base.common.network.packets.ReceiveCooldownEndPacket
import com.tmvkrpxl0.tcombat.base.common.saveddata.SkillData.Companion.skillData
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import net.minecraft.resources.ResourceLocation
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.player.Player
import net.neoforged.neoforge.network.PacketDistributor
import java.lang.Long.max

interface SkillPredicate {
    val name: ResourceLocation
    fun check(player: Player, skill: Skill): Boolean
}

fun SkillPredicate(name: ResourceLocation, predicate: (Player, Skill) -> Boolean) = object : SkillPredicate {
    override val name = name

    override fun check(player: Player, skill: Skill) = predicate(player, skill)
}

infix fun Player.cooldownOf(skill: Skill): Long {
    val endTime: Long = skill.playerSkillCooldownEndTimes[this]?.endTime ?: 0L

    return max(endTime - level().gameTime, 0L)
}

val COOLDOWN = SkillPredicate("cooldown".toResource()) { player, skill ->
    return@SkillPredicate player cooldownOf skill <= 0L
}

fun Player.setSkillCooldown(skill: Skill, cooldown: Long) {
    if (cooldown == 0L) return
    val currentTime = level().gameTime
    skill.playerSkillCooldownEndTimes[this] = (currentTime + cooldown).endTime()

    if (this is ServerPlayer) {
        skillData.isDirty = true
        ReceiveCooldownEndPacket(skill, skill.playerSkillCooldownEndTimes[this]!!).send(PacketDistributor.PLAYER.with { this })
    }
}
