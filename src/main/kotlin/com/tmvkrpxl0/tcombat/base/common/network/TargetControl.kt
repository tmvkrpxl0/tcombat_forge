package com.tmvkrpxl0.tcombat.base.common.network

import com.mojang.serialization.Codec
import com.mojang.serialization.DataResult
import com.tmvkrpxl0.tcombat.base.client.network.TargetNotifications
import com.tmvkrpxl0.tcombat.base.common.util.pickFocus
import com.tmvkrpxl0.tcombat.base.common.util.pickTargets
import com.tmvkrpxl0.tcombat.base.common.util.targetInfo
import net.minecraft.network.codec.ByteBufCodecs
import net.minecraft.network.codec.StreamCodec
import net.minecraft.network.protocol.common.custom.CustomPacketPayload
import net.minecraft.server.level.ServerPlayer
import net.neoforged.neoforge.network.handling.IPayloadContext


data class TargetControl(private val requestType: RequestType) : CustomPacketPayload {
    companion object : CompanionPayload<TargetControl> {
        override val type = "target_control".toPayloadType()
        override val codec = StreamCodec.composite(
            ByteBufCodecs.fromCodec(Codec.INT.comapFlatMap( { ordinal ->
                if (RequestType.entries.size <= ordinal) {
                    DataResult.error { "RequestType: Received invalid ordinal $ordinal" }
                } else {
                    DataResult.success(RequestType.entries[ordinal])
                }
            }, RequestType::ordinal)),
            TargetControl::requestType,
            ::TargetControl
        )
    }

    fun handle(context: IPayloadContext) {
        val player = context.player() as ServerPlayer
        when (requestType) {
            RequestType.SET -> player.pickTargets()
            RequestType.UNSET -> {
                player.targetInfo.targets.clear()
                player.targetInfo.primary = null
                player.connection.send(TargetNotifications.Targets(emptyList()))
            }

            RequestType.PICK_FOCUS -> player.pickFocus()
        }
    }

    override fun type() = type

    enum class RequestType {
        SET, UNSET, PICK_FOCUS
    }
}

