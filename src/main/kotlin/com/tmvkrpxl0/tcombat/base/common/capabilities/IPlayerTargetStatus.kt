package com.tmvkrpxl0.tcombat.base.common.capabilities

import com.tmvkrpxl0.tcombat.base.common.network.packets.TargetModifyNotifyPacket
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.LivingEntity
import net.neoforged.neoforge.network.PacketDistributor
import java.util.*

//On server side, Every player instance should have capability that gets removed when player dies
//On client side, only `Minecraft.getInstance().getPlayer()` instance should have capability that gets removed when the player dies
interface IPlayerTargetStatus {
    var focused: LivingEntity?
    var pickMode: PickMode
    val targets: LinkedList<LivingEntity>
    fun sendChange(player: ServerPlayer) {
        TargetModifyNotifyPacket.Targets(targets).send(PacketDistributor.PLAYER.with { player })
    }

    fun changeTargets(targets: List<LivingEntity>)

    enum class RequestType {
        SET, UNSET, PICK_FOCUS, NEXT_PICK_MODE
    }

    enum class PickMode {
        LOOK, CLOSE, RANDOM
    }
}