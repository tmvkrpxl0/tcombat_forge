package com.tmvkrpxl0.tcombat.base.common.network.packets

import com.tmvkrpxl0.tcombat.base.common.network.TCombatPacketHandler
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.server.level.ServerPlayer
import net.neoforged.fml.LogicalSide
import net.neoforged.neoforge.network.NetworkEvent
import net.neoforged.neoforge.network.PacketDistributor
import org.joml.Vector4f

//Eh, marking packet using annotation is prettier
annotation class Packet(val receiveSide: LogicalSide)
interface ITCombatPacket {
    companion object {
        fun FriendlyByteBuf.writeVector4f(vector4f: Vector4f) {
            this.writeFloat(vector4f.x())
            this.writeFloat(vector4f.y())
            this.writeFloat(vector4f.z())
            this.writeFloat(vector4f.w())
        }

        fun FriendlyByteBuf.readVector4f(): Vector4f {
            return Vector4f(this.readFloat(), this.readFloat(), this.readFloat(), this.readFloat())
        }
    }

    fun encode(buffer: FriendlyByteBuf)
    fun handle(context: NetworkEvent.Context)
    fun sendToServer() {
        TCombatPacketHandler.INSTANCE.sendToServer(this)
    }

    fun send(target: PacketDistributor.PacketTarget) {
        TCombatPacketHandler.INSTANCE.send(target, this)
    }

    fun sendTo(serverPlayer: ServerPlayer) {
        send(PacketDistributor.PLAYER.with { serverPlayer })
    }
}