package com.tmvkrpxl0.tcombat.base.common.network.packets

import com.tmvkrpxl0.tcombat.base.common.capabilities.IPlayerTargetStatus
import com.tmvkrpxl0.tcombat.base.common.util.pickFocus
import com.tmvkrpxl0.tcombat.base.common.util.setTargetsAuto
import com.tmvkrpxl0.tcombat.base.common.util.targetInfo
import net.minecraft.network.FriendlyByteBuf
import net.neoforged.fml.LogicalSide
import net.neoforged.neoforge.network.NetworkEvent

@Packet(receiveSide = LogicalSide.SERVER)
class TargetRequestPacket(val type: IPlayerTargetStatus.RequestType) : ITCombatPacket {

    companion object {
        fun decode(buffer: FriendlyByteBuf): TargetRequestPacket =
            TargetRequestPacket(IPlayerTargetStatus.RequestType.values()[buffer.readVarInt()])
    }

    override fun encode(buffer: FriendlyByteBuf) {
        buffer.writeVarInt(type.ordinal)
    }

    override fun handle(context: NetworkEvent.Context) {
        val player = context.sender!!
        context.enqueueWork {
            when (type) {
                IPlayerTargetStatus.RequestType.SET -> setTargetsAuto(player)
                IPlayerTargetStatus.RequestType.UNSET -> {
                    player.targetInfo.targets.clear()
                    player.targetInfo.sendChange(player)
                }
                IPlayerTargetStatus.RequestType.NEXT_PICK_MODE -> {
                    val mode = player.targetInfo.pickMode
                    val next = IPlayerTargetStatus.PickMode.values()[(mode.ordinal + 1) % IPlayerTargetStatus.PickMode.values().size]
                    player.targetInfo.pickMode = next
                }
                IPlayerTargetStatus.RequestType.PICK_FOCUS -> player.pickFocus()
            }
        }
    }
}

