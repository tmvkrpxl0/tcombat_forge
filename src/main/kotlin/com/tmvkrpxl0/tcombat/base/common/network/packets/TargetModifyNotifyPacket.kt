package com.tmvkrpxl0.tcombat.base.common.network.packets

import com.tmvkrpxl0.tcombat.base.common.capabilities.IPlayerTargetStatus
import com.tmvkrpxl0.tcombat.base.common.util.focus
import com.tmvkrpxl0.tcombat.base.common.util.targetInfo
import net.minecraft.client.Minecraft
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.world.entity.LivingEntity
import net.neoforged.fml.LogicalSide
import net.neoforged.neoforge.network.NetworkEvent
import java.util.*

object TargetModifyNotifyPacket {
    @Packet(receiveSide = LogicalSide.CLIENT)
    class Mode(private val pickMode: IPlayerTargetStatus.PickMode) : ITCombatPacket {
        companion object {
            fun decode(buffer: FriendlyByteBuf): Mode = Mode(IPlayerTargetStatus.PickMode.values()[buffer.readVarInt()])
        }

        override fun encode(buffer: FriendlyByteBuf) {
            buffer.writeVarInt(pickMode.ordinal)
        }

        override fun handle(context: NetworkEvent.Context) {
            context.enqueueWork { Minecraft.getInstance().player!!.targetInfo.pickMode = pickMode }
        }
    }

    @Packet(receiveSide = LogicalSide.CLIENT)
    class Targets(private val targets: List<LivingEntity>) : ITCombatPacket {
        companion object {
            fun decode(buffer: FriendlyByteBuf): Targets {
                val entityIds = buffer.readVarIntArray()
                val player = Minecraft.getInstance().player!!
                val livingEntities = LinkedList<LivingEntity>()
                for (id in entityIds) {
                    val entity = player.level().getEntity(id)
                    if (entity is LivingEntity) {
                        livingEntities.add(entity)
                    }
                }
                return Targets(livingEntities)
            }
        }

        override fun encode(buffer: FriendlyByteBuf) {
            val entityIds = LinkedList<Int>()
            for (livingEntity in targets) {
                if (livingEntity.isAlive) {
                    entityIds.add(livingEntity.id)
                }
            }
            buffer.writeVarIntArray(entityIds.toIntArray())
        }

        override fun handle(context: NetworkEvent.Context) {
            context.enqueueWork { Minecraft.getInstance().player!!.targetInfo.changeTargets(targets) }
        }
    }

    @Packet(receiveSide = LogicalSide.CLIENT)
    class Focus(private val focus: LivingEntity?) : ITCombatPacket {
        companion object {
            fun decode(buffer: FriendlyByteBuf): Focus {
                val entityId = buffer.readVarInt()
                val player = Minecraft.getInstance().player!!
                val entity = player.level().getEntity(entityId)
                return if (entity != null && entity is LivingEntity && entity.level() == player.level()) {
                    Focus(entity)
                } else {
                    Focus(null)
                }
            }
        }

        override fun encode(buffer: FriendlyByteBuf) {
            buffer.writeVarInt(focus?.id ?: 0)
        }

        override fun handle(context: NetworkEvent.Context) {
            context.enqueueWork { Minecraft.getInstance().player!!.focus = focus }
        }
    }
}