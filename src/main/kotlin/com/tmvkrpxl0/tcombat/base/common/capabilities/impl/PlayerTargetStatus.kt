package com.tmvkrpxl0.tcombat.base.common.capabilities.impl

import com.tmvkrpxl0.tcombat.base.common.capabilities.IPlayerTargetStatus
import com.tmvkrpxl0.tcombat.base.common.capabilities.TARGET_HOLDER
import net.minecraft.core.Direction
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.player.Player
import net.neoforged.neoforge.common.capabilities.Capability
import net.neoforged.neoforge.common.capabilities.ICapabilityProvider
import net.neoforged.neoforge.common.util.LazyOptional
import java.util.*

class PlayerTargetStatus(private val player: Player) : IPlayerTargetStatus {
    override val targets: LinkedList<LivingEntity> = LinkedList()
        get() {
            field.removeIf { target -> !target.isAlive || target.level() != player.level() || target.distanceToSqr(player) > 100 * 100 }
            return field
        }

    override fun changeTargets(targets: List<LivingEntity>) {
        this.targets.clear()
        this.targets.addAll(targets)
    }

    override var focused: LivingEntity? = null
        get() {
            if (field?.isAlive != true || !this.targets.contains(field)) {
                this.focused = null //Forget reference just in case, I'm not sure if it's necessary though
            }
            return field
        }
        set(value) {
            if (this.targets.contains(value)) field = value
        }

    override var pickMode = IPlayerTargetStatus.PickMode.LOOK

    class Provider(player: Player) : ICapabilityProvider {
        private val targetHolder: IPlayerTargetStatus = PlayerTargetStatus(player)

        @Suppress("UNCHECKED_CAST")
        override fun <T : Any> getCapability(cap: Capability<T>, side: Direction?): LazyOptional<T> {
            return if (cap === TARGET_HOLDER) {
                LazyOptional.of { return@of targetHolder as T }
            } else LazyOptional.empty()
        }
    }
}