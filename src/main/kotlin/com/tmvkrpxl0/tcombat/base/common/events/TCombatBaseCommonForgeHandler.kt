package com.tmvkrpxl0.tcombat.base.common.events

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.capabilities.impl.PlayerTargetStatus
import com.tmvkrpxl0.tcombat.base.common.entities.miscs.GroundChunk
import com.tmvkrpxl0.tcombat.base.common.util.ignoreNoDamageTicks
import com.tmvkrpxl0.tcombat.base.common.util.tickInstantArrow
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import com.tmvkrpxl0.tcombat.base.explosionImmune
import com.tmvkrpxl0.tcombat.common.entities.projectile.WorldAxe
import net.minecraft.world.entity.Entity
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.player.Player
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.enchantment.EnchantmentHelper
import net.minecraft.world.item.enchantment.Enchantments
import net.minecraft.world.item.enchantment.FrostWalkerEnchantment
import net.minecraft.world.phys.EntityHitResult
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.LogicalSide
import net.neoforged.fml.common.Mod
import net.neoforged.neoforge.event.AttachCapabilitiesEvent
import net.neoforged.neoforge.event.TickEvent
import net.neoforged.neoforge.event.entity.ProjectileImpactEvent
import net.neoforged.neoforge.event.entity.player.PlayerEvent
import net.neoforged.neoforge.server.ServerLifecycleHooks

@Mod.EventBusSubscriber(
    modid = MODID, bus = Mod.EventBusSubscriber.Bus.FORGE, value = [Dist.CLIENT, Dist.DEDICATED_SERVER]
)
object TCombatBaseCommonForgeHandler {
    private val TARGET_HOLDER = "target_holder".toResource()

    @SubscribeEvent
    fun onServerTick(event: TickEvent.ServerTickEvent) {
        if (event.side !== LogicalSide.SERVER) return
        if (event.phase !== TickEvent.Phase.START) return

        tickInstantArrow(event.side)
        explosionImmune.clear()

        for (world in ServerLifecycleHooks.getCurrentServer().allLevels) {
            for (entity in world.allEntities) if (entity is LivingEntity && !entity.onGround()) {
                val vehicle = entity.rootVehicle
                if (vehicle !== entity && vehicle.onGround()) {
                    val i = EnchantmentHelper.getEnchantmentLevel(Enchantments.FROST_WALKER, entity)
                    if (i > 0) {
                        val previousState = entity.onGround()
                        entity.setOnGround(true)
                        FrostWalkerEnchantment.onEntityMoved(entity, world, vehicle.blockPosition(), i)
                        entity.setOnGround(previousState)
                    }
                }
            }
        }
    }

    @SubscribeEvent
    fun attachCapToPlayer(event: AttachCapabilitiesEvent<Entity>) {
        if (event.`object` !is Player) return
        val player = event.`object` as Player
        event.addCapability(TARGET_HOLDER, PlayerTargetStatus.Provider(player))
    }

    @SubscribeEvent
    fun discardEntitiesOnLogout(event: PlayerEvent.PlayerLoggedOutEvent) {
        ServerLifecycleHooks.getCurrentServer().allLevels.forEach { serverWorld ->
            serverWorld.allEntities.forEach { entity ->
                when (entity) {
                    is GroundChunk -> {
                        if (event.entity.uuid == entity.owner) {
                            entity.discard()
                        }
                    }
                    is WorldAxe -> {
                        if (event.entity == entity.owner) {
                            entity.discard()
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    fun discardEntities(event: PlayerEvent.PlayerChangedDimensionEvent) {
        ServerLifecycleHooks.getCurrentServer().allLevels.forEach { serverWorld ->
            serverWorld.allEntities.forEach { entity ->
                when (entity) {
                    is GroundChunk -> {
                        if (event.entity.uuid == entity.owner) {
                            entity.discard()
                        }
                    }
                    is WorldAxe -> {
                        if (event.entity == entity.owner) {
                            entity.discard()
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    fun resetInvulnerableTime(event: ProjectileImpactEvent) {
        if (event.entity !is AbstractArrow) return
        val arrow = event.entity as AbstractArrow
        if (arrow.level().isClientSide) return
        if (!arrow.ignoreNoDamageTicks) return

        val r = event.rayTraceResult
        if (r is EntityHitResult) {
            r.entity.invulnerableTime = 0
        }
    }
}