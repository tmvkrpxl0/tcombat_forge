package com.tmvkrpxl0.tcombat.base.common.skill

import net.minecraft.world.entity.player.Player

infix fun Player.isUsingAuto(skill: Skill) = skill.autoExecuteFor.contains(this)

infix fun Player.startAutoExecuting(skill: Skill) {
    skill.autoExecuteFor.add(this)
}

infix fun Player.stopAutoExecuting(skill: Skill) {
    skill.autoExecuteFor.remove(this)
}

private val enableAutoExecuteByDefault = HashSet<Skill>()

var Skill.autoEnable: Boolean
    get() = enableAutoExecuteByDefault.contains(this)
    set(value) {
        if (value) enableAutoExecuteByDefault.add(this)
        else enableAutoExecuteByDefault.remove(this)
    }