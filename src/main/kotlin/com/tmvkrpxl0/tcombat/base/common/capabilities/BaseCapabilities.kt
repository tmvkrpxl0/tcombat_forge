package com.tmvkrpxl0.tcombat.base.common.capabilities

import net.neoforged.neoforge.common.capabilities.Capability
import net.neoforged.neoforge.common.capabilities.CapabilityManager
import net.neoforged.neoforge.common.capabilities.CapabilityToken

val TARGET_HOLDER: Capability<IPlayerTargetStatus> = CapabilityManager.get(object : CapabilityToken<IPlayerTargetStatus>() {})