package com.tmvkrpxl0.tcombat.base.common.network

import com.tmvkrpxl0.tcombat.base.client.renderers.alertColors
import com.tmvkrpxl0.tcombat.base.client.util.getClientPlayer
import com.tmvkrpxl0.tcombat.base.common.skill.SKILL_REGISTRY
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.util.Color
import com.tmvkrpxl0.tcombat.base.common.util.TCombatDataSerializers
import com.tmvkrpxl0.tcombat.base.common.util.autoExecutes
import com.tmvkrpxl0.tcombat.base.common.util.skillCooldowns
import io.netty.buffer.ByteBuf
import net.minecraft.network.RegistryFriendlyByteBuf
import net.minecraft.network.codec.ByteBufCodecs
import net.minecraft.network.codec.StreamCodec
import net.minecraft.network.protocol.common.custom.CustomPacketPayload
import net.minecraft.server.level.ServerPlayer
import net.neoforged.neoforge.network.handling.IPayloadContext
import java.util.*

data class ExecuteSkill(private val skill: Skill<*>) : CustomPacketPayload {
    companion object : CompanionPayload<ExecuteSkill> {
        override val type = "execute_skill".toPayloadType()
        override val codec = ByteBufCodecs.fromCodecWithRegistries(
            SKILL_REGISTRY.byNameCodec().xmap(::ExecuteSkill, ExecuteSkill::skill)
        )
    }

    fun handle(context: IPayloadContext) {
        val player = context.player() as ServerPlayer
        skill.tryExecute(player)
    }

    override fun type() = type
}

data class AutoExecuteSkill(private val skill: Skill<*>, private val isEnable: Boolean) : CustomPacketPayload {
    companion object : CompanionPayload<AutoExecuteSkill> {
        override val type = "auto_execute".toPayloadType()
        override val codec = StreamCodec.composite(
            ByteBufCodecs.fromCodecWithRegistries(SKILL_REGISTRY.byNameCodec()),
            AutoExecuteSkill::skill,
            ByteBufCodecs.BOOL,
            AutoExecuteSkill::isEnable,
            ::AutoExecuteSkill
        )
    }

    override fun type() = type

    fun handle(context: IPayloadContext) {
        val player = context.player()
        if (isEnable) player.autoExecutes += skill
        else player.autoExecutes.remove(skill)

        if (player is ServerPlayer) {
            player.connection.send(AutoExecuteSkill(skill, isEnable))
        }
    }
}

data class SetCooldown(private val skill: Skill<*>, private val endTime: Long) : CustomPacketPayload {
    companion object : CompanionPayload<SetCooldown> {
        override val type = "set_cooldown".toPayloadType()
        override val codec = StreamCodec.composite(
            ByteBufCodecs.fromCodecWithRegistries(SKILL_REGISTRY.byNameCodec()),
            SetCooldown::skill,
            ByteBufCodecs.VAR_LONG,
            SetCooldown::endTime,
            ::SetCooldown
        )
    }

    override fun type() = type

    fun handle(context: IPayloadContext) {
        val cooldown = getClientPlayer().skillCooldowns
        cooldown[skill] = endTime
    }
}

data class Alert(private val skill: Skill<*>, private val tint: Optional<Color>) : CustomPacketPayload {
    companion object : CompanionPayload<Alert> {
        override val type = "alert".toPayloadType()
        override val codec = StreamCodec.composite(
            ByteBufCodecs.fromCodecWithRegistries(SKILL_REGISTRY.byNameCodec()), Alert::skill,
            ByteBufCodecs.optional(TCombatDataSerializers.COLOR.codec().cast()), Alert::tint,
            ::Alert
        )
    }

    override fun type() = type

    fun handle(context: IPayloadContext) {
        tint.ifPresentOrElse({ alertColors[skill] = it}, { alertColors.remove(skill)})
    }
}