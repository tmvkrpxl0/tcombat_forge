package com.tmvkrpxl0.tcombat.base.client.renderers

import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.texture.TextureAtlasSprite
import net.minecraft.client.resources.TextureAtlasHolder

val SKILL_ATLAS_LOCATION = "textures/atlas/skills.png".toResource()
private val BACKGROUND = "skill_hud_background".toResource()

object SkillAtlasHolder : TextureAtlasHolder(
    Minecraft.getInstance().getTextureManager(), SKILL_ATLAS_LOCATION, "skills".toResource()
) {

    val HUD_BACKGROUND: TextureAtlasSprite by lazy { getSprite(BACKGROUND) }
    val Skill<*>.skillTexture: TextureAtlasSprite
        get() = getSprite(skillTextureLocation)
    val id: Int
        get() = textureAtlas.id
}
