package com.tmvkrpxl0.tcombat.base.client.renderers

import com.mojang.blaze3d.vertex.PoseStack
import com.tmvkrpxl0.tcombat.base.common.entities.miscs.GroundChunk
import net.minecraft.client.renderer.MultiBufferSource
import net.minecraft.client.renderer.RenderType
import net.minecraft.client.renderer.entity.EntityRenderer
import net.minecraft.client.renderer.entity.EntityRendererProvider
import net.minecraft.client.renderer.texture.OverlayTexture
import net.minecraft.world.inventory.InventoryMenu

class GroundChunkRenderer(pContext: EntityRendererProvider.Context) : EntityRenderer<GroundChunk>(pContext) {
    override fun getTextureLocation(pEntity: GroundChunk) = InventoryMenu.BLOCK_ATLAS

    override fun render(
        groundChunk: GroundChunk,
        pEntityYaw: Float,
        pPartialTick: Float,
        pPoseStack: PoseStack,
        pBuffer: MultiBufferSource,
        pPackedLight: Int
    ) {
        val buffer = pBuffer.getBuffer(RenderType.cutout())

        groundChunk.cache.resolve().get().forEach { (pos, quads) ->
            if (pos.y >= groundChunk.size.second - groundChunk.dissolvedLayers) return@forEach

            pPoseStack.pushPose()
            pPoseStack.translate(pos.x.toFloat(), pos.y.toFloat(), pos.z.toFloat())

            val color = groundChunk.colors.get()[pos] ?: 0b11111111111111111111111111111111u
            val red = (color shr 16 and 255u).toFloat() / 255F
            val green = (color shr 8 and 255u).toFloat() / 255F
            val blue = (color and 255u).toFloat() / 255F

            quads.forEach { quad ->
                if (quad.isTinted) {
                    buffer.putBulkData(pPoseStack.last(), quad, red, green, blue, pPackedLight, OverlayTexture.NO_OVERLAY)
                } else {
                    buffer.putBulkData(pPoseStack.last(), quad, 1F, 1F, 1F, pPackedLight, OverlayTexture.NO_OVERLAY)
                }
            }
            pPoseStack.popPose()
        }

        super.render(groundChunk, pEntityYaw, pPartialTick, pPoseStack, pBuffer, pPackedLight)
    }
}