package com.tmvkrpxl0.tcombat.base.client.events

import com.tmvkrpxl0.tcombat.base.MODID
import com.tmvkrpxl0.tcombat.base.client.renderers.SkillAtlasHolder
import com.tmvkrpxl0.tcombat.base.client.renderers.renderSkillOverlay
import com.tmvkrpxl0.tcombat.base.common.util.toResource
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.EventBusSubscriber
import net.neoforged.neoforge.client.event.RegisterClientReloadListenersEvent
import net.neoforged.neoforge.client.event.RegisterGuiLayersEvent

@EventBusSubscriber(modid = MODID, bus = EventBusSubscriber.Bus.MOD, value = [Dist.CLIENT])
object TCombatBaseClientModHandler {
    @SubscribeEvent
    fun onClientSetup(event: RegisterGuiLayersEvent) {
        event.registerBelowAll("skill_overlay".toResource(), ::renderSkillOverlay)
    }

    @SubscribeEvent
    fun registerSkillAtlas(event: RegisterClientReloadListenersEvent) {
        event.registerReloadListener(SkillAtlasHolder)
    }
}