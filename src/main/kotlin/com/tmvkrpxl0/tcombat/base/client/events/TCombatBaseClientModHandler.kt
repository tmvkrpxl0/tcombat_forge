package com.tmvkrpxl0.tcombat.base.client.events

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.client.renderers.SkillAtlasHolder
import com.tmvkrpxl0.tcombat.base.client.renderers.renderSkillOverlay
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.Mod
import net.neoforged.neoforge.client.event.RegisterClientReloadListenersEvent
import net.neoforged.neoforge.client.event.RegisterGuiOverlaysEvent

@Mod.EventBusSubscriber(modid = MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = [Dist.CLIENT])
object TCombatBaseClientModHandler {
    @SubscribeEvent
    fun onClientSetup(event: RegisterGuiOverlaysEvent) {
        event.registerBelowAll("skill_overlay", ::renderSkillOverlay)
    }

    @SubscribeEvent
    fun registerSkillAtlas(event: RegisterClientReloadListenersEvent) {
        event.registerReloadListener(SkillAtlasHolder)
    }
}