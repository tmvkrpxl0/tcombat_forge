package com.tmvkrpxl0.tcombat.base.client.renderers

import com.mojang.blaze3d.vertex.DefaultVertexFormat
import com.mojang.blaze3d.vertex.DefaultVertexFormat.POSITION_COLOR_TEX
import com.mojang.blaze3d.vertex.VertexFormat
import net.minecraft.client.renderer.RenderType

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "INACCESSIBLE_TYPE")
class TCombatBaseRenderTypes : RenderType(null, null, null, 0, false, false, null, null) {
    init {
        throw AssertionError("This constructor should never be invoked!")
    }

    companion object {
        val SOLID_COLOR: RenderType = create(
            "tcombat_target",
            DefaultVertexFormat.POSITION_COLOR,
            VertexFormat.Mode.QUADS,
            4096,
            false,
            false,
            CompositeState.builder().setShaderState(POSITION_COLOR_SHADER).setDepthTestState(NO_DEPTH_TEST)
                .setTransparencyState(TRANSLUCENT_TRANSPARENCY).createCompositeState(false)
        )

        val SKILL_GUI: RenderType = create(
            "tcombat_skill_gui",
            POSITION_COLOR_TEX,
            VertexFormat.Mode.QUADS,
            4096,
            false,
            false,
            CompositeState.builder().setShaderState(POSITION_COLOR_TEX_SHADER).setTextureState(TextureStateShard(SKILL_ATLAS_LOCATION, false, false))
                .setTransparencyState(TRANSLUCENT_TRANSPARENCY)
                .createCompositeState(false)
        )
    }
}