package com.tmvkrpxl0.tcombat.base.client.renderers

import com.mojang.blaze3d.vertex.VertexConsumer
import com.tmvkrpxl0.tcombat.base.client.renderers.TCombatBaseRenderTypes.Companion.SOLID_COLOR
import net.minecraft.client.renderer.MultiBufferSource
import net.minecraft.client.renderer.RenderType

class ColorWrapperSource(
    private var red: Float,
    private var green: Float,
    private var blue: Float,
    private var alpha: Float,
    private val original: MultiBufferSource.BufferSource,
) : MultiBufferSource by original {
    override fun getBuffer(renderType: RenderType): VertexConsumer {
        return if (renderType.toString().contains("entity_shadow")) {
            VoidBufferBuilder
        } else {
            val buffer = original.getBuffer(SOLID_COLOR)
            ColorBuilder(buffer, red, green, blue, alpha)
        }
    }

    fun setColor(red: Float, green: Float, blue: Float, alpha: Float) {
        this.red = red
        this.green = green
        this.blue = blue
        this.alpha = alpha
    }

    class ColorBuilder(
        private val delegate: VertexConsumer,
        private var red: Float,
        private var green: Float,
        private var blue: Float,
        private var alpha: Float
    ) : VertexConsumer by delegate {
        override fun color(r: Int, g: Int, b: Int, a: Int): VertexConsumer {
            return this.delegate.color(red, green, blue, alpha)
        }
    }

    object VoidBufferBuilder : VertexConsumer {

        override fun vertex(p_85945_: Double, p_85946_: Double, p_85947_: Double): VertexConsumer {
            return this
        }

        override fun color(p_85973_: Int, p_85974_: Int, p_85975_: Int, p_85976_: Int): VertexConsumer {
            return this
        }

        override fun uv(p_85948_: Float, p_85949_: Float): VertexConsumer {
            return this
        }

        override fun overlayCoords(p_85971_: Int, p_85972_: Int): VertexConsumer {
            return this
        }

        override fun uv2(p_86010_: Int, p_86011_: Int): VertexConsumer {
            return this
        }

        override fun normal(p_86005_: Float, p_86006_: Float, p_86007_: Float): VertexConsumer {
            return this
        }

        override fun endVertex() {
            return
        }

        override fun defaultColor(p_166901_: Int, p_166902_: Int, p_166903_: Int, p_166904_: Int) {
            return
        }

        override fun unsetDefaultColor() {
            return
        }
    }
}