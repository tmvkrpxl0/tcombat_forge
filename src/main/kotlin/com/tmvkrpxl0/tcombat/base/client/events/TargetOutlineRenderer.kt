package com.tmvkrpxl0.tcombat.base.client.events

import com.mojang.blaze3d.systems.RenderSystem
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.client.renderers.ColorWrapperSource
import com.tmvkrpxl0.tcombat.base.common.util.targetInfo
import net.minecraft.client.Minecraft
import net.minecraft.client.model.EntityModel
import net.minecraft.util.Mth.lerp
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.phys.Vec3
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.Mod.EventBusSubscriber
import net.neoforged.neoforge.client.event.RenderLevelStageEvent
import net.neoforged.neoforge.client.event.RenderLivingEvent
import org.lwjgl.opengl.GL11
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.minus


/**
 * Target outline renderer
 *
 * It draws outline by rendering target entities using targetBuffer before rendering actual entity
 * It starts drawing after block rendering has finished
 * It disables depth test and mask when drawing outlines, forcing them to be in front of all blocks
 * But those outlines will get overwritten by pretty much everything else on the screen
 *
 * Reason for this is to draw parts of entities that are behind other blocks as solid color
 */
@EventBusSubscriber(modid = MODID, bus = EventBusSubscriber.Bus.FORGE, value = [Dist.CLIENT])
object TargetOutlineRenderer {
    private val targetBuffer: ColorWrapperSource by lazy {
        ColorWrapperSource(
            0F, 1F, 1F, 1F,
            Minecraft.getInstance().renderBuffers().bufferSource(),
        )
    }

    private var renderingTargets = false

    @SubscribeEvent
    fun afterSolid(event: RenderLevelStageEvent) {
        if (event.stage != RenderLevelStageEvent.Stage.AFTER_SOLID_BLOCKS) return

        val minecraft = Minecraft.getInstance()
        val mainPlayer = minecraft.player ?: return

        if (!mainPlayer.isAlive) return

        val camPos = minecraft.gameRenderer.mainCamera.position
        val dispatcher = minecraft.entityRenderDispatcher
        val poseStack = event.poseStack

        val targetInfo = mainPlayer.targetInfo
        val normalTargets = targetInfo.targets
        val focused = targetInfo.focused

        val wasDepthMaskEnabled = GL11.glGetBoolean(GL11.GL_DEPTH_WRITEMASK)
        val wasDepthTestEnabled = GL11.glGetBoolean(GL11.GL_DEPTH_TEST)

        RenderSystem.depthMask(false)
        RenderSystem.disableDepthTest()


        renderingTargets = true
        normalTargets.filter { !it.isInvisibleTo(mainPlayer) }.forEach { target ->
            if (target !== focused) {
                targetBuffer.setColor(0.0F, 1.0F, 1.0F, 1.0F)
            } else {
                targetBuffer.setColor(1.0F, 0.0F, 0.0F, 1.0F)
            }

            val partialPos = target.position().partial(event.partialTick, target.xOld, target.yOld, target.zOld)
            val yaw = lerp(event.partialTick, target.yRot, target.yRotO)
            val renderPos = partialPos - camPos
            dispatcher.render(
                target,
                renderPos.x,
                renderPos.y,
                renderPos.z,
                yaw,
                event.partialTick,
                poseStack,
                targetBuffer,
                dispatcher.getPackedLightCoords(target, event.partialTick)
            )
        }
        renderingTargets = false


        RenderSystem.depthMask(wasDepthMaskEnabled)
        if (wasDepthTestEnabled) {
            RenderSystem.enableDepthTest()
        } else {
            RenderSystem.disableDepthTest()
        }
    }

    @SubscribeEvent
    fun <T : LivingEntity> renderTargetOutlineStart(event: RenderLivingEvent.Pre<T, EntityModel<T>>) {
        if (renderingTargets) {
            event.poseStack.pushPose()
            event.poseStack.scale(1.05F, 1.05F, 1.05F)
        }
    }

    @SubscribeEvent
    fun <T : LivingEntity> renderTargetOutlineEnd(event: RenderLivingEvent.Post<T, EntityModel<T>>) {
        if (renderingTargets) {
            event.poseStack.popPose()
        }
    }

    private fun Vec3.partial(partialTicks: Float, xOld: Double, yOld: Double, zOld: Double): Vec3 {
        val pX = lerp(partialTicks.toDouble(), x, xOld)
        val pY = lerp(partialTicks.toDouble(), y, yOld)
        val pZ = lerp(partialTicks.toDouble(), z, zOld)
        return Vec3(pX, pY, pZ)
    }
}
