package com.tmvkrpxl0.tcombat.base.client.util

import net.minecraft.client.Minecraft
import net.minecraft.world.entity.player.Player

// This is unsafe to call without a check
fun getClientPlayer(): Player {
    return Minecraft.getInstance().player!!
}