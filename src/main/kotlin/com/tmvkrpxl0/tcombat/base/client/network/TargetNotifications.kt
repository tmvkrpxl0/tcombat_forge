package com.tmvkrpxl0.tcombat.base.client.network

import com.tmvkrpxl0.tcombat.base.common.network.CompanionPayload
import com.tmvkrpxl0.tcombat.base.common.util.targetInfo
import io.netty.buffer.ByteBuf
import net.minecraft.client.Minecraft
import net.minecraft.client.multiplayer.ClientLevel
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.network.codec.StreamCodec
import net.minecraft.network.protocol.common.custom.CustomPacketPayload
import net.minecraft.world.entity.LivingEntity
import net.neoforged.neoforge.network.handling.IPayloadContext

object TargetNotifications {

    data class Targets(private val targets: List<LivingEntity>) : CustomPacketPayload {
        companion object: CompanionPayload<Targets> {
            override val type = "notify/targets".toPayloadType()
            override val codec = StreamCodec.of<FriendlyByteBuf, Targets>(
                { buffer, payload -> buffer.writeVarIntArray(payload.targets.map(LivingEntity::getId).toIntArray())},
                { buffer -> buffer.readVarIntArray().let(Minecraft.getInstance().level!!::getLivingEntities).let(::Targets) }
            )
        }

        fun handle(context: IPayloadContext) {
            val targetInfo = Minecraft.getInstance().player!!.targetInfo
            targetInfo.targets.clear()
            targetInfo.targets.addAll(targets)

            if (!targets.contains(targetInfo.primary)) {
                targetInfo.primary = null
            }
        }

        override fun type() = type
    }

    data class Primary(private val primary: LivingEntity?) : CustomPacketPayload {
        companion object: CompanionPayload<Primary> {
            override val type = "notify/primary".toPayloadType()
            override val codec = StreamCodec.of<ByteBuf, Primary>(
                { buffer, payload -> buffer.writeInt(payload.primary?.id ?: 0) },
                { buffer -> Minecraft.getInstance().level?.getEntity(buffer.readInt()).let { Primary(it as LivingEntity?) } }
            )
        }

        override fun type() = type

        fun handle(context: IPayloadContext) {
            Minecraft.getInstance().player!!.targetInfo.primary = primary
        }
    }
}

private fun ClientLevel.getLivingEntities(ids: IntArray): List<LivingEntity> {
    val set = ids.toSet()
    val entities = entitiesForRendering()
    return entities.asSequence().filter { set.contains(it.id) }.filterIsInstance<LivingEntity>().toList()
}