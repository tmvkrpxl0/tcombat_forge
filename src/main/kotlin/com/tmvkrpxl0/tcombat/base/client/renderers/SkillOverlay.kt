package com.tmvkrpxl0.tcombat.base.client.renderers

import com.mojang.blaze3d.vertex.DefaultVertexFormat
import com.mojang.blaze3d.vertex.VertexConsumer
import com.mojang.blaze3d.vertex.VertexFormat
import com.tmvkrpxl0.tcombat.base.client.renderers.SkillAtlasHolder.skillTexture
import com.tmvkrpxl0.tcombat.base.common.skill.SKILL_REGISTRY
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.skill.cooldownOf
import com.tmvkrpxl0.tcombat.base.common.util.Color
import com.tmvkrpxl0.tcombat.base.common.util.autoExecutes
import net.minecraft.client.DeltaTracker
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiGraphics
import net.minecraft.client.renderer.GameRenderer
import net.minecraft.client.renderer.RenderStateShard
import net.minecraft.client.renderer.RenderStateShard.ShaderStateShard
import net.minecraft.client.renderer.RenderType
import net.minecraft.client.renderer.texture.TextureAtlasSprite
import net.minecraft.util.Mth
import net.minecraft.world.phys.Vec2
import org.joml.Matrix4f
import kotlin.math.max
import kotlin.math.min

val alertColors = HashMap<Skill<*>, Color>()

// I'm pretty sure there would be better ways to write this
fun renderSkillOverlay(guiGraphics: GuiGraphics, deltaTracker: DeltaTracker) {
    val minecraft = Minecraft.getInstance()
    if (minecraft.options.hideGui) {
        return
    }

    val poseStack = guiGraphics.pose()
    val player = minecraft.player!!
    val matrix = poseStack.last().pose()
    val width = guiGraphics.guiWidth()
    val size = width / 8F //Order of vertices is ]
    val unit = size / 8F
    val multiBufferSource = guiGraphics.bufferSource()
    val activeSkillOffset = size / 2
    val toShow = SKILL_REGISTRY.filter { skill -> skill.shouldShowInOverlay(player) }
    val partitions = toShow.partition { player.autoExecutes.contains(it) }
    val showInPassive = partitions.first
    val showInActive = partitions.second

    val toAlert = mutableListOf<Pair<Pair<Vec2, Vec2>, Color>>()
    val hasCooldown = mutableListOf<Pair<Skill<*>, Pair<Vec2, Vec2>>>()

    val skillGuiBuffer = multiBufferSource.getBuffer(SKILL_GUI)
    val background = SkillAtlasHolder.HUD_BACKGROUND

    // Render 2 backgrounds first
    skillGuiBuffer.blitSprite(
        matrix, Vec2(0F, 0F), Vec2(size, size / 2), 0F, 1F, 1F, 1F, 0.5F, background
    )
    skillGuiBuffer.blitSprite(
        matrix, Vec2(0F, activeSkillOffset), Vec2(size, activeSkillOffset + size / 2), 0F, 1F, 1F, 1F, 0.5F, background
    )

    // Render passive skills first
    showInPassive.forEachIndexed { index, skill ->
        val startX = unit + index * 2 * unit
        val startY = if (index >= 4) unit * 2 else unit
        val start = Vec2(startX, startY)
        val end = Vec2(startX + unit, startY + unit)
        val texture = skill.skillTexture

        alertColors[skill]?.let { toAlert += (start to end) to it }

        skillGuiBuffer.blitSprite(matrix, start, end, 0.1F, 1F, 1F, 1F, 1F, texture)
    }

    // Render active skills
    showInActive.forEachIndexed { index, skill ->
        val startX = unit + (index % 4) * 2 * unit
        val startY = if (index >= 4) activeSkillOffset + unit * 3 else activeSkillOffset + unit
        val start = Vec2(startX, startY)
        val end = Vec2(startX + unit, startY + unit)
        val texture = skill.skillTexture
        val transparency = if (player cooldownOf skill <= 0) {
            1.0F
        } else {
            hasCooldown += skill to (start to end)
            0.5F
        }

        alertColors[skill]?.let { toAlert += (start to end) to it }

        skillGuiBuffer.blitSprite(matrix, start, end, 0.1F, 1F, 1F, 1F, transparency, texture)
    }
    multiBufferSource.endBatch(SKILL_GUI)

    // Now renders solid colors
    val solidColorBuffer = multiBufferSource.getBuffer(SOLID_COLOR)

    // Render alert
    if (player.clientLevel.gameTime % 2 == 0L) {
        toAlert.forEach {
            val color = it.second
            val start = it.first.first
            val end = it.first.second

            val r = (color.r.toFloat()) / 255F
            val g = (color.g.toFloat()) / 255F
            val b = (color.b.toFloat()) / 255F
            val a = (color.a.toFloat()) / 255F
            solidColorBuffer.drawSquare(matrix, start, end, 0.1F, r, g, b, a)
        }
    }

    // Render cooldown indicator
    hasCooldown.forEach {
        val skill = it.first

        val cooldown = player cooldownOf skill
        val maxCooldown = skill.cooldownTick.toFloat()
        val step = 1.0F / maxCooldown
        val current = min(step * cooldown, 1.0F)
        val previous = max(0.0F, current - step)
        val lerped = Mth.lerp(deltaTracker.realtimeDeltaTicks, previous, current)

        val originalStart = it.second.first

        val start = Vec2(originalStart.x, originalStart.y + unit * (1 - lerped))
        val end = it.second.second

        solidColorBuffer.drawSquare(matrix, start, end, 0.1F, 0.2F, 0.2F, 0.2F, 0.5F)
    }

    multiBufferSource.endBatch(SOLID_COLOR)

}

//Order of vertices: U where upper left is the first
fun VertexConsumer.blitSprite(
    matrix: Matrix4f, ul: Vec2, dr: Vec2, z: Float, red: Float, green: Float, blue: Float, alpha: Float, texture: TextureAtlasSprite
) {
    val xys = arrayOf(
        Vec2(ul.x, ul.y) to Vec2(texture.u0, texture.v0),
        Vec2(ul.x, dr.y) to Vec2(texture.u0, texture.v1),
        Vec2(dr.x, dr.y) to Vec2(texture.u1, texture.v1),
        Vec2(dr.x, ul.y) to Vec2(texture.u1, texture.v0),
    )

    xys.forEach { (xy, uv) ->
        addVertex(matrix, xy.x, xy.y, z)
        setUv(uv.x, uv.y)
        setColor(red, green, blue, alpha)
    }
}

fun VertexConsumer.drawSquare(
    matrix: Matrix4f, ul: Vec2, dr: Vec2, z: Float, red: Float, green: Float, blue: Float, alpha: Float
) {
    val xys = arrayOf(
        Vec2(ul.x, ul.y),
        Vec2(ul.x, dr.y),
        Vec2(dr.x, dr.y),
        Vec2(dr.x, ul.y),
    )

    xys.forEach { xy ->
        addVertex(matrix, xy.x, xy.y, z)
        setColor(red, green, blue, alpha)
    }
}

val SOLID_COLOR: RenderType = RenderType.create(
    "solid_color",
    DefaultVertexFormat.POSITION_COLOR,
    VertexFormat.Mode.QUADS,
    4096,
    false,
    false,
    RenderType.CompositeState.builder()
        .setShaderState(RenderType.POSITION_COLOR_SHADER)
        .setDepthTestState(RenderType.NO_DEPTH_TEST)
        .setTransparencyState(RenderType.TRANSLUCENT_TRANSPARENCY)
        .createCompositeState(false)
)

val POSITION_COLOR_TEX_SHADER = ShaderStateShard(GameRenderer::getPositionTexColorShader)

val SKILL_GUI: RenderType = RenderType.create(
    "skill_gui",
    DefaultVertexFormat.POSITION_TEX_COLOR,
    VertexFormat.Mode.QUADS,
    4096,
    false,
    false,
    RenderType.CompositeState.builder()
        .setShaderState(POSITION_COLOR_TEX_SHADER)
        .setTextureState(RenderStateShard.TextureStateShard(SKILL_ATLAS_LOCATION, false, false))
        .setTransparencyState(RenderType.TRANSLUCENT_TRANSPARENCY)
        .createCompositeState(false)
)

