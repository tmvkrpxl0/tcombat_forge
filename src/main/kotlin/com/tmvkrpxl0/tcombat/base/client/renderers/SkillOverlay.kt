package com.tmvkrpxl0.tcombat.base.client.renderers

import com.mojang.blaze3d.vertex.VertexConsumer
import com.tmvkrpxl0.tcombat.base.client.renderers.SkillAtlasHolder.skillTexture
import com.tmvkrpxl0.tcombat.base.client.renderers.TCombatBaseRenderTypes.Companion.SKILL_GUI
import com.tmvkrpxl0.tcombat.base.client.renderers.TCombatBaseRenderTypes.Companion.SOLID_COLOR
import com.tmvkrpxl0.tcombat.base.common.skill.SKILL_REGISTRY
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.skill.cooldownOf
import com.tmvkrpxl0.tcombat.base.common.skill.isUsingAuto
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiGraphics
import net.minecraft.client.renderer.texture.TextureAtlasSprite
import net.minecraft.world.phys.Vec2
import net.neoforged.neoforge.client.gui.overlay.ExtendedGui
import org.joml.Matrix4f
import org.joml.Vector4f
import java.lang.Float.min


// I'm pretty sure there would be better ways to write this
@Suppress("UNUSED_PARAMETER")
fun renderSkillOverlay(gui: ExtendedGui, guiGraphics: GuiGraphics, partialTicks: Float, width: Int, height: Int) {
    val minecraft = Minecraft.getInstance()
    if (!minecraft.options.hideGui) {
        val poseStack = guiGraphics.pose()
        val player = minecraft.player!!
        val matrix = poseStack.last().pose()
        val size = width / 8F //Order of vertices is ]
        val unit = size / 8F
        val multiBufferSource = guiGraphics.bufferSource()
        val activeSkillOffset = size / 2
        val showInPassive = SKILL_REGISTRY.filter { skill -> skill.shouldShowInOverlay(player) && player isUsingAuto skill }
        val showInActive = SKILL_REGISTRY.filter { skill -> skill.shouldShowInOverlay(player) && !(player isUsingAuto skill) }

        //Order of vertices: U
        fun VertexConsumer.blit(
            matrix: Matrix4f, ul: Vec2, dr: Vec2, z: Float, red: Float, green: Float, blue: Float, alpha: Float, texture: TextureAtlasSprite?
        ) {
            vertex(matrix, ul.x, ul.y, z)
            color(red, green, blue, alpha)
            if (texture != null) uv(texture.u0, texture.v0)
            endVertex()

            vertex(matrix, ul.x, dr.y, z)
            color(red, green, blue, alpha)
            if (texture != null) uv(texture.u0, texture.v1)
            endVertex()

            vertex(matrix, dr.x, dr.y, z)
            color(red, green, blue, alpha)
            if (texture != null) uv(texture.u1, texture.v1)
            endVertex()

            vertex(matrix, dr.x, ul.y, z)
            color(red, green, blue, alpha)
            if (texture != null) uv(texture.u1, texture.v0)
            endVertex()
        }

        val toAlert = mutableListOf<Pair<Pair<Vec2, Vec2>, Vector4f>>()
        val hasCooldown = mutableListOf<Pair<Skill, Pair<Vec2, Vec2>>>()

        val skillGuiBuffer = multiBufferSource.getBuffer(SKILL_GUI)
        val background = SkillAtlasHolder.HUD_BACKGROUND

        // Render 2 backgrounds first
        skillGuiBuffer.blit(
            matrix, Vec2(0F, 0F), Vec2(size, size / 2), 0F, 1F, 1F, 1F, 0.5F, background
        )
        skillGuiBuffer.blit(
            matrix, Vec2(0F, activeSkillOffset), Vec2(size, activeSkillOffset + size / 2), 0F, 1F, 1F, 1F, 0.5F, background
        )

        // Render passive skills first
        showInPassive.forEachIndexed { index, skill ->
            val startX = unit + index * 2 * unit
            val startY = if (index >= 4) unit * 2 else unit
            val start = Vec2(startX, startY)
            val end = Vec2(startX + unit, startY + unit)
            val texture = skill.skillTexture

            if (skill.alertColor != null) toAlert += (start to end) to skill.alertColor!!

            skillGuiBuffer.blit(matrix, start, end, 0.1F, 1F, 1F, 1F, 1F, texture)
        }

        // Render active skills
        showInActive.forEachIndexed { index, skill ->
            val startX = unit + (index % 4) * 2 * unit
            val startY = if (index >= 4) activeSkillOffset + unit * 3 else activeSkillOffset + unit
            val start = Vec2(startX, startY)
            val end = Vec2(startX + unit, startY + unit)
            val texture = skill.skillTexture
            val transparency = if (player cooldownOf skill <= 0) {
                1.0F
            } else {
                hasCooldown += skill to (start to end)
                0.5F
            }

            if (skill.alertColor != null) toAlert += (start to end) to skill.alertColor!!

            skillGuiBuffer.blit(matrix, start, end, 0.1F, 1F, 1F, 1F, transparency, texture)
        }
        multiBufferSource.endBatch(SKILL_GUI)

        // Now renders solid colors
        val solidColorBuffer = multiBufferSource.getBuffer(SOLID_COLOR)

        // Render alert
        if (player.clientLevel.gameTime % 2 == 0L) {
            toAlert.forEach {
                val color = it.second
                val start = it.first.first
                val end = it.first.second
                solidColorBuffer.blit(matrix, start, end, 0.1F, color.x(), color.y(), color.z(), color.w(), null)
            }
        }

        // Render cooldown indicator
        hasCooldown.forEach {
            val skill = it.first

            val cooldown = player cooldownOf skill
            val maxCooldown = skill.skillCooldownTick

            val remainingCooldownPercent = min(1F, if (maxCooldown == 0L) 0F else cooldown.toFloat() / maxCooldown)

            val originalStart = it.second.first

            val start = Vec2(originalStart.x, originalStart.y + unit * (1 - remainingCooldownPercent))
            val end = it.second.second

            solidColorBuffer.blit(matrix, start, end, 0.1F, 0.2F, 0.2F, 0.2F, 0.5F, null)
        }

        multiBufferSource.endBatch(SOLID_COLOR)

    }
}

