package com.tmvkrpxl0.tcombat.base

import com.tmvkrpxl0.tcombat.base.common.data.registerAttachments
import com.tmvkrpxl0.tcombat.base.common.network.registerPackets
import com.tmvkrpxl0.tcombat.base.common.skill.createSkillRegistry
import com.tmvkrpxl0.tcombat.base.common.util.TCombatDataSerializers
import net.minecraft.advancements.critereon.EntityFlagsPredicate
import net.minecraft.advancements.critereon.EntityPredicate
import net.neoforged.fml.common.Mod
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import thedarkcolour.kotlinforforge.neoforge.forge.MOD_BUS

internal const val MODID = "tcombat_base"
val LOGGER: Logger = LogManager.getLogger("tcombat base")

@Mod(MODID)
object TCombatBase {
    init {
        createSkillRegistry(MOD_BUS)
        registerAttachments(MOD_BUS)
        TCombatDataSerializers.register(MOD_BUS)
        MOD_BUS.addListener(::registerPackets)


        EntityPredicate.Builder.entity().flags(EntityFlagsPredicate.Builder.flags().setOnGround(true)).build()
    }
}
