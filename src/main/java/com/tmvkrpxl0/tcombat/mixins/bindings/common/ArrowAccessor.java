package com.tmvkrpxl0.tcombat.mixins.bindings.common;


import net.minecraft.world.entity.projectile.AbstractArrow;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(AbstractArrow.class)
public interface ArrowAccessor {

    @Accessor("inGround")
    boolean isInGround();
}
