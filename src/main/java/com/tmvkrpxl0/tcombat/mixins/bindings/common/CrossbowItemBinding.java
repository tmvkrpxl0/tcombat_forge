package com.tmvkrpxl0.tcombat.mixins.bindings.common;

import com.tmvkrpxl0.tcombat.mixins.common.CrossbowItemMixinKt;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.item.*;
import net.minecraft.world.level.Level;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(CrossbowItem.class)
public abstract class CrossbowItemBinding extends ProjectileWeaponItem implements Vanishable {
    @Unique
    public final float test = 3.0F;

    public CrossbowItemBinding(Properties pProperties) {
        super(pProperties);
    }

    @ModifyConstant(
            method = "performShooting(Lnet/minecraft/world/level/Level;Lnet/minecraft/world/entity/LivingEntity;Lnet/minecraft/world/InteractionHand;Lnet/minecraft/world/item/ItemStack;FF)V",
            constant = {
                    @Constant(floatValue = 0.0F),
                    @Constant(floatValue = 10.0F),
                    @Constant(floatValue = -10.0F)
            }
    )
    private static float narrowMultiShot(float angle, Level level, LivingEntity shooter, InteractionHand usedHand, ItemStack crossbow, float velocity, float inaccuracy) {
        return CrossbowItemMixinKt.narrowMultiShot(angle, crossbow);
    }

    @Inject(
            method = "getArrow(Lnet/minecraft/world/level/Level;Lnet/minecraft/world/entity/LivingEntity;Lnet/minecraft/world/item/ItemStack;Lnet/minecraft/world/item/ItemStack;)Lnet/minecraft/world/entity/projectile/AbstractArrow;",
            at = @At("RETURN"),
            locals = LocalCapture.CAPTURE_FAILSOFT
    )
    private static void setArrowOnFire(Level level, LivingEntity livingEntity, ItemStack crossbowStack, ItemStack ammoStack, CallbackInfoReturnable cir, ArrowItem arrowItem, AbstractArrow arrowEntity) {
        CrossbowItemMixinKt.setArrowOnFire(crossbowStack, arrowEntity);
    }
}
