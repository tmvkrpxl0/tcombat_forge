package com.tmvkrpxl0.tcombat.mixins.bindings.common;

import net.minecraft.core.dispenser.DispenseItemBehavior;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.DispenserBlock;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(DispenserBlock.class)
public interface DispenserAccessor {
    @Invoker("getDispenseMethod")
    DispenseItemBehavior getBehavior(ItemStack item);
}
