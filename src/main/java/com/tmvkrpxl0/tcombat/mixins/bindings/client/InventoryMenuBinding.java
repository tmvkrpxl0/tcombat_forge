package com.tmvkrpxl0.tcombat.mixins.bindings.client;

import com.tmvkrpxl0.tcombat.mixins.client.InventoryMenuMixinKt;
import net.minecraft.world.Container;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(targets = "net.minecraft.world.inventory.InventoryMenu$2")
public abstract class InventoryMenuBinding extends Slot {

    public InventoryMenuBinding(Container pContainer, int pIndex, int pX, int pY) {
        //Dummy
        super(pContainer, pIndex, pX, pY);
    }

    @Override
    public boolean mayPlace(ItemStack itemStack) {
        //The mixin target doesn't override this one so this is required
        return super.mayPlace(itemStack);
    }

    //Lnet/minecraft/world/inventory/InventoryMenu;recipeMatches(Lnet/minecraft/world/item/crafting/Recipe;)Z
    //Lnet/minecraft/world/inventory/InventoryMenu$2;mayPlace(Lnet/minecraft/world/item/ItemStack;)Z

    @Inject(
            method = "mayPlace(Lnet/minecraft/world/item/ItemStack;)Z",
            at = @At("HEAD"),
            cancellable = true
    )
    private void constructorBind(ItemStack stack, CallbackInfoReturnable<Boolean> ci) {
        InventoryMenuMixinKt.addCondition(stack, ci);
    }
}
