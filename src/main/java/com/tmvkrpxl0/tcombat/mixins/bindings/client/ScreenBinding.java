package com.tmvkrpxl0.tcombat.mixins.bindings.client;

import com.tmvkrpxl0.tcombat.mixins.client.ScreenMixinKt;
import net.minecraft.client.gui.components.events.AbstractContainerEventHandler;
import net.minecraft.client.gui.screens.Screen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(Screen.class)
public abstract class ScreenBinding extends AbstractContainerEventHandler {

    @ModifyConstant(
            method = "renderTransparentBackground(Lnet/minecraft/client/gui/GuiGraphics;)V",
            constant = {@Constant(intValue = -1072689136), @Constant(intValue = -804253680)}
    )
    private int mixinBind(int from) {
        return ScreenMixinKt.modifyRenderBackground((Screen)((Object)this), from);
    }
}
