package com.tmvkrpxl0.tcombat.client.key

import com.mojang.blaze3d.platform.InputConstants
import net.minecraft.client.KeyMapping
import net.minecraft.client.Minecraft
import net.neoforged.neoforge.client.settings.KeyConflictContext
import org.lwjgl.glfw.GLFW

const val KEY_CATEGORY = "tmvkrpxl0's Combat"
val KB_SHATTER_ARROW = KeyMapping(
    "Shatter arrows", KeyConflictContext.IN_GAME, InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_C, KEY_CATEGORY
)
val KB_SET_TARGETS = KeyMapping("Set targets", KeyConflictContext.IN_GAME, InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_X, KEY_CATEGORY)
val KB_SET_TARGET_MODE = KeyMapping(
    "Set targetting mode", KeyConflictContext.IN_GAME, InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_R, KEY_CATEGORY
)
val KB_REFLECTION_BLAST = KeyMapping(
    "Reflection blast", KeyConflictContext.IN_GAME, InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_N, KEY_CATEGORY
)
val KB_TEST = KeyMapping("Test", KeyConflictContext.IN_GAME, InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_MINUS, KEY_CATEGORY)

// TODO Implement wielding in a better way
val KB_WIELD = KeyMapping("Wield item", KeyConflictContext.IN_GAME, InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_R, KEY_CATEGORY)

val KEYBINDINGS = hashSetOf(KB_SHATTER_ARROW, KB_SET_TARGETS, KB_SET_TARGET_MODE, KB_REFLECTION_BLAST, KB_TEST, KB_WIELD)
val MODIFIED_IN_GAME_OR_GUI: HashSet<KeyMapping> = run {
    val options = Minecraft.getInstance().options
    with(options) {
        hashSetOf(keyUp, keyDown, keyLeft, keyRight, keyJump, keySprint)
    }
}

