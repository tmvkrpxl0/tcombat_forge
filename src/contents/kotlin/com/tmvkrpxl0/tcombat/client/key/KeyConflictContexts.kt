package com.tmvkrpxl0.tcombat.client.key

import net.minecraft.client.Minecraft
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen
import net.neoforged.neoforge.client.settings.IKeyConflictContext
import net.neoforged.neoforge.client.settings.KeyConflictContext

/**
 * @param original Just in case this might overwrite other {@link IKeyConflictContext}, wrap it
 */
class InGameOrInventory(private val original: IKeyConflictContext) : IKeyConflictContext {
    override fun isActive(): Boolean {
        val screen = Minecraft.getInstance().screen
        return original.isActive || screen == null || screen is AbstractContainerScreen<*>
    }

    override fun conflicts(other: IKeyConflictContext): Boolean {
        return original.conflicts(other) || other.conflicts(KeyConflictContext.IN_GAME) || other.conflicts(KeyConflictContext.GUI)
    }
}