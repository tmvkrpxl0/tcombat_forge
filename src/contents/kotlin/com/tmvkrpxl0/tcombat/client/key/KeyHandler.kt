package com.tmvkrpxl0.tcombat.client.key

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.network.ExecuteSkill
import com.tmvkrpxl0.tcombat.base.common.network.TargetControl
import com.tmvkrpxl0.tcombat.base.common.skill.cooldownOf
import com.tmvkrpxl0.tcombat.base.common.util.next
import com.tmvkrpxl0.tcombat.client.debug.ImGuiDebugScreen
import com.tmvkrpxl0.tcombat.common.skill.REFLECTION_BLAST
import com.tmvkrpxl0.tcombat.common.skill.SHATTER_ARROW
import com.tmvkrpxl0.tcombat.common.skill.ShatterArrow
import com.tmvkrpxl0.tcombat.mixins.bindings.common.ArrowAccessor
import net.minecraft.client.Minecraft
import net.minecraft.network.chat.Component
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.phys.AABB
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.EventBusSubscriber
import net.neoforged.neoforge.client.event.ClientTickEvent

@EventBusSubscriber(Dist.CLIENT, modid = MODID)
object KeyHandler {
    private var requestType = TargetControl.RequestType.SET

    @SubscribeEvent
    fun onTick(event: ClientTickEvent.Pre) {
        val player = Minecraft.getInstance().player ?: return
        if (KB_SHATTER_ARROW.consumeClick()) {
            if (player cooldownOf SHATTER_ARROW > 0) return

            val level = player.clientLevel

            val entities = level.getEntities(
                player, AABB(-15.0, -15.0, -15.0, 15.0, 15.0, 15.0).move(player.position())
            ).asSequence()
                .filterIsInstance<AbstractArrow>()
                .filter { it.distanceToSqr(player) <= (5 * 5) }
                .filter { !(it as ArrowAccessor).isInGround || it.isPickable }
                .toList()

            if (entities.isEmpty()) {
                player.playNotifySound(SoundEvents.PLAYER_ATTACK_SWEEP, SoundSource.MASTER, 0.2f, 1f)
                player.sendSystemMessage(Component.translatable("${ShatterArrow.translationKey}.fail"))
            }

            entities.forEach { arrow ->
                level.playLocalSound(
                    arrow.x, arrow.y, arrow.z,
                    SoundEvents.SHIELD_BLOCK, SoundSource.PLAYERS,
                    0.8F, 0.8F + level.random.nextFloat() * 0.4F,
                    false
                )
            }
            player.connection.send(ExecuteSkill(SHATTER_ARROW))
        }
        if (KB_SET_TARGETS.consumeClick()) {
            player.connection.send(TargetControl(requestType))
        }
        if (KB_SET_TARGET_MODE.consumeClick()) {
            requestType = requestType.next()
            player.sendSystemMessage(Component.nullToEmpty(requestType.name))
        }
        if (KB_REFLECTION_BLAST.consumeClick()) {
            player.connection.send(ExecuteSkill(REFLECTION_BLAST))
        }
        if (KB_TEST.consumeClick()) {
            Minecraft.getInstance().setScreen(ImGuiDebugScreen())
        }
    }
}