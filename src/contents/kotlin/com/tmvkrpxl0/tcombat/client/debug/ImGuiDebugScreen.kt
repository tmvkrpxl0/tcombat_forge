package com.tmvkrpxl0.tcombat.client.debug

import com.mojang.blaze3d.pipeline.TextureTarget
import com.tmvkrpxl0.tcombat.base.client.renderers.SkillAtlasHolder
import com.tmvkrpxl0.tcombat.base.client.renderers.SkillAtlasHolder.skillTexture
import com.tmvkrpxl0.tcombat.base.common.network.AutoExecuteSkill
import com.tmvkrpxl0.tcombat.base.common.network.ExecuteSkill
import com.tmvkrpxl0.tcombat.base.common.skill.SKILL_REGISTRY
import com.tmvkrpxl0.tcombat.base.common.util.autoExecutes
import com.tmvkrpxl0.tcombat.common.skill.ARROW_SENSE
import com.tmvkrpxl0.tcombat.common.skill.TEST_SKILL
import com.tmvkrpxl0.tcombat.toResource
import imgui.ImGui
import imgui.ImGuiIO
import imgui.gl3.ImGuiImplGl3
import imgui.glfw.ImGuiImplGlfw
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiGraphics
import net.minecraft.client.gui.screens.Screen
import net.minecraft.client.renderer.PostChain
import net.minecraft.network.chat.Component
import org.joml.Vector4f

lateinit var DEPTH_TARGET: TextureTarget
lateinit var DEPTH_EFFECT: PostChain

class ImGuiDebugScreen : Screen(Component.nullToEmpty("tcombat")) {
    var shouldPause = false

    companion object {
        private val window: Long = Minecraft.getInstance().window.window
        private val implGl3: ImGuiImplGl3 = ImGuiImplGl3()
        private val implGlfw: ImGuiImplGlfw = ImGuiImplGlfw()
        private val io: ImGuiIO

        init {
            val context = ImGui.createContext()
            ImGui.setCurrentContext(context)
            io = ImGui.getIO()
            implGlfw.init(window, false)
            implGl3.init("#version 150")
        }
    }

    override fun isPauseScreen(): Boolean = shouldPause

    override fun render(guiGraphics: GuiGraphics, mouseX: Int, mouseY: Int, partialTicks: Float) {
        val minecraft = getMinecraft()
        val player = minecraft.player ?: return

        implGlfw.newFrame()

        ImGui.newFrame()
        ImGui.begin("Hello")

        //Put ImGui code here to test stuffs
        ImGui.text(if (shouldPause) "Pausing..." else "Unpaused")
        if (ImGui.button("Toggle pause")) {
            shouldPause = !shouldPause
        }

        if (ImGui.button("Toggle Arrow sense")) {
            player.connection.send(AutoExecuteSkill(ARROW_SENSE, !(player.autoExecutes.contains(ARROW_SENSE))))
        }
        if (ImGui.button("Send test skill")) {
            player.connection.send(ExecuteSkill(TEST_SKILL))
        }

        val id = SkillAtlasHolder.id
        SKILL_REGISTRY.iterator().withIndex().forEach { (index, skill) ->
            val texture = skill.skillTexture
            if (index % 4 != 0) {
                ImGui.sameLine(0F, 2F)
            }

            val autoExecutes = player.autoExecutes.contains(skill)
            val background = if (autoExecutes) {
                Vector4f(0.4F, 0.9F, 0.35F, 1.0F)
            } else {
                Vector4f(0.8F, 0.09F, 0.0F, 1.0F)
            }

            ImGui.imageButton(
                id,
                24F,
                24F,
                texture.u0,
                texture.v0,
                texture.u1,
                texture.v1,
                2,
                background.x,
                background.y,
                background.z,
                background.w
            )
            if (ImGui.isItemClicked(0)) {
                player.connection.send(AutoExecuteSkill(skill, !autoExecutes))
            }
            if (ImGui.isItemClicked(1)) {
                player.connection.send(ExecuteSkill(skill))
            }
        }

        if (::DEPTH_TARGET.isInitialized) {
            ImGui.image(DEPTH_TARGET.colorTextureId, 100F, 100F)
        } else {
            if (ImGui.button("create")) {
                val window = minecraft.window
                DEPTH_TARGET = TextureTarget(window.screenWidth, window.screenHeight, false, false)

                DEPTH_EFFECT = PostChain(minecraft.textureManager, minecraft.resourceManager, DEPTH_TARGET, "shaders/post/depth_test.json".toResource())
            }
        }

        ImGui.end()
        ImGui.render()
        implGl3.renderDrawData(ImGui.getDrawData())
    }
}