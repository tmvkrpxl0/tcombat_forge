package com.tmvkrpxl0.tcombat.client.events

import com.mojang.blaze3d.platform.GlStateManager
import com.mojang.blaze3d.systems.RenderSystem
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.client.fadeLevel
import com.tmvkrpxl0.tcombat.client.isFading
import com.tmvkrpxl0.tcombat.client.key.MODIFIED_IN_GAME_OR_GUI
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.screens.inventory.InventoryScreen
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.EventBusSubscriber
import net.neoforged.neoforge.client.event.ClientTickEvent
import net.neoforged.neoforge.client.event.ScreenEvent
import org.lwjgl.opengl.GL14

@EventBusSubscriber(modid = MODID, value = [Dist.CLIENT], bus = EventBusSubscriber.Bus.GAME)
object TCombatClientForgeHandler {

    @SubscribeEvent
    fun onClientTick(event: ClientTickEvent.Pre) {
        val minecraft = Minecraft.getInstance()
        minecraft.level ?: return
        val options = minecraft.options
        val isMoving = MODIFIED_IN_GAME_OR_GUI.any { kb -> kb != options.keySprint && kb.isDown }
        isFading = isMoving
        if (isMoving) {
            if (fadeLevel < 10) fadeLevel++
        } else {
            if (fadeLevel > 0) fadeLevel--
        }
    }

    @SubscribeEvent
    fun beforeDrawingScreen(event: ScreenEvent.BackgroundRendered) {
        if (event.screen !is InventoryScreen) return

        RenderSystem.enableBlend()
        RenderSystem.blendFunc(
            GlStateManager.SourceFactor.CONSTANT_ALPHA,
            GlStateManager.DestFactor.ONE_MINUS_CONSTANT_ALPHA
        )
        var alpha = 1f - (fadeLevel / 10f)
        if (alpha < 0.2f) alpha = 0.2f
        if (alpha > 1f) alpha = 1f
        alpha *= alpha //square it
        GL14.glBlendColor(0f, 0f, 0f, alpha) //I don't think I should be using opengl function directly...
    }

    @SubscribeEvent
    fun onKeyPressedWithScreen(event: ScreenEvent.KeyPressed.Pre) {
        MODIFIED_IN_GAME_OR_GUI.find { it.key.value == event.keyCode }?.let {
            it.isDown = true
        }
    }

    @SubscribeEvent
    fun onKeyReleasedWithScreen(event: ScreenEvent.KeyReleased.Pre) {
        MODIFIED_IN_GAME_OR_GUI.find { it.key.value == event.keyCode }?.let {
            it.isDown = false
        }
    }
}