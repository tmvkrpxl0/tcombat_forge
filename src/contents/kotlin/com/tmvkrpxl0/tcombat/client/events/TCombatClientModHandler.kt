package com.tmvkrpxl0.tcombat.client.events

import com.mojang.blaze3d.vertex.PoseStack
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.client.StructureModelLoader
import com.tmvkrpxl0.tcombat.client.key.InGameOrInventory
import com.tmvkrpxl0.tcombat.client.key.KEYBINDINGS
import com.tmvkrpxl0.tcombat.client.key.MODIFIED_IN_GAME_OR_GUI
import com.tmvkrpxl0.tcombat.client.models.TNTArrowModel
import com.tmvkrpxl0.tcombat.client.renderers.*
import com.tmvkrpxl0.tcombat.common.entity.*
import com.tmvkrpxl0.tcombat.common.item.MOBILE_DISPENSER
import com.tmvkrpxl0.tcombat.common.item.misc.MobileDispenserItem
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.client.color.item.ItemColor
import net.minecraft.client.model.HumanoidModel
import net.minecraft.client.player.LocalPlayer
import net.minecraft.client.renderer.item.ItemProperties
import net.minecraft.world.InteractionHand
import net.minecraft.world.entity.HumanoidArm
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.item.Item
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.block.RedStoneWireBlock
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.EventBusSubscriber
import net.neoforged.fml.common.asm.enumextension.EnumProxy
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent
import net.neoforged.neoforge.client.IArmPoseTransformer
import net.neoforged.neoforge.client.event.EntityRenderersEvent
import net.neoforged.neoforge.client.event.ModelEvent
import net.neoforged.neoforge.client.event.RegisterColorHandlersEvent
import net.neoforged.neoforge.client.event.RegisterKeyMappingsEvent
import net.neoforged.neoforge.client.extensions.common.IClientItemExtensions
import net.neoforged.neoforge.client.extensions.common.RegisterClientExtensionsEvent
import net.neoforged.neoforge.registries.DeferredItem
import org.joml.AxisAngle4f
import org.joml.Quaternionf
import org.joml.Vector3f

@EventBusSubscriber(modid = MODID, bus = EventBusSubscriber.Bus.MOD, value = [Dist.CLIENT])
object TCombatClientModHandler {

    @SubscribeEvent
    fun registerKeys(event: RegisterKeyMappingsEvent) {
        KEYBINDINGS.forEach { event.register(it) }
    }

    @SubscribeEvent
    fun onClientSetup(event: FMLClientSetupEvent) {
        event.enqueueWork {
            MODIFIED_IN_GAME_OR_GUI.forEach { toWrap ->
                toWrap.keyConflictContext = InGameOrInventory(toWrap.keyConflictContext)
            }

            ItemProperties.register(
                MOBILE_DISPENSER, "mb_firing".toResource()
            ) { _, _, livingEntity, _ ->
                if (livingEntity == null) return@register 0F
                val isFiring = livingEntity.isUsingItem && livingEntity.getItemInHand(livingEntity.usedItemHand).item is MobileDispenserItem
                return@register if (isFiring) 1F else 0F
            }

            ItemProperties.register(
                MOBILE_DISPENSER, "mb_front".toResource()
            ) { _, _, livingEntity, _ ->
                if (livingEntity == null) return@register 0F
                val isFiring = livingEntity.isUsingItem && livingEntity.getItemInHand(livingEntity.usedItemHand).item is MobileDispenserItem
                val time = livingEntity.level().gameTime
                val frontTorch = isFiring && time % 2 == 0L && time % 16 <= 7
                return@register if (frontTorch) 1F else 0F
            }

            ItemProperties.register(
                MOBILE_DISPENSER, "mb_back".toResource()
            ) { _, _, livingEntity, _ ->
                if (livingEntity == null) return@register 0F
                val isFiring = livingEntity.isUsingItem && livingEntity.getItemInHand(livingEntity.usedItemHand).item is MobileDispenserItem
                val time = livingEntity.level().gameTime
                val backTorch = isFiring && time % 2 == 0L && time % 16 > 7
                return@register if (backTorch) 1F else 0F
            }
        }
    }

    @SubscribeEvent
    fun onRendererRegister(event: EntityRenderersEvent.RegisterRenderers) {
        event.registerEntityRenderer(GROUND_CHUNK, ::GroundChunkRenderer)

        event.registerEntityRenderer(TNT_ARROW, ::TNTArrowRenderer)
        event.registerEntityRenderer(SNIPE_ARROW, ::SnipeArrowRenderer)
        event.registerEntityRenderer(REFLECTIVE_ARROW, ::ReflectiveArrowRenderer)
        event.registerEntityRenderer(IRON_ARROW, ::IronArrowRenderer)
    }

    @SubscribeEvent
    fun onRegisterLayerDefinition(event: EntityRenderersEvent.RegisterLayerDefinitions) {
        event.registerLayerDefinition(TNTArrowModel.LAYER_LOCATION) { TNTArrowModel.createBodyLayer() }
    }

    @SubscribeEvent
    fun onRegisterModelLoaders(event: ModelEvent.RegisterGeometryLoaders) {
        event.register("structure".toResource(), StructureModelLoader)
    }

    @OptIn(ExperimentalStdlibApi::class)
    @SubscribeEvent
    fun onColorRegister(event: RegisterColorHandlersEvent.Item) {
        // Color format is 0xAARRGGBB
        // Redstone wire does not include alpha, so we need to inject it ourselves
        val maxAlpha = -16777216 // 0xFF000000
        val minPower = RedStoneWireBlock.POWER.possibleValues.min()
        val maxPower = RedStoneWireBlock.POWER.possibleValues.max()
        
        fun injectAlpha(original: Int): Int {
            if (original.and(maxAlpha) != 0) {
                val hexString = original.toHexString(HexFormat.UpperCase)
                throw IllegalStateException("They changed format. Recheck RedstoneWireBlock class. Original Color: $hexString")
            }

            return original.or(maxAlpha)
        }
        
        val maxColor = injectAlpha(RedStoneWireBlock.getColorForPower(maxPower))
        val minColor = injectAlpha(RedStoneWireBlock.getColorForPower(minPower))

        val itemColor = ItemColor { _, tintIndex ->
            if (tintIndex == 1) {
                maxColor
            } else {
                minColor
            }
        }
        event.register(itemColor, MOBILE_DISPENSER)
    }

    private const val POSE_NAME = "TCOMBAT_MOBILE_DISPENSER"

    @JvmStatic
    fun getPoseParams(index: Int, type: Class<*>): Any {
        return when (index) {
            0 -> false
            1 -> IArmPoseTransformer {
                    model, entity, arm ->
                if (entity.mainArm != arm) return@IArmPoseTransformer

                when (arm) {
                    HumanoidArm.LEFT -> {
                        val firing = entity.isUsingItem
                        model.rightArm.xRot =
                            if (firing) -0.7853982F else 0.7853982F //if (firing) -5 degree else 45 degree
                        model.rightArm.yRot = if (firing) 0.6981317F else -0.6981317F
                        model.leftArm.xRot = model.head.xRot - 0.7853982F
                        model.leftArm.yRot = 0F
                    }

                    HumanoidArm.RIGHT -> {
                        val firing = entity.isUsingItem
                        model.rightArm.xRot = model.head.xRot - 0.7853982F
                        model.rightArm.yRot = 0F

                        val zAngle = -0.43633232F
                        val yRotation = AxisAngle4f(zAngle, Vector3f(0F, 0F, 1F))
                        val xRotation = AxisAngle4f(model.rightArm.xRot + 0.2617994F, Vector3f(1F, 0F, 0F))
                        if (!firing) xRotation.angle += 0.7853982F

                        val total = Quaternionf(xRotation).mul(Quaternionf(yRotation))
                        val angles = total.getEulerAnglesZYX(Vector3f())
                        model.leftArm.xRot = angles.x
                        model.leftArm.yRot = angles.y
                        model.leftArm.zRot = angles.z
                    }

                    else -> {
                        throw IllegalStateException("?")
                    }
                }
            }
            else -> throw IllegalStateException()
        }
    }

    @SubscribeEvent
    fun registerPose(event: RegisterClientExtensionsEvent) {
        event.registerItem(object : IClientItemExtensions {
            private val pose: HumanoidModel.ArmPose = HumanoidModel.ArmPose.valueOf(POSE_NAME)

            override fun getArmPose(
                entityLiving: LivingEntity,
                hand: InteractionHand,
                itemStack: ItemStack
            ) = pose

            override fun applyForgeHandTransform(
                poseStack: PoseStack,
                player: LocalPlayer,
                arm: HumanoidArm,
                itemInHand: ItemStack,
                partialTick: Float,
                equipProcess: Float,
                swingProcess: Float
            ): Boolean {
                return true
            }
        }, MOBILE_DISPENSER)
    }
}