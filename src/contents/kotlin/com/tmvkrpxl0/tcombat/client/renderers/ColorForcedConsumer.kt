package com.tmvkrpxl0.tcombat.client.renderers

import com.mojang.blaze3d.vertex.VertexConsumer

class ColorForcedConsumer(
    private val original: VertexConsumer,
    private val red: Int,
    private val green: Int,
    private val blue: Int,
    private val alpha: Int
) : VertexConsumer by original{
    override fun setColor(unused1: Int, unused2: Int, unused3: Int, unused4: Int): VertexConsumer =
        original.setColor(red, green, blue, alpha)
}