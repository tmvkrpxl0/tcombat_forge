package com.tmvkrpxl0.tcombat.client.renderers

import net.minecraft.core.BlockPos
import net.minecraft.world.level.BlockGetter
import net.minecraft.world.level.block.Blocks
import net.minecraft.world.level.block.state.BlockState
import net.minecraft.world.level.material.FluidState
import net.minecraft.world.level.material.Fluids

class FakeLevel: BlockGetter {
    var blocks: Map<BlockPos, BlockState> = HashMap()
    override fun getHeight() = 256

    override fun getMinBuildHeight() = 0

    override fun getBlockEntity(pPos: BlockPos) = null

    override fun getBlockState(blockPos: BlockPos) = blocks.getOrDefault(blockPos, Blocks.AIR.defaultBlockState())

    override fun getFluidState(pPos: BlockPos): FluidState = Fluids.EMPTY.defaultFluidState()
}