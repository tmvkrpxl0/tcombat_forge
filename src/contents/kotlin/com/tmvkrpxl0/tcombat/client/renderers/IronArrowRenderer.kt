package com.tmvkrpxl0.tcombat.client.renderers

import com.tmvkrpxl0.tcombat.common.entity.projectile.IronArrow
import net.minecraft.client.renderer.entity.ArrowRenderer
import net.minecraft.client.renderer.entity.EntityRendererProvider
import net.minecraft.client.renderer.entity.TippableArrowRenderer
import net.minecraft.resources.ResourceLocation

class IronArrowRenderer(context: EntityRendererProvider.Context) : ArrowRenderer<IronArrow>(context) {
    override fun getTextureLocation(arrow: IronArrow): ResourceLocation = TippableArrowRenderer.NORMAL_ARROW_LOCATION
}