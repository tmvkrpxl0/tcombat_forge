package com.tmvkrpxl0.tcombat.client.renderers

import com.tmvkrpxl0.tcombat.common.entity.projectile.ReflectiveArrow
import net.minecraft.client.renderer.entity.ArrowRenderer
import net.minecraft.client.renderer.entity.EntityRendererProvider
import net.minecraft.client.renderer.entity.TippableArrowRenderer
import net.minecraft.resources.ResourceLocation

class ReflectiveArrowRenderer(context: EntityRendererProvider.Context) : ArrowRenderer<ReflectiveArrow>(context) {
    override fun getTextureLocation(arrow: ReflectiveArrow): ResourceLocation = TippableArrowRenderer.NORMAL_ARROW_LOCATION
}