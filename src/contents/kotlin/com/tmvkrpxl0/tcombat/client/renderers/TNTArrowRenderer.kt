package com.tmvkrpxl0.tcombat.client.renderers

import com.mojang.blaze3d.vertex.PoseStack
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.util.quaternion
import com.tmvkrpxl0.tcombat.base.common.util.toRadian
import com.tmvkrpxl0.tcombat.client.models.TNTArrowModel
import com.tmvkrpxl0.tcombat.common.entity.projectile.TNTArrow
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.client.renderer.MultiBufferSource
import net.minecraft.client.renderer.entity.ArrowRenderer
import net.minecraft.client.renderer.entity.EntityRendererProvider.Context
import net.minecraft.client.renderer.entity.TippableArrowRenderer
import net.minecraft.client.renderer.texture.OverlayTexture
import net.minecraft.resources.ResourceLocation
import net.minecraft.util.ColorRGBA
import net.minecraft.util.Mth
import org.joml.Vector3f


private val forwards = Vector3f(0.0f, 0.0f, 1.0f)
private val up = Vector3f(0.0f, 1.0f, 0.0f)
class TNTArrowRenderer(contextIn: Context) : ArrowRenderer<TNTArrow>(contextIn) {
    private val model: TNTArrowModel = TNTArrowModel(contextIn.bakeLayer(TNTArrowModel.LAYER_LOCATION))

    override fun getTextureLocation(entity: TNTArrow): ResourceLocation {
        return TippableArrowRenderer.NORMAL_ARROW_LOCATION
    }

    override fun render(
        entityIn: TNTArrow, entityYaw: Float, partialTicks: Float, poseStackIn: PoseStack, bufferSource: MultiBufferSource, packedLightIn: Int
    ) {
        poseStackIn.pushPose() //Rotate box that this will render

        poseStackIn.mulPose(up quaternion (Mth.lerp(partialTicks, entityIn.yRotO, entityIn.yRot) - 90f).toRadian())
        poseStackIn.mulPose(forwards quaternion Mth.lerp(partialTicks, entityIn.xRotO, entityIn.xRot).toRadian())
        poseStackIn.translate(0.0, 0.0, -1 / 32.0) //render the box
        model.renderToBuffer(
            poseStackIn, bufferSource.getBuffer(model.renderType(BOX)), packedLightIn, OverlayTexture.pack(0f, false), -1
        )
        poseStackIn.popPose()
        super.render(entityIn, entityYaw, partialTicks, poseStackIn, bufferSource, packedLightIn)
    }

    companion object {
        private val BOX = "textures/entity/projectiles/tnt_arrow.png".toResource()
    }

}