package com.tmvkrpxl0.tcombat.client.renderers

import com.tmvkrpxl0.tcombat.common.entity.projectile.SnipeArrow
import net.minecraft.client.renderer.entity.ArrowRenderer
import net.minecraft.client.renderer.entity.EntityRendererProvider.Context
import net.minecraft.client.renderer.entity.TippableArrowRenderer
import net.minecraft.resources.ResourceLocation

class SnipeArrowRenderer(context: Context) : ArrowRenderer<SnipeArrow>(context) {
    override fun getTextureLocation(arrow: SnipeArrow): ResourceLocation = TippableArrowRenderer.NORMAL_ARROW_LOCATION
}