package com.tmvkrpxl0.tcombat.client.renderers

import com.mojang.blaze3d.shaders.Uniform
import com.mojang.blaze3d.vertex.DefaultVertexFormat
import com.mojang.blaze3d.vertex.VertexFormat
import com.tmvkrpxl0.tcombat.client.renderers.DoomSkyRenderType.COLOR_UNIFORM
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.client.renderer.RenderType
import net.minecraft.client.renderer.ShaderInstance
import net.neoforged.neoforge.client.event.RegisterShadersEvent

private lateinit var doomSkyInstance: ShaderInstance
object DoomSkyRenderType: RenderType("", DefaultVertexFormat.POSITION, VertexFormat.Mode.TRIANGLES, 0, false, false, {}, {}) {
    val instance: RenderType by lazy {
        val composite = CompositeState.builder()
            .setShaderState(ShaderStateShard(::doomSkyInstance))
            .setTransparencyState(NO_TRANSPARENCY)
            .setWriteMaskState(COLOR_WRITE)
            .setCullState(NO_CULL)
            .createCompositeState(false)
        return@lazy create("doom_sky", DefaultVertexFormat.POSITION, VertexFormat.Mode.QUADS, 72, false, false, composite)
    }

    lateinit var COLOR_UNIFORM: Uniform
}

fun registerShader(event: RegisterShadersEvent) {
    val doomSky = ShaderInstance(event.resourceProvider, "test".toResource(), DefaultVertexFormat.POSITION)
    event.registerShader(doomSky) {
        doomSkyInstance = it
        COLOR_UNIFORM = doomSkyInstance.getUniform("Color")!!
    }
}
