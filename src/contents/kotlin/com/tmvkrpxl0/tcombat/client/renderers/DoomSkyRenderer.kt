package com.tmvkrpxl0.tcombat.client.renderers

import com.mojang.blaze3d.pipeline.RenderTarget
import com.mojang.blaze3d.pipeline.TextureTarget
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.util.Color
import net.minecraft.client.Minecraft
import net.minecraft.world.item.Items
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.EventBusSubscriber
import net.neoforged.neoforge.client.event.RenderLevelStageEvent
import org.joml.Matrix4f
import org.joml.Vector3f

@EventBusSubscriber(Dist.CLIENT, modid = MODID)
object DoomSkyRenderer {
    var currentSky = Color(0, 0, 0, 0)

    @SubscribeEvent
    fun afterCloud(event: RenderLevelStageEvent) {
        if (event.stage != RenderLevelStageEvent.Stage.AFTER_SKY) {
            return
        }

        val minecraft = Minecraft.getInstance()
        if (!minecraft.player!!.inventory.items.any { it.`is`(Items.NETHER_STAR) }) {
            return
        }
        val type = minecraft.level!!.dimensionType()
        val height = (type.logicalHeight + type.minY).toFloat()

        val buffers = minecraft.renderBuffers().bufferSource()
        val buffer = buffers.getBuffer(DoomSkyRenderType.instance)

        DoomSkyRenderType.COLOR_UNIFORM.set(1F, 0F, 0.0F, 1F)

        val length = 1000.0F
        val a = Vector3f(-length, height, -length)
        val b = Vector3f( length, height, -length)
        val c = Vector3f( length, height,  length)
        val d = Vector3f(-length, height,  length)
        val projection = Matrix4f(event.modelViewMatrix)
        projection.translate(0F, -minecraft.player!!.y.toFloat(), 0F)

        arrayOf(a, b, c, d).forEach {
            buffer.addVertex(projection, it.x, it.y, it.z)
        }

        buffers.endBatch(DoomSkyRenderType.instance)
    }
}