package com.tmvkrpxl0.tcombat.common.enchantment

import com.mojang.serialization.MapCodec
import net.minecraft.server.level.ServerLevel
import net.minecraft.world.entity.Entity
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.item.BowItem
import net.minecraft.world.item.CrossbowItem
import net.minecraft.world.item.enchantment.EnchantedItemInUse
import net.minecraft.world.item.enchantment.effects.EnchantmentEntityEffect
import net.minecraft.world.phys.Vec3

object AutoReloadEffect: EnchantmentEntityEffect {
    override fun codec(): MapCodec<AutoReloadEffect> = MapCodec.unit(this)

    override fun apply(
        level: ServerLevel,
        enchantmentLevel: Int,
        itemContext: EnchantedItemInUse,
        entity: Entity,
        origin: Vec3
    ) {
        if (entity !is LivingEntity) return
        val remainingUseDuration = entity.useItemRemainingTicks

        val pulledMax = when (itemContext.itemStack.item) {
            is BowItem -> {
                remainingUseDuration > 20
            }

            is CrossbowItem -> {
                remainingUseDuration == 0
            }

            else -> false
        }

        if (!pulledMax) return

        entity.releaseUsingItem()
    }
}