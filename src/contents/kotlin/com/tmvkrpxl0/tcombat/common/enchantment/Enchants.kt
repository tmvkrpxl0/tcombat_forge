package com.tmvkrpxl0.tcombat.common.enchantment

import com.mojang.serialization.Codec
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.core.component.DataComponentType
import net.minecraft.core.registries.Registries
import net.minecraft.tags.TagKey
import net.minecraft.world.item.enchantment.ConditionalEffect
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.registries.DeferredRegister
import thedarkcolour.kotlinforforge.neoforge.forge.getValue
import java.util.function.Supplier

private val ENCHANTMENTS = DeferredRegister.create(Registries.ENCHANTMENT, MODID)
private val EFFECTS = DeferredRegister.create(Registries.ENCHANTMENT_ENTITY_EFFECT_TYPE, MODID)
private val EFFECT_COMPONENTS = DeferredRegister.create(Registries.ENCHANTMENT_EFFECT_COMPONENT_TYPE, MODID)

val AUTO_RELOAD_EFFECT by EFFECTS.register("auto_reload", AutoReloadEffect::codec)
val AUTO_RELOAD_COMPONENT by EFFECT_COMPONENTS.register("auto_reload", Supplier {
    DataComponentType.builder<List<ConditionalEffect<AutoReloadEffect>>>()
        .persistent(
            ConditionalEffect.codec(
                Codec.unit(AutoReloadEffect),
                LootContextParamSets.FISHING
            ).listOf()
        )
        .build()
})

fun registerEnchants(bus: IEventBus) {
    ENCHANTMENTS.register(bus)

}
