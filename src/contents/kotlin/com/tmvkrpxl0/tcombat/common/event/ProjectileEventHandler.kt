package com.tmvkrpxl0.tcombat.common.event

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.util.testSweptAABB
import com.tmvkrpxl0.tcombat.common.entity.projectile.ReflectiveArrow
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.server.level.ServerLevel
import net.minecraft.world.InteractionHand
import net.minecraft.world.damagesource.DamageTypes
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.player.Player
import net.minecraft.world.entity.projectile.SpectralArrow
import net.minecraft.world.item.ItemStack
import net.minecraft.world.item.Items.GLOWSTONE
import net.minecraft.world.level.block.RespawnAnchorBlock
import net.minecraft.world.phys.BlockHitResult
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.Event
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.EventBusSubscriber
import net.neoforged.neoforge.common.util.FakePlayerFactory
import net.neoforged.neoforge.event.entity.EntityTeleportEvent.EnderPearl
import net.neoforged.neoforge.event.entity.ProjectileImpactEvent
import net.neoforged.neoforge.event.entity.living.LivingDamageEvent
import net.neoforged.neoforge.event.entity.player.CriticalHitEvent
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.minus

@EventBusSubscriber(modid = MODID, bus = EventBusSubscriber.Bus.GAME, value = [Dist.CLIENT, Dist.DEDICATED_SERVER])
object ProjectileEventHandler {

    @SubscribeEvent
    fun onSpectralArrowHit(event: ProjectileImpactEvent) {
        if (event.entity !is SpectralArrow) return
        if (event.entity.level().isClientSide) return

        val arrow = event.entity as SpectralArrow
        val result = event.rayTraceResult

        if (result !is BlockHitResult) return
        if (arrow.owner !is Player) return

        val blockPos = result.blockPos
        val level = arrow.level() as ServerLevel
        val blockState = level.getBlockState(blockPos)

        if (blockState.block is RespawnAnchorBlock) {
            val fakePlayer = FakePlayerFactory.getMinecraft(level)
            val glowstone = ItemStack(GLOWSTONE, 1)
            fakePlayer.teleportTo(blockPos.x.toDouble(), blockPos.y.toDouble(), blockPos.z.toDouble())
            fakePlayer.setItemInHand(InteractionHand.MAIN_HAND, glowstone)

            blockState.useItemOn(glowstone, level, fakePlayer, InteractionHand.MAIN_HAND, result)
            arrow.kill()
        }
    }


    private var enderPearlSweeping = false

    @SubscribeEvent
    fun onEnderPearlHit(event: EnderPearl) {
        val from = event.player.boundingBox
        val to = event.player.boundingBox.move(event.target - event.prev)

        val moving = event.entity.boundingBox
        val entities = event.entity.level().getEntities(event.entity, from.minmax(to).inflate(event.player.boundingBox.size)) {
            if (it is LivingEntity) {
                val toTest = it
                val fakeVelocity = event.target - event.prev - it.deltaMovement

                val static = toTest.boundingBox

                val time = testSweptAABB(moving, static, fakeVelocity)

                0.0 <= time && time < 1.0
            } else false
        }
        enderPearlSweeping = true
        entities.forEach {
            event.player.attack(it)
            val center = it.boundingBox.center
            event.player.serverLevel().sendParticles(ParticleTypes.SWEEP_ATTACK, center.x, center.y, center.z, 1, 0.0, 0.0, 0.0, 0.7)
        }
        enderPearlSweeping = false
    }

    @SubscribeEvent
    fun onEnderPearlSweep(event: CriticalHitEvent) {
        if (enderPearlSweeping) {
            event.isCriticalHit = true
            event.damageMultiplier = 1.5F
        }
    }

    @SubscribeEvent
    fun onReflectiveArrowHit(event: LivingDamageEvent.Pre) {
        if (!event.source.`is`(DamageTypes.ARROW)) return
        val e = event.source.directEntity
        if (e !is ReflectiveArrow) return

        event.container.newDamage += e.reflectCount
    }
}