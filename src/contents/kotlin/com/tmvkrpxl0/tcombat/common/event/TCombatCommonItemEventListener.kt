package com.tmvkrpxl0.tcombat.common.event

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.item.MOBILE_DISPENSER
import com.tmvkrpxl0.tcombat.common.item.misc.MobileDispenserItem
import net.minecraft.world.effect.MobEffects
import net.minecraft.world.item.BowItem
import net.minecraft.world.item.CrossbowItem
import net.neoforged.api.distmarker.Dist
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.common.EventBusSubscriber
import net.neoforged.neoforge.event.ItemAttributeModifierEvent
import net.neoforged.neoforge.event.entity.living.LivingEntityUseItemEvent
import java.lang.Integer.min

@EventBusSubscriber(
    modid = MODID, bus = EventBusSubscriber.Bus.GAME, value = [Dist.CLIENT, Dist.DEDICATED_SERVER]
)
object TCombatCommonItemEventListener {
    @SubscribeEvent
    fun onUseTick(event: LivingEntityUseItemEvent.Start) {
        if (event.item.item is CrossbowItem || event.item.item is BowItem) {
            if (event.entity.hasEffect(MobEffects.DAMAGE_BOOST)) {
                val effectInstance = event.entity.getEffect(MobEffects.DAMAGE_BOOST)
                val amplifier = min(effectInstance!!.amplifier, 4)
                event.duration -= 3 * amplifier
            }
            if (event.entity.hasEffect(MobEffects.DIG_SPEED)) {
                val effectInstance = event.entity.getEffect(MobEffects.DIG_SPEED)
                val amplifier = min(effectInstance!!.amplifier, 4)
                event.duration -= 3 * amplifier
            }
        }
    }

    @SubscribeEvent
    fun onAttributeModify(event: ItemAttributeModifierEvent) {
        when (event.itemStack.item) {
            MOBILE_DISPENSER -> MobileDispenserItem.onAttributeModify(event)
        }
    }
}