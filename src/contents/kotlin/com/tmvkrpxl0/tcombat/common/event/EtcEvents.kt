package com.tmvkrpxl0.tcombat.common.event

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.attachments.INSTANT_ARROW
import com.tmvkrpxl0.tcombat.common.entity.miscs.GroundChunk
import com.tmvkrpxl0.tcombat.common.entity.projectile.TNTArrow
import com.tmvkrpxl0.tcombat.common.item.misc.MobileDispenserItem
import com.tmvkrpxl0.tcombat.common.skill.ArrowSense
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.player.Inventory
import net.minecraft.world.entity.player.Player
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.BucketItem
import net.minecraft.world.level.material.FlowingFluid
import net.minecraft.world.phys.EntityHitResult
import net.neoforged.bus.api.SubscribeEvent
import net.neoforged.fml.LogicalSide
import net.neoforged.fml.common.EventBusSubscriber
import net.neoforged.neoforge.common.SoundActions
import net.neoforged.neoforge.event.ItemStackedOnOtherEvent
import net.neoforged.neoforge.event.entity.ProjectileImpactEvent
import net.neoforged.neoforge.event.entity.living.LivingSwapItemsEvent
import net.neoforged.neoforge.event.entity.player.PlayerEvent
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent
import net.neoforged.neoforge.event.level.ExplosionEvent
import net.neoforged.neoforge.server.ServerLifecycleHooks

@EventBusSubscriber(modid = MODID, bus = EventBusSubscriber.Bus.GAME)
internal object EtcEvents {
    private fun cleanup(player: Player) {
        val server = ServerLifecycleHooks.getCurrentServer() ?: return
        server.allLevels.forEach { serverWorld ->
            serverWorld.allEntities.forEach { entity ->
                when (entity) {
                    is GroundChunk -> {
                        if (player.uuid == entity.owner) {
                            entity.discard()
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    fun discardEntitiesOnLogout(event: PlayerEvent.PlayerLoggedOutEvent) {
        cleanup(event.entity)
    }

    @SubscribeEvent
    fun discardEntities(event: PlayerEvent.PlayerChangedDimensionEvent) {
        cleanup(event.entity)
    }

    @SubscribeEvent
    fun resetInvulnerableTime(event: ProjectileImpactEvent) {
        if (event.entity !is AbstractArrow) return
        val arrow = event.entity as AbstractArrow
        if (arrow.level().isClientSide) return
        if (!arrow.hasData(INSTANT_ARROW) || !arrow.getData(INSTANT_ARROW)) return

        val r = event.rayTraceResult
        if (r is EntityHitResult) {
            r.entity.invulnerableTime = 0
        }
    }

    @SubscribeEvent
    fun onLeakFluid(event: PlayerInteractEvent.LeftClickBlock) {
        if (event.side == LogicalSide.CLIENT) return
        val stack = event.itemStack
        if (stack.item !is BucketItem) return
        if (event.face == null) return

        val level = event.level
        val blockPos = event.pos.offset(event.face!!.normal)
        val blockState = level.getBlockState(blockPos)
        val bucketItem = stack.item as BucketItem
        var fluid = bucketItem.content
        if (fluid !is FlowingFluid) return
        fluid = fluid.flowing

        if (blockState.isAir || blockState.canBeReplaced(fluid)) {
            level.setBlockAndUpdate(blockPos, fluid.defaultFluidState().createLegacyBlock())

            val soundEvent =
                fluid.fluidType.getSound(event.entity, level, blockPos, SoundActions.BUCKET_EMPTY)
                    ?: if (fluid.fluidType.temperature >= 1000) SoundEvents.BUCKET_EMPTY_LAVA else SoundEvents.BOTTLE_EMPTY
            level.playSound(event.entity, blockPos, soundEvent, SoundSource.PLAYERS, 1F, 1F)
        }
    }

    @SubscribeEvent
    fun excludeFromAffectedList(event: ExplosionEvent.Detonate) {
        val source = event.explosion.directSourceEntity
        if (source is TNTArrow) {
            event.affectedEntities.remove(source.owner)
        }
    }

    @SubscribeEvent
    fun removeAlertWhenLeaving(event: PlayerEvent.PlayerLoggedOutEvent) {
        ArrowSense.alerting.remove(event.entity.uuid)
    }

    @SubscribeEvent
    fun disallowMB(event: ItemStackedOnOtherEvent) {
        if (event.stackedOnItem.item is MobileDispenserItem && event.slot.container is Inventory && event.slot.containerSlot == 40) {
            event.isCanceled = true
        }
    }

    @SubscribeEvent
    fun disallowMBSwap(event: LivingSwapItemsEvent.Hands) {
        if (event.itemSwappedToOffHand.item is MobileDispenserItem) {
            event.isCanceled = true
        }
    }
}