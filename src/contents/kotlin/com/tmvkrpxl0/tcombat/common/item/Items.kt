package com.tmvkrpxl0.tcombat.common.item

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.item.misc.MobileDispenserItem
import com.tmvkrpxl0.tcombat.common.item.projectile.IronArrowItem
import com.tmvkrpxl0.tcombat.common.item.projectile.ReflectiveArrowItem
import com.tmvkrpxl0.tcombat.common.item.projectile.SnipeArrowItem
import com.tmvkrpxl0.tcombat.common.item.projectile.TNTArrowItem
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.CreativeModeTabs
import net.minecraft.world.item.Item
import net.minecraft.world.level.ItemLike
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent
import net.neoforged.neoforge.registries.DeferredItem
import net.neoforged.neoforge.registries.DeferredRegister
import thedarkcolour.kotlinforforge.neoforge.forge.getValue
import java.util.function.Supplier

private val ITEMS = DeferredRegister.createItems(MODID)
private val arrows = arrayListOf<ItemLike>()

val TNT_ARROW: ArrowItem by registerArrow("tnt_arrow") { TNTArrowItem }
val SNIPE_ARROW: ArrowItem by registerArrow("snipe_arrow") { SnipeArrowItem }
val REFLECTIVE_ARROW: ArrowItem by registerArrow("reflective_arrow") { ReflectiveArrowItem }
val IRON_ARROW: ArrowItem by registerArrow("iron_arrow") { IronArrowItem }

val MOBILE_DISPENSER: Item by ITEMS.register("mobile_dispenser", Supplier { MobileDispenserItem })

fun registerItems(bus: IEventBus) {
    ITEMS.register(bus)
}

fun arrows(): ArrayList<ItemLike> {
    return arrows
}

fun buildContents(event: BuildCreativeModeTabContentsEvent) {
    when (event.tabKey) {
        CreativeModeTabs.COMBAT -> {
            event.accept(MOBILE_DISPENSER)
            arrows().forEach(event::accept)
        }
    }
}

private fun <T: ArrowItem> registerArrow(name: String, supplier: Supplier<T>): DeferredItem<T> {
    val holder = ITEMS.register(name, supplier)
    arrows += holder
    return holder
}
