package com.tmvkrpxl0.tcombat.common.item.projectile

import com.tmvkrpxl0.tcombat.common.entity.TNT_ARROW
import com.tmvkrpxl0.tcombat.common.entity.projectile.IronArrow
import com.tmvkrpxl0.tcombat.common.entity.projectile.TNTArrow
import net.minecraft.core.Direction
import net.minecraft.core.Position
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.entity.projectile.Projectile
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.Level

object TNTArrowItem : ArrowItem(Properties()) {

    override fun createArrow(level: Level, stack: ItemStack, shooter: LivingEntity, weaponItem: ItemStack?): AbstractArrow =
        TNTArrow(level, shooter, stack, weaponItem)

    override fun asProjectile(level: Level, pos: Position, stack: ItemStack, direction: Direction): Projectile {
        val arrow = TNTArrow(level, pos.x(), pos.y(), pos.z(), stack.copyWithCount(1), null)
        arrow.pickup = AbstractArrow.Pickup.ALLOWED
        return arrow
    }
}