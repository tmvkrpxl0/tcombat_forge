package com.tmvkrpxl0.tcombat.common.item.misc

import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import com.tmvkrpxl0.tcombat.common.attachments.INSTANT_ARROW
import com.tmvkrpxl0.tcombat.common.attachments.MOBILE_DISPENSER_INVENTORY
import com.tmvkrpxl0.tcombat.common.container.MobileDispenserInventory
import com.tmvkrpxl0.tcombat.common.container.MobileDispenserMenu
import com.tmvkrpxl0.tcombat.mixins.bindings.common.DispenserAccessor
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.client.multiplayer.ClientLevel
import net.minecraft.core.Direction
import net.minecraft.core.NonNullList
import net.minecraft.core.dispenser.ProjectileDispenseBehavior
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.InteractionHand
import net.minecraft.world.InteractionResultHolder
import net.minecraft.world.MenuProvider
import net.minecraft.world.entity.EquipmentSlotGroup
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.ai.attributes.AttributeModifier
import net.minecraft.world.entity.ai.attributes.Attributes
import net.minecraft.world.entity.player.Inventory
import net.minecraft.world.entity.player.Player
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.Item
import net.minecraft.world.item.ItemStack
import net.minecraft.world.item.Items
import net.minecraft.world.level.ClipContext
import net.minecraft.world.level.Level
import net.minecraft.world.level.block.Blocks
import net.minecraft.world.phys.shapes.CollisionContext
import net.neoforged.neoforge.event.ItemAttributeModifierEvent
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.plus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.times

object MobileDispenserItem : Item(Properties().stacksTo(1)) {
    override fun onUseTick(level: Level, livingEntity: LivingEntity, stack: ItemStack, tick: Int) {
        val handler = stack.inventory
        val toDispense = handler
            .items
            .mapIndexed { index, itemStack -> index to itemStack }
            .filter { !it.second.isEmpty && isProjectile(it.second) }
            .randomOrNull()
        if (toDispense == null) {
            livingEntity.releaseUsingItem()
            return
        }

        if (level.isServerSide) {
            val eye = livingEntity.eyePosition
            val spawnAt = eye + (livingEntity.lookAngle * 2.0)
            val clipContext = ClipContext(
                livingEntity.eyePosition,
                spawnAt,
                ClipContext.Block.COLLIDER,
                ClipContext.Fluid.NONE,
                CollisionContext.empty()
            )
            val result = level.clip(clipContext)
            if (spawnAt != result.location) {
                if (level is ClientLevel) {
                    level.playLocalSound(
                        result.location.x, result.location.y, result.location.z,
                        SoundEvents.DISPENSER_FAIL,
                        SoundSource.PLAYERS,
                        1F, 1.2F,
                        true
                    )
                }
                return
            }

            val behavior = (Blocks.DISPENSER as DispenserAccessor).accessDispenseMethod(
                level,
                toDispense.second
            ) as ProjectileDispenseBehavior
            val copied = toDispense.second.copy()

            val projectileEntity = behavior.projectileItem.asProjectile(level, spawnAt, toDispense.second, Direction.DOWN)
            val projectilePower = 2F * behavior.dispenseConfig.power()
            val lookAngle = livingEntity.lookAngle
            projectileEntity.shoot(
                lookAngle.x, lookAngle.y, lookAngle.z, projectilePower, 0F
            )

            projectileEntity.owner = livingEntity
            projectileEntity.setData(INSTANT_ARROW, true)

            level.addFreshEntity(projectileEntity)

            if (!(livingEntity is Player && livingEntity.abilities.instabuild)) {
                copied.shrink(1)
                handler.setStackInSlot(toDispense.first, copied)
            }
        } else {
            val clientLevel = level as ClientLevel
            val pos = livingEntity.eyePosition.add(livingEntity.lookAngle)

            clientLevel.playLocalSound(
                pos.x,
                pos.y,
                pos.z,
                SoundEvents.DISPENSER_LAUNCH,
                SoundSource.PLAYERS,
                1F,
                1F,
                true
            )
            clientLevel.playLocalSound(
                pos.x,
                pos.y,
                pos.z,
                SoundEvents.REDSTONE_TORCH_BURNOUT,
                SoundSource.BLOCKS,
                0.5f,
                2.6f,
                true
            )
            clientLevel.addParticle(ParticleTypes.SMOKE, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0)
        }

    }

    override fun use(level: Level, player: Player, usedHand: InteractionHand): InteractionResultHolder<ItemStack> {
        val stack = player.getItemInHand(usedHand)
        val dispenserInventory = stack.inventory
        if (player.isUsingItem) return InteractionResultHolder.pass(stack)

        if (player.isShiftKeyDown) {
            if (level.isServerSide) player.openMenu(object : MenuProvider {
                override fun createMenu(
                    windowId: Int,
                    inventory: Inventory,
                    player: Player
                ) = MobileDispenserMenu(windowId, inventory, dispenserInventory)

                override fun getDisplayName() = stack.displayName

            })
            return InteractionResultHolder.pass(stack)
        }

        val found = dispenserInventory.items.any { !it.isEmpty && isProjectile(it) }

        return if (found) {
            player.startUsingItem(usedHand)
            level.playSound(null, player, SoundEvents.LEVER_CLICK, SoundSource.PLAYERS, 0.5F, 0.6F)
            InteractionResultHolder.consume(stack)
        } else {
            level.playSound(null, player, SoundEvents.DISPENSER_FAIL, SoundSource.PLAYERS, 1F, 1.2F)
            InteractionResultHolder.fail(stack)
        }
    }

    override fun getUseDuration(mobileDispenser: ItemStack, entity: LivingEntity): Int {
        return 72000
    }

    override fun releaseUsing(pStack: ItemStack, level: Level, entity: LivingEntity, timeCharged: Int) {
        if (level.isClientSide) {
            val pos = entity.position()
            entity.level().playLocalSound(
                pos.x, pos.y, pos.z, SoundEvents.LEVER_CLICK, SoundSource.PLAYERS, 0.5F, 0.5F, true
            )
        }
    }

    private val HEAVY_WEIGHT = "heavy_weight".toResource()

    fun onAttributeModify(event: ItemAttributeModifierEvent) {
        val slow = AttributeModifier(
            HEAVY_WEIGHT,
            -0.04,
            AttributeModifier.Operation.ADD_VALUE
        )
        event.addModifier(Attributes.MOVEMENT_SPEED, slow, EquipmentSlotGroup.MAINHAND)
        event.addModifier(Attributes.MOVEMENT_SPEED, slow, EquipmentSlotGroup.OFFHAND)
    }

    fun isProjectile(projectile: ItemStack): Boolean = projectile.item is ArrowItem

    private val ItemStack.inventory: MobileDispenserInventory
        get() {
            var data = this.get(MOBILE_DISPENSER_INVENTORY)
            if (data == null) {
                data = MobileDispenserInventory(NonNullList.withSize(9, ItemStack(Items.AIR)))
                this.set(MOBILE_DISPENSER_INVENTORY, data)
            }

            return data
        }
}