package com.tmvkrpxl0.tcombat.common.item.projectile

import com.tmvkrpxl0.tcombat.common.entity.SNIPE_ARROW
import com.tmvkrpxl0.tcombat.common.entity.projectile.SnipeArrow
import net.minecraft.core.Direction
import net.minecraft.core.Position
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.entity.projectile.Projectile
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.Level

object SnipeArrowItem : ArrowItem(Properties()) {
    override fun createArrow(level: Level, stack: ItemStack, shooter: LivingEntity, weaponItem: ItemStack?): AbstractArrow =
        SnipeArrow(level, shooter, stack, weaponItem)

    override fun asProjectile(level: Level, pos: Position, stack: ItemStack, direction: Direction): Projectile {
        val arrow = SnipeArrow(level, pos.x(), pos.y(), pos.z(), stack.copyWithCount(1), null)
        arrow.pickup = AbstractArrow.Pickup.CREATIVE_ONLY
        return arrow
    }
}
