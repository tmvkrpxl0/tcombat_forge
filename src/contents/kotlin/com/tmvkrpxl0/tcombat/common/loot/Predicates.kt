package com.tmvkrpxl0.tcombat.common.loot

import com.mojang.serialization.MapCodec
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.advancements.critereon.ItemPredicate
import net.minecraft.core.registries.Registries
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.storage.loot.LootContext
import net.minecraft.world.level.storage.loot.parameters.LootContextParam
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition
import net.minecraft.world.level.storage.loot.predicates.LootItemConditionType
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.registries.DeferredRegister
import thedarkcolour.kotlinforforge.neoforge.forge.getValue
import java.util.function.Supplier

val AMMO = LootContextParam<ItemStack>("ammo".toResource())

private val LOOT_CONDITION_TYPES = DeferredRegister.create(Registries.LOOT_CONDITION_TYPE, MODID)
val AMMO_CONDITION: LootItemConditionType by LOOT_CONDITION_TYPES.register("ammo", Supplier {
    LootItemConditionType(AmmoCondition.CODEC)
})

fun registerLootConditions(bus: IEventBus) {
    LOOT_CONDITION_TYPES.register(bus)
}

data class AmmoCondition(val predicate: ItemPredicate?): LootItemCondition {
    companion object {
        val CODEC = MapCodec.assumeMapUnsafe(ItemPredicate.CODEC).xmap(::AmmoCondition, AmmoCondition::predicate)
    }

    override fun test(context: LootContext): Boolean {
        val ammo = context.getParamOrNull(AMMO) ?: return false
        if (predicate == null) return true

        return predicate.test(ammo)
    }

    override fun getType(): LootItemConditionType {
        return AMMO_CONDITION
    }

    class Builder(val predicate: ItemPredicate.Builder?): LootItemCondition.Builder {
        override fun build(): AmmoCondition {
            return AmmoCondition(predicate?.build())
        }
    }
}