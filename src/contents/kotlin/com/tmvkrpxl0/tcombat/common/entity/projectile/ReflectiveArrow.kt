package com.tmvkrpxl0.tcombat.common.entity.projectile

import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import com.tmvkrpxl0.tcombat.base.common.util.reflect
import com.tmvkrpxl0.tcombat.common.attachments.INSTANT_ARROW
import com.tmvkrpxl0.tcombat.common.entity.REFLECTIVE_ARROW
import net.minecraft.commands.arguments.EntityAnchorArgument
import net.minecraft.network.RegistryFriendlyByteBuf
import net.minecraft.network.syncher.EntityDataAccessor
import net.minecraft.network.syncher.EntityDataSerializers
import net.minecraft.network.syncher.SynchedEntityData
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.ClipContext
import net.minecraft.world.level.Level
import net.minecraft.world.phys.BlockHitResult
import net.minecraft.world.phys.EntityHitResult
import net.minecraft.world.phys.HitResult
import net.minecraft.world.phys.Vec3
import net.neoforged.neoforge.entity.IEntityWithComplexSpawn
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.plus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.times
import com.tmvkrpxl0.tcombat.common.item.REFLECTIVE_ARROW as REFLECTIVE_ARROW_ITEM

class ReflectiveArrow : AbstractArrow, IEntityWithComplexSpawn {
    private var totalDistance = 0.0
    var reflectCount = 0
        private set
    private var maxDistance: Float = 0.0F

    private var from: Vec3? = null

    companion object {
        val STOP: EntityDataAccessor<Boolean> = SynchedEntityData.defineId(ReflectiveArrow::class.java, EntityDataSerializers.BOOLEAN)
    }

    constructor(entityType: EntityType<ReflectiveArrow>, level: Level): super(entityType, level)
    constructor(level: Level,
                owner: LivingEntity,
                pickupItemStack: ItemStack,
                firedFromWeapon: ItemStack?
    ):  super(
        REFLECTIVE_ARROW, owner, level, pickupItemStack, firedFromWeapon)

    constructor(
        level: Level,
        x: Double,
        y: Double,
        z: Double,
        pickupItemStack: ItemStack,
        firedFromWeapon: ItemStack?
    ): super(REFLECTIVE_ARROW, x, y, z, level, pickupItemStack, firedFromWeapon)

    constructor(
        shooter: LivingEntity,
        maxDistance: Float,
        level: Level,
        itemStack: ItemStack = ItemStack(REFLECTIVE_ARROW_ITEM),
        firedFrom: ItemStack? = null
    ) : super(REFLECTIVE_ARROW, shooter, level, itemStack, firedFrom) {
        this.maxDistance = maxDistance
        this.from = shooter.eyePosition
    }

    init {
        if (this.level().isServerSide) {
            setData(INSTANT_ARROW, true)
        }
    }

    override fun defineSynchedData(builder: SynchedEntityData.Builder) {
        builder.define(STOP, false)
        super.defineSynchedData(builder)
    }

    private fun hasStopped(): Boolean = this.entityData.get(STOP)

    private fun stop() {
        require(this.level().isServerSide) { "Reflective Arrow should never be stopped on client side!" }
        this.entityData.set(STOP, true)
        this.isNoGravity = false
    }

    override fun tick() {
        if (this.level().isServerSide) {
            if (isInWater) stop()

            if (from == null) {
                from = position()
            }

            totalDistance += from!!.distanceTo(position())
            from = position()
            if (totalDistance >= maxDistance) stop()
        }
        if (!hasStopped()) {
            deltaMovement = deltaMovement.scale(1 / 0.999)
        }
        super.tick()
    }

    override fun onHitBlock(result: BlockHitResult) {
        if (!hasStopped() && level().isServerSide) {
            if (!isInWater) {
                if (totalDistance < maxDistance) {
                    reflect(result)
                    return
                }
            }
        }
        super.onHitBlock(result)
    }

    override fun onHitEntity(result: EntityHitResult) {
        if (!hasStopped() && level().isServerSide) {
            if (result.entity == owner || (result.entity is LivingEntity && (result.entity as LivingEntity).isBlocking)) {
                deltaMovement *= -1.0
                return
            }
        }
        super.onHitEntity(result)
    }

    override fun getDefaultPickupItem(): ItemStack = ItemStack(REFLECTIVE_ARROW_ITEM)

    private fun reflect(blockHitResult: BlockHitResult) {
        var tempResult = blockHitResult
        while (tempResult.type == HitResult.Type.BLOCK && this.totalDistance < maxDistance) {
            reflectCount++
            level().playSound(null, this, SoundEvents.ARROW_HIT, SoundSource.NEUTRAL, 1F, 1F)
            val normal = tempResult.direction.normal
            val surface = Vec3(normal.x.toDouble(), normal.y.toDouble(), normal.z.toDouble())
            val reflected = this.deltaMovement.reflect(surface)
            deltaMovement = reflected
            totalDistance += from!!.distanceTo(tempResult.location)
            from = tempResult.location
            setPos(tempResult.location)

            val position = position()
            val next = position + deltaMovement
            lookAt(EntityAnchorArgument.Anchor.EYES, reflected)
            val entityHitResult = findHitEntity(position, next)
            if (entityHitResult?.entity != null) {
                onHitEntity(entityHitResult)
                break
            }
            tempResult = level().clip(ClipContext(position, next, ClipContext.Block.COLLIDER, ClipContext.Fluid.NONE, this))
        }
        markHurt()
    }

    override fun writeSpawnData(buffer: RegistryFriendlyByteBuf) {
        buffer.writeFloat(maxDistance)
    }

    override fun readSpawnData(buffer: RegistryFriendlyByteBuf) { //Already being read on Custom Client factory
        maxDistance = buffer.readFloat()
    }
}
