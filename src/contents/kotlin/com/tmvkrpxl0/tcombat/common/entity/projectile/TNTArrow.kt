package com.tmvkrpxl0.tcombat.common.entity.projectile

import com.tmvkrpxl0.tcombat.base.common.util.isServerSide
import com.tmvkrpxl0.tcombat.common.entity.TNT_ARROW
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.Level
import net.minecraft.world.phys.EntityHitResult
import net.minecraft.world.phys.HitResult
import net.minecraft.world.phys.Vec3
import com.tmvkrpxl0.tcombat.common.item.TNT_ARROW as TNT_ARROW_ITEM

class TNTArrow: AbstractArrow {
    private var explode = true
    private var explodeOn: Vec3? = null

    constructor(entityType: EntityType<TNTArrow>, level: Level): super(entityType, level)
    constructor(level: Level,
                owner: LivingEntity,
                pickupItemStack: ItemStack,
                firedFromWeapon: ItemStack?
    ):  super(
        TNT_ARROW, owner, level, pickupItemStack, firedFromWeapon)

    constructor(
        level: Level,
        x: Double,
        y: Double,
        z: Double,
        pickupItemStack: ItemStack,
        firedFromWeapon: ItemStack?
    ): super(TNT_ARROW, x, y, z, level, pickupItemStack, firedFromWeapon)

    override fun tick() {
        val level = level()
        if (level.isServerSide && explodeOn != null) {
            val location = explodeOn!!
            level.explode(
                this, location.x, location.y, location.z, (if (this.isCritArrow) 4 else 2).toFloat(), this.isOnFire, Level.ExplosionInteraction.BLOCK
            )
            this.explodeOn = null
            this.discard()
        }
        super.tick()
        if (this.isInWaterRainOrBubble) explode = false
        if (explode) {
            if (this.level().isClientSide) {
                val lookVec = this.lookAngle.normalize().scale(0.35)
                level.addParticle(
                    ParticleTypes.SMOKE, this.x - lookVec.x, this.y - lookVec.y + 0.15, this.z - lookVec.z, 0.0, 0.0, 0.0
                )
            }
        }
    }

    override fun onHit(result: HitResult) {
        if (!(result.type == HitResult.Type.ENTITY && this.owner != null && this.owner == (result as EntityHitResult).entity)) {
            if (!level().isClientSide && explode && !this.isInWaterRainOrBubble) {
                explodeOn = result.location
            }
        }
        super.onHit(result)
    }

    override fun remove(pReason: RemovalReason) {
        if (this.explodeOn == null) super.remove(pReason)
    }

    override fun getDefaultPickupItem() = ItemStack(TNT_ARROW_ITEM)
}