package com.tmvkrpxl0.tcombat.common.entity.projectile

import com.tmvkrpxl0.tcombat.common.entity.IRON_ARROW
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.Level
import com.tmvkrpxl0.tcombat.common.item.IRON_ARROW as IRON_ARROW_ITEM

class IronArrow: AbstractArrow {
    constructor(entityType: EntityType<IronArrow>, level: Level): super(entityType, level)
    constructor(level: Level,
                owner: LivingEntity,
                pickupItemStack: ItemStack,
                firedFromWeapon: ItemStack?
    ):  super(
        IRON_ARROW, owner, level, pickupItemStack, firedFromWeapon)

    constructor(
        level: Level,
        x: Double,
        y: Double,
        z: Double,
        pickupItemStack: ItemStack,
        firedFromWeapon: ItemStack?
    ): super(IRON_ARROW, x, y, z, level, pickupItemStack, firedFromWeapon)

    override fun getDefaultPickupItem() = ItemStack(IRON_ARROW_ITEM)
}