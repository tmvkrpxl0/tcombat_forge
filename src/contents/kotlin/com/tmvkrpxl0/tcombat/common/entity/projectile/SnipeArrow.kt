package com.tmvkrpxl0.tcombat.common.entity.projectile


import com.tmvkrpxl0.tcombat.common.entity.SNIPE_ARROW
import net.minecraft.core.BlockPos
import net.minecraft.core.particles.BlockParticleOption
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.LivingEntity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.entity.projectile.ProjectileUtil
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.ClipContext
import net.minecraft.world.level.Level
import net.minecraft.world.phys.AABB
import net.minecraft.world.phys.BlockHitResult
import net.minecraft.world.phys.HitResult
import net.minecraft.world.phys.Vec3
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.plus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.times
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.toVec3i
import com.tmvkrpxl0.tcombat.common.item.SNIPE_ARROW as SNIPE_ARROW_ITEM

class SnipeArrow: AbstractArrow {
    constructor(entityType: EntityType<SnipeArrow>, level: Level): super(entityType, level)
    constructor(level: Level,
                owner: LivingEntity,
                pickupItemStack: ItemStack,
                firedFromWeapon: ItemStack?
    ):  super(
        SNIPE_ARROW, owner, level, pickupItemStack, firedFromWeapon)

    constructor(
        level: Level,
        x: Double,
        y: Double,
        z: Double,
        pickupItemStack: ItemStack,
        firedFromWeapon: ItemStack?
    ): super(SNIPE_ARROW, x, y, z, level, pickupItemStack, firedFromWeapon)

    init {
        pickup = Pickup.CREATIVE_ONLY
    }

    override fun getDefaultPickupItem() = ItemStack(SNIPE_ARROW_ITEM)

    override fun shoot(x: Double, y: Double, z: Double, velocity: Float, inaccuracy: Float) {
        val start = position()
        var current = start
        val direction = Vec3(x, y, z).normalize()
        val end = current + (direction * 100.0)
        deltaMovement = direction * velocity.toDouble()

        val entityResult = ProjectileUtil.getEntityHitResult(
            level(),
            this,
            start,
            end,
            AABB.encapsulatingFullBlocks(BlockPos(start.toVec3i()), BlockPos(end.toVec3i())).inflate(1.0)
        ) { entity -> canHitEntity(entity) }
        val distanceSq = entityResult?.location?.distanceToSqr(start) ?: Double.POSITIVE_INFINITY
        var pos = end

        while (true) {
            val clipContext = ClipContext(current, end, ClipContext.Block.COLLIDER, ClipContext.Fluid.NONE, this)
            val result = level().clip(clipContext)
            if (result.type == HitResult.Type.MISS) {
                break
            }
            if (result.location.distanceToSqr(start) >= distanceSq) {
                pos = result.location
                onHitEntity(entityResult!!)
                break
            }

            if (level().getBlockState(result.blockPos).block.defaultDestroyTime() <= 0.3F) {
                level().destroyBlock(result.blockPos, false, this)
                current = result.location
            } else {
                pos = result.location
                break
            }
        }

        setPos(pos)
    }

    override fun onHitBlock(result: BlockHitResult) {
        val state = this.level().getBlockState(result.blockPos)
        val location = result.location
        val normal = result.direction.opposite.normal

        val effectLocation = location + Vec3.atLowerCornerOf(normal)
        val level = level()

        level.playSound(
            null, effectLocation.x, effectLocation.y, effectLocation.z, state.getSoundType(level, result.blockPos, this).breakSound, SoundSource.MASTER, 1F, 1F
        )

        if (level.isClientSide) {
            val particleDirection = deltaMovement.normalize().scale(2.0)
            val blockParticle = BlockParticleOption(ParticleTypes.BLOCK, state)
            repeat(128) {
                level.addParticle(
                    blockParticle,
                    true,
                    effectLocation.x,
                    effectLocation.y,
                    effectLocation.z,
                    particleDirection.x,
                    particleDirection.y,
                    particleDirection.z
                )
            }
        }

        super.onHitBlock(result)
    }
}
