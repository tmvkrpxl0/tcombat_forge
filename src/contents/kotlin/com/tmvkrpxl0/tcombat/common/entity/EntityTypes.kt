package com.tmvkrpxl0.tcombat.common.entity

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.entity.miscs.GroundChunk
import com.tmvkrpxl0.tcombat.common.entity.projectile.IronArrow
import com.tmvkrpxl0.tcombat.common.entity.projectile.ReflectiveArrow
import com.tmvkrpxl0.tcombat.common.entity.projectile.SnipeArrow
import com.tmvkrpxl0.tcombat.common.entity.projectile.TNTArrow
import net.minecraft.core.registries.Registries
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.MobCategory
import net.minecraft.world.level.Level
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.registries.DeferredRegister
import thedarkcolour.kotlinforforge.neoforge.forge.getValue
import java.util.function.Supplier

private val ENTITY_TYPES = DeferredRegister.create(Registries.ENTITY_TYPE, MODID)

val GROUND_CHUNK: EntityType<GroundChunk> by ENTITY_TYPES.register("ground_chunk", Supplier {
    EntityType.Builder.of(::GroundChunk, MobCategory.MISC).noSave().build("ground_chunk")
})

val TNT_ARROW: EntityType<TNTArrow> by ENTITY_TYPES.register("tnt_arrow", Supplier {
    EntityType.Builder.of(::TNTArrow, MobCategory.MISC).sized(0.5f, 0.5f).build("tnt_arrow")
})

val SNIPE_ARROW: EntityType<SnipeArrow> by ENTITY_TYPES.register("snipe_arrow", Supplier {
    EntityType.Builder.of({ snipeArrowType: EntityType<SnipeArrow>, level: Level ->
        SnipeArrow(snipeArrowType, level)
    }, MobCategory.MISC).sized(0.5f, 0.5f).build("snipe_arrow")
})

val IRON_ARROW: EntityType<IronArrow> by ENTITY_TYPES.register("iron_arrow", Supplier {
    EntityType.Builder.of(::IronArrow, MobCategory.MISC).sized(0.5f, 0.5f).build("iron_arrow")
})

val REFLECTIVE_ARROW: EntityType<ReflectiveArrow> by ENTITY_TYPES.register("reflective_arrow", Supplier {
    EntityType.Builder.of(::ReflectiveArrow, MobCategory.MISC).sized(0.5f, 0.5f).build("reflective_arrow")
})

fun registerEntities(bus: IEventBus) {
    ENTITY_TYPES.register(bus)
}