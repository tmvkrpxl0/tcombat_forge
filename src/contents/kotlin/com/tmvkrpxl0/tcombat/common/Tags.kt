package com.tmvkrpxl0.tcombat.common

import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.core.registries.Registries
import net.minecraft.tags.TagKey

val RANGED = TagKey.create(Registries.ITEM, "enchantable/ranged".toResource())