package com.tmvkrpxl0.tcombat.common.network

import com.tmvkrpxl0.tcombat.base.common.util.Color
import com.tmvkrpxl0.tcombat.base.common.util.TCombatDataSerializers
import com.tmvkrpxl0.tcombat.client.renderers.DoomSkyRenderer
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.core.registries.Registries
import net.minecraft.network.codec.ByteBufCodecs
import net.minecraft.network.codec.StreamCodec
import net.minecraft.network.protocol.common.custom.CustomPacketPayload
import net.minecraft.network.protocol.common.custom.CustomPacketPayload.Type
import net.minecraft.world.level.Level
import net.neoforged.neoforge.network.handling.IPayloadContext

class DoomSkyUpdatePacket(val level: Level, val color: Color): CustomPacketPayload {
    companion object {
        val type = Type<DoomSkyUpdatePacket>("doom_sky_update".toResource())
        val codec = StreamCodec.composite(
            ByteBufCodecs.registry(Registries.DIMENSION), DoomSkyUpdatePacket::level,
            TCombatDataSerializers.COLOR.codec(), DoomSkyUpdatePacket::color,
            ::DoomSkyUpdatePacket
        )
    }


    fun handle(context: IPayloadContext) {
        DoomSkyRenderer.currentSky = color
    }

    override fun type() = type
}