package com.tmvkrpxl0.tcombat.common.network

import com.tmvkrpxl0.tcombat.MODID
import net.neoforged.neoforge.network.event.RegisterPayloadHandlersEvent

internal fun registerPackets(event: RegisterPayloadHandlersEvent) {
    val registrar = event.registrar(MODID)

    registrar.playToClient(DoomSkyUpdatePacket.type, DoomSkyUpdatePacket.codec, DoomSkyUpdatePacket::handle)
}