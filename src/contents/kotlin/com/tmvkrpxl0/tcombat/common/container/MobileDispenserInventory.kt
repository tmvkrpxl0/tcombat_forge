package com.tmvkrpxl0.tcombat.common.container

import com.tmvkrpxl0.tcombat.common.item.misc.MobileDispenserItem
import net.minecraft.core.NonNullList
import net.minecraft.world.item.ItemStack
import net.neoforged.neoforge.items.ItemStackHandler


data class MobileDispenserInventory(val items: NonNullList<ItemStack>) : ItemStackHandler(items) {
    init {
        require(items.size == 9)
        items
    }

    override fun isItemValid(slot: Int, stack: ItemStack) = MobileDispenserItem.isProjectile(stack)
}