package com.tmvkrpxl0.tcombat.common.container

import net.minecraft.world.entity.player.Inventory
import net.minecraft.world.entity.player.Player
import net.minecraft.world.inventory.AbstractContainerMenu
import net.minecraft.world.inventory.MenuType
import net.minecraft.world.inventory.Slot
import net.minecraft.world.item.ItemStack
import net.neoforged.neoforge.items.IItemHandler
import net.neoforged.neoforge.items.SlotItemHandler

class MobileDispenserMenu(
    containerId: Int,
    inventory: Inventory,
    handler: IItemHandler
) : AbstractContainerMenu(MenuType.GENERIC_3x3, containerId) {
    init {
        for (y in 0 until 3) {
            for (x in 0 until 3) {
                addSlot(SlotItemHandler(handler, x + y * 3, 62 + x * 18, 17 + y * 18))
            }
        }

        for (k in 0 until 3) {
            for (i1 in 0 until 9) {
                addSlot(Slot(inventory, i1 + k * 9 + 9, 8 + i1 * 18, 84 + k * 18))
            }
        }

        for (l in 0 until 9) {
            addSlot(Slot(inventory, l, 8 + l * 18, 142))
        }
    }

    override fun stillValid(player: Player): Boolean {
        return player.isAlive
    }

    override fun quickMoveStack(player: Player, index: Int): ItemStack {
        var backup = ItemStack.EMPTY
        val slot = slots[index]
        if (slot.hasItem()) {
            val slotItem = slot.item
            backup = slotItem.copy()
            if (index < 9) {
                if (!moveItemStackTo(slotItem, 9, 45, true)) {
                    return ItemStack.EMPTY
                }
            } else if (!moveItemStackTo(slotItem, 0, 9, false)) {
                return ItemStack.EMPTY
            }
            if (slotItem.isEmpty) {
                slot.set(ItemStack.EMPTY)
            } else {
                slot.setChanged()
            }
            if (slotItem.count == backup.count) {
                return ItemStack.EMPTY
            }
            slot.onTake(player, slotItem)
        }
        return backup
    }
}