package com.tmvkrpxl0.tcombat.common.datagen

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.RANGED
import com.tmvkrpxl0.tcombat.common.item.arrows
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.core.HolderLookup
import net.minecraft.core.registries.Registries
import net.minecraft.data.PackOutput
import net.minecraft.data.tags.ItemTagsProvider
import net.minecraft.tags.ItemTags
import net.minecraft.tags.TagKey
import net.minecraft.world.item.Items
import net.minecraft.world.level.block.Block
import net.neoforged.neoforge.common.data.ExistingFileHelper
import java.util.concurrent.CompletableFuture

class TcombatTagsProvider(
    output: PackOutput,
    holders: CompletableFuture<HolderLookup.Provider>,
    tags: CompletableFuture<TagLookup<Block>>,
    existingFileHelper: ExistingFileHelper?
) : ItemTagsProvider(output, holders, tags, MODID, existingFileHelper) {
    override fun addTags(lookupProvider: HolderLookup.Provider) {
        val arrowTags = tag(ItemTags.ARROWS)
        arrows().map { it.asItem() }.forEach(arrowTags::add)

        val rangedTags = tag(RANGED)
        rangedTags.add(Items.ARROW, Items.CROSSBOW)
    }
}