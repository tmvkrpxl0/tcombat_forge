package com.tmvkrpxl0.tcombat.common.datagen

import com.tmvkrpxl0.tcombat.common.item.*
import net.minecraft.core.HolderLookup
import net.minecraft.data.PackOutput
import net.minecraft.data.recipes.RecipeCategory
import net.minecraft.data.recipes.RecipeOutput
import net.minecraft.data.recipes.RecipeProvider
import net.minecraft.data.recipes.ShapedRecipeBuilder
import net.minecraft.tags.ItemTags
import net.minecraft.world.item.ArrowItem
import net.minecraft.world.item.Item
import net.minecraft.world.item.Items
import java.util.concurrent.CompletableFuture

class ItemRecipeProvider(output: PackOutput, provider: CompletableFuture<HolderLookup.Provider>) : RecipeProvider(output, provider) {
    override fun buildRecipes(finishedRecipeConsumer: RecipeOutput) {
        arrow(finishedRecipeConsumer, TNT_ARROW, Items.TNT)
        arrow(finishedRecipeConsumer, SNIPE_ARROW, Items.REDSTONE)
        arrow(finishedRecipeConsumer, REFLECTIVE_ARROW, Items.SLIME_BALL)
        arrow(finishedRecipeConsumer, IRON_ARROW, Items.IRON_INGOT)

        ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, MOBILE_DISPENSER).define('s', Items.STONE).define('d', Items.DISPENSER).define('r', Items.REDSTONE)
            .define('t', Items.REDSTONE_TORCH).define('l', Items.STONE_SLAB)
            .define('f', ItemTags.WOODEN_FENCES).define('v', Items.LEVER).unlockedBy("has_dispenser", has(Items.DISPENSER))

            .pattern("lfv").pattern("drs").pattern("tst").save(finishedRecipeConsumer)
    }

    private fun arrow(finishedRecipeConsumer: RecipeOutput, resultArrow: ArrowItem, center: Item) {
        ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, resultArrow, 8)
            .define('a', Items.ARROW)
            .define('c', center)
            .pattern("aaa")
            .pattern("aca")
            .pattern("aaa")
            .unlockedBy("has_arrow", has(ItemTags.ARROWS))
            .save(finishedRecipeConsumer)
    }
}