package com.tmvkrpxl0.tcombat.common.datagen

import com.tmvkrpxl0.tcombat.common.RANGED
import com.tmvkrpxl0.tcombat.common.enchantment.AUTO_RELOAD_COMPONENT
import com.tmvkrpxl0.tcombat.common.enchantment.AutoReloadEffect
import com.tmvkrpxl0.tcombat.common.item.IRON_ARROW
import com.tmvkrpxl0.tcombat.common.loot.AmmoCondition
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.advancements.critereon.*
import net.minecraft.core.Holder
import net.minecraft.core.RegistrySetBuilder
import net.minecraft.core.Vec3i
import net.minecraft.core.registries.Registries
import net.minecraft.resources.ResourceKey
import net.minecraft.tags.BlockTags
import net.minecraft.tags.DamageTypeTags
import net.minecraft.world.entity.EquipmentSlotGroup
import net.minecraft.world.item.enchantment.Enchantment
import net.minecraft.world.item.enchantment.Enchantment.EnchantmentDefinition
import net.minecraft.world.item.enchantment.Enchantment.constantCost
import net.minecraft.world.item.enchantment.EnchantmentEffectComponents
import net.minecraft.world.item.enchantment.Enchantments
import net.minecraft.world.item.enchantment.LevelBasedValue
import net.minecraft.world.item.enchantment.LevelBasedValue.Clamped
import net.minecraft.world.item.enchantment.effects.AddValue
import net.minecraft.world.item.enchantment.effects.DamageImmunity
import net.minecraft.world.item.enchantment.effects.ReplaceDisk
import net.minecraft.world.level.block.Blocks
import net.minecraft.world.level.gameevent.GameEvent
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider
import net.minecraft.world.level.material.Fluids
import net.minecraft.world.level.storage.loot.LootContext
import net.minecraft.world.level.storage.loot.predicates.AnyOfCondition
import net.minecraft.world.level.storage.loot.predicates.DamageSourceCondition
import net.minecraft.world.level.storage.loot.predicates.InvertedLootItemCondition
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition
import java.util.*

fun generateEnchants() {
    val builder = RegistrySetBuilder()

    builder.add(Registries.ENCHANTMENT) { bootstrap ->
        val itemLookup = bootstrap.lookup(Registries.ITEM)
        val enchantLookup = bootstrap.lookup(Registries.ENCHANTMENT)
        val original = enchantLookup.getOrThrow(Enchantments.FROST_WALKER).unwrap().right().orElseThrow()

        val frostWalker = Enchantment
            .enchantment(original.definition)
            .exclusiveWith(original.exclusiveSet)
            .withEffect(
                EnchantmentEffectComponents.DAMAGE_IMMUNITY,
                DamageImmunity.INSTANCE,
                DamageSourceCondition.hasDamageSource(
                    DamageSourcePredicate.Builder.damageType()
                        .tag(TagPredicate.`is`(DamageTypeTags.BURN_FROM_STEPPING))
                        .tag(TagPredicate.isNot(DamageTypeTags.BYPASSES_INVULNERABILITY))
                )
            )
            .withEffect(
                EnchantmentEffectComponents.LOCATION_CHANGED,
                ReplaceDisk(
                    Clamped(LevelBasedValue.perLevel(3.0f, 1.0f), 0.0f, 16.0f),
                    LevelBasedValue.constant(1.0f),
                    Vec3i(0, -1, 0),
                    Optional.of(
                        BlockPredicate.allOf(
                            BlockPredicate.matchesTag(Vec3i(0, 1, 0), BlockTags.AIR),
                            BlockPredicate.matchesBlocks(Blocks.WATER),
                            BlockPredicate.matchesFluids(Fluids.WATER),
                            BlockPredicate.unobstructed()
                        )
                    ),
                    BlockStateProvider.simple(Blocks.FROSTED_ICE),
                    Optional.of<Holder<GameEvent>>(GameEvent.BLOCK_PLACE)
                ),
                AnyOfCondition.anyOf(
                    LootItemEntityPropertyCondition.hasProperties(
                        LootContext.EntityTarget.THIS,
                        EntityPredicate.Builder.entity().flags(EntityFlagsPredicate.Builder.flags().setOnGround(true)),
                    ),
                    LootItemEntityPropertyCondition.hasProperties(
                        LootContext.EntityTarget.THIS,
                        EntityPredicate.Builder.entity().vehicle(
                            EntityPredicate.Builder.entity()
                                .flags(EntityFlagsPredicate.Builder.flags().setOnGround(true))
                        )
                    )
                )
            ).build(Enchantments.FROST_WALKER.location())

        bootstrap.register(Enchantments.FROST_WALKER, frostWalker)
    }.add(Registries.ENCHANTMENT) { bootstrap ->
        val reference = bootstrap.lookup(Registries.ENCHANTMENT).getOrThrow(Enchantments.FLAME).unwrap()
        val original = reference.right().orElseThrow()
        val itemLookup = bootstrap.lookup(
            Registries.ITEM
        )

        val newDefinition = EnchantmentDefinition(
            itemLookup.getOrThrow(RANGED),
            original.definition.primaryItems,
            original.definition.weight,
            original.definition.maxLevel,
            original.definition.minCost,
            original.definition.maxCost,
            original.definition.anvilCost,
            original.definition.slots,
        )
        val newFlame = Enchantment(original.description, newDefinition, original.exclusiveSet, original.effects())
        bootstrap.register(Enchantments.FLAME, newFlame)
    }.add(Registries.ENCHANTMENT) { bootstrap ->
        val itemLookup = bootstrap.lookup(
            Registries.ITEM
        )

        val key = ResourceKey.create(Registries.ENCHANTMENT, "auto_reload".toResource())
        val autoReload = Enchantment.enchantment(
            Enchantment.definition(
                itemLookup.getOrThrow(RANGED),
                2,
                1,
                constantCost(20),
                constantCost(50),
                4,
                EquipmentSlotGroup.MAINHAND,
                EquipmentSlotGroup.OFFHAND
            )
        ).withEffect(AUTO_RELOAD_COMPONENT, AutoReloadEffect).build(key.location())

        bootstrap.register(key, autoReload)
    }.add(Registries.ENCHANTMENT) { bootstrap ->
        val reference = bootstrap.lookup(Registries.ENCHANTMENT).getOrThrow(Enchantments.MULTISHOT).unwrap()
        val original = reference.right().orElseThrow()

        val newMultiShot = Enchantment
            .enchantment(original.definition)
            .exclusiveWith(original.exclusiveSet)
            .withEffect(
                EnchantmentEffectComponents.PROJECTILE_COUNT,
                AddValue(LevelBasedValue.perLevel(2.0F))
            )
            .withEffect(
                EnchantmentEffectComponents.PROJECTILE_SPREAD,
                AddValue(LevelBasedValue.perLevel(10.0F)),
                InvertedLootItemCondition.invert(
                    AmmoCondition.Builder(
                        ItemPredicate.Builder.item().of(IRON_ARROW)
                    )
                )
            )

        bootstrap.register(Enchantments.MULTISHOT, newMultiShot.build(Enchantments.MULTISHOT.location()))
    }
}