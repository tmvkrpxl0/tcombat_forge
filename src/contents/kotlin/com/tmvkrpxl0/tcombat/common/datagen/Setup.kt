package com.tmvkrpxl0.tcombat.common.datagen

import com.tmvkrpxl0.tcombat.MODID
import net.minecraft.core.HolderLookup
import net.neoforged.neoforge.common.data.BlockTagsProvider
import net.neoforged.neoforge.data.event.GatherDataEvent

fun setupDatagen(event: GatherDataEvent) {
    val fileHelper = event.existingFileHelper
    val generator = event.generator
    val output = generator.packOutput
    val lookupProvider = event.lookupProvider

    val blocks = object: BlockTagsProvider(output, lookupProvider, MODID, fileHelper) {
        override fun addTags(pProvider: HolderLookup.Provider) {}
    }
    // Server
    val blockProvider = generator.addProvider(event.includeServer(), blocks)
    generator.addProvider(event.includeServer(), TcombatTagsProvider(output, lookupProvider, blockProvider.contentsGetter(), fileHelper))
    generator.addProvider(event.includeServer(), ItemRecipeProvider(output, lookupProvider))
    generateEnchants()

    // Client
    generator.addProvider(event.includeClient(), MobileDispenserModelProvider(output, fileHelper))
}
