package com.tmvkrpxl0.tcombat.common.datagen

import com.google.common.base.Preconditions
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.tmvkrpxl0.tcombat.toResource
import net.neoforged.neoforge.client.model.generators.CustomLoaderBuilder
import net.neoforged.neoforge.client.model.generators.ModelBuilder
import net.neoforged.neoforge.common.data.ExistingFileHelper
import org.joml.Vector3f
import java.util.*

internal class StructureModelBuilder<T : ModelBuilder<T>> (
    parent: T,
    existingFileHelper: ExistingFileHelper
): CustomLoaderBuilder<T>("structure".toResource(), parent, existingFileHelper, false) {
    private val structure = LinkedList<Pair<TransformEntry, T>>()
    fun add(transform: TransformEntry, model: T) {
        Preconditions.checkArgument(model != this, "Self reference is forbidden!")
        structure.push(transform to model)
    }

    override fun toJson(jsonObject: JsonObject): JsonObject {
        val json = super.toJson(jsonObject)
        val structureArray = JsonArray()
        structure.forEach {
            val structureEntry = JsonObject()
            val (transform, model) = it
            val transformJson = JsonObject()
            var added = false
            if (transform.rotation != null) {
                transformJson.add("rotation", transform.rotation.toJson())
                added = true
            }
            if (transform.translation != null) {
                transformJson.add("translation", transform.translation.toJson())
                added = true
            }
            if (transform.scale != null) {
                transformJson.add("scale", transform.scale.toJson())
                added = true
            }
            if (transform.postRotation != null) {
                transformJson.add("post-rotation", transform.postRotation.toJson())
                added = true
            }
            if (transform.origin != null) {
                transformJson.add("origin", transform.origin.toJson())
                added = true
            }
            if (added) structureEntry.add("transform", transformJson)
            structureEntry.add("model", model.toJson())
            structureArray.add(structureEntry)
        }
        json.add("structure", structureArray)

        return json
    }

    private fun Vector3f.toJson(): JsonElement {
        val ret = JsonArray()
        ret.add(x)
        ret.add(y)
        ret.add(z)
        return ret
    }

    data class TransformEntry(
        val rotation: Vector3f? = null,
        val translation: Vector3f? = null,
        val scale: Vector3f? = null,
        val postRotation: Vector3f? = null,
        val origin: Vector3f? = null
    )
}