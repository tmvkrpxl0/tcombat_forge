package com.tmvkrpxl0.tcombat.common.skill

import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.player.Player

object EffectStorm : Skill<Unit>() {
    override val skillName = "effect_storm".toResource()
    override val cooldownTick: Long = 20 * 60 * 5
    override fun canExecute(player: Player): Unit? = Unit

    override fun execute(player: ServerPlayer, extra: Unit) {
        // TODO It's dummy now
    }
}