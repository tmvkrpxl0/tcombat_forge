package com.tmvkrpxl0.tcombat.common.skill

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.skill.SKILL_REGISTRY
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.registries.DeferredRegister
import thedarkcolour.kotlinforforge.neoforge.forge.getValue
import java.util.function.Supplier

private val SKILLS: DeferredRegister<Skill<*>> = DeferredRegister.create(SKILL_REGISTRY, MODID)

val ARROW_SENSE: ArrowSense by SKILLS.register(ArrowSense.skillName.path, Supplier { ArrowSense })
val AUTO_BRIDGE: AutoBridge by SKILLS.register(AutoBridge.skillName.path, Supplier { AutoBridge })
val REFLECTION_BLAST: ReflectionBlast by SKILLS.register(ReflectionBlast.skillName.path, Supplier { ReflectionBlast })
val SHATTER_ARROW: ShatterArrow by SKILLS.register(ShatterArrow.skillName.path, Supplier { ShatterArrow })
val TEST_SKILL: TestSkill by SKILLS.register(TestSkill.skillName.path, Supplier { TestSkill })

fun registerSkills(bus: IEventBus) {
    SKILLS.register(bus)
}