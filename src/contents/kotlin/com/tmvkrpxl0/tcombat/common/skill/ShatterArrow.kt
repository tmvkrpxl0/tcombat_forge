package com.tmvkrpxl0.tcombat.common.skill

import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.skill.checkCooldown
import com.tmvkrpxl0.tcombat.base.common.skill.setSkillCooldown
import com.tmvkrpxl0.tcombat.mixins.bindings.common.ArrowAccessor
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.core.particles.ItemParticleOption
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.server.level.ServerPlayer
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.player.Player
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.phys.AABB

object ShatterArrow : Skill<List<AbstractArrow>>() {
    override val skillName = "shatter_arrow".toResource()

    override fun canExecute(player: Player): List<AbstractArrow>? {
        if (!checkCooldown(player, this)) {
            return null
        }
        if (player is ServerPlayer) {
            player.setSkillCooldown(this, cooldownTick)
        }

        val arrows = player.level().getEntities(
            player, scanRegion.move(player.position())
        ).asSequence()
            .filterIsInstance<AbstractArrow>()
            .filter { !(it as ArrowAccessor).isInGround || it.isPickable }
            .filter { it.distanceToSqr(player) <= (5 * 5) }
            .toList()


        return arrows.ifEmpty { null }
    }
    override val cooldownTick = 20L

    private val scanRegion = AABB(-15.0, -15.0, -15.0, 15.0, 15.0, 15.0)

    override fun execute(player: ServerPlayer, extra: List<AbstractArrow>) {
        extra.forEach { arrow ->
            val particleOption = ItemParticleOption(ParticleTypes.ITEM, (arrow as ArrowAccessor).pickupItemAccess())
            val serverLevel = player.serverLevel()

            serverLevel.sendParticles(
                particleOption,
                arrow.x, arrow.y, arrow.z,
                16,
                0.0, 0.0, 0.0,
                0.04
            )
            serverLevel.sendParticles(
                ParticleTypes.SWEEP_ATTACK,
                arrow.x, arrow.y, arrow.z,
                1,
                0.0, 0.0, 0.0,
                0.2
            )

            serverLevel.playSound(
                player,
                arrow,
                SoundEvents.SHIELD_BLOCK, SoundSource.PLAYERS,
                0.8F, 0.8F + serverLevel.random.nextFloat() * 0.4F
            )
            arrow.discard()
        }
    }
}