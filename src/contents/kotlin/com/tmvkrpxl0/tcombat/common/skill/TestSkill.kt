package com.tmvkrpxl0.tcombat.common.skill

import com.tmvkrpxl0.tcombat.base.common.skill.SimpleSkill
import com.tmvkrpxl0.tcombat.common.entity.miscs.GroundChunk
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.core.particles.BlockParticleOption
import net.minecraft.core.particles.ParticleTypes
import net.minecraft.server.level.ServerPlayer
import net.minecraft.sounds.SoundSource
import net.minecraft.world.phys.AABB
import net.minecraft.world.phys.Vec3
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.plus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.toVec3

object TestSkill : SimpleSkill() {
    override val skillName = "test".toResource()

    override fun execute(player: ServerPlayer) {
        val playerPos = player.blockPosition()
        val start = playerPos.offset(-5, -5, -5).toVec3()
        val end = playerPos.offset(5, -1, 5).toVec3()
        val blocks = AABB(start, end)
        val level = player.level()

        val ground = GroundChunk(player, blocks)
        ground.setPos(player.position().add(0.0, 3.0, 0.0))
        level.addFreshEntity(ground)
        /*ground.dissolve(4) { blockPos, state ->
            val pos = Vec3.atCenterOf(blockPos) + ground.position()
            player.serverLevel().playSound(null, pos.x, pos.y, pos.z, state.getSoundType(level, blockPos, ground).breakSound, SoundSource.BLOCKS)
            player.serverLevel().sendParticles(
                BlockParticleOption(ParticleTypes.BLOCK, state),
                pos.x, pos.y, pos.z,
                10,
                0.0, 0.0, 0.0,
                10.0
            )
        }*/
    }

}