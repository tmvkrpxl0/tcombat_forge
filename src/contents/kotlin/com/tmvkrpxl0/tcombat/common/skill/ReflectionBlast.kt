package com.tmvkrpxl0.tcombat.common.skill

import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.base.common.skill.Skill
import com.tmvkrpxl0.tcombat.base.common.skill.checkCooldown
import com.tmvkrpxl0.tcombat.common.entity.projectile.ReflectiveArrow
import com.tmvkrpxl0.tcombat.common.item.REFLECTIVE_ARROW
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.network.chat.Component
import net.minecraft.server.level.ServerPlayer
import net.minecraft.util.RandomSource
import net.minecraft.world.entity.player.Player
import net.minecraft.world.phys.Vec3
import kotlin.math.min

object ReflectionBlast : Skill<Int>() {
    override val skillName = "reflection_blast".toResource()

    override fun canExecute(player: Player): Int? {
        if (!checkCooldown(player, this)) {
            player.sendSystemMessage(Component.translatable("$MODID.skill.cooldown_fail", this))
            return null
        }

        val count = if (!player.isCreative) {
            min(16, player.inventory.countItem(REFLECTIVE_ARROW))
        } else {
            16
        }

        return if (count == 0) {
            player.sendSystemMessage(Component.translatable("$translationKey.count_arrow_fail"))
            null
        } else {
            count
        }
    }


    override val cooldownTick: Long = 200
    override fun execute(player: ServerPlayer, extra: Int) {
        val random = player.level().random
        val base = player.position().add(0.0, player.eyeHeight.toDouble(), 0.0).add(player.lookAngle)
        val delta = player.lookAngle.scale(1.5)
        for (i in 1..extra) {
            val arrow = ReflectiveArrow(player, 25F, player.level())
            arrow.owner = player
            val perpendicular = getPerpendicularRandom(delta, random).scale(random.nextDouble() * 10 - 5)
            val combined = base.add(perpendicular)
            arrow.setPos(combined.x, combined.y, combined.z)
            arrow.deltaMovement = delta
            player.level().addFreshEntity(arrow)
            for (item in player.inventory.items) {
                if (player.isCreative) break
                if (item.item == REFLECTIVE_ARROW) {
                    item.count--
                    break
                }
            }
        }
    }

    //v1.x*v2.x + v1.y*v2.y + v1.z*v2.z = 0
    //v1.z*v2.z = - v1.x*v2.x - v1.y*v2.y
    //v2.z = (- v1.x*v2.x - v1.y*v2.y) / v1.z
    private fun getPerpendicularRandom(v1: Vec3, random: RandomSource): Vec3 {
        val v2x = random.nextDouble() * v1.x
        val v2y = random.nextDouble() * v1.y
        val v2z = (-v2x - v2y) / v1.z
        return Vec3(v2x, v2y, v2z).normalize()
    }
}