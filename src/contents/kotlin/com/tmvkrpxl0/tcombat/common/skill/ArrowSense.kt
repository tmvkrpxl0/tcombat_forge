package com.tmvkrpxl0.tcombat.common.skill

import com.tmvkrpxl0.tcombat.base.common.skill.SimpleSkill
import com.tmvkrpxl0.tcombat.base.common.skill.alertPlayer
import com.tmvkrpxl0.tcombat.base.common.util.Color
import com.tmvkrpxl0.tcombat.mixins.bindings.common.ArrowAccessor
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.server.level.ServerPlayer
import net.minecraft.sounds.SoundEvents
import net.minecraft.sounds.SoundSource
import net.minecraft.world.entity.Entity
import net.minecraft.world.entity.projectile.AbstractArrow
import net.minecraft.world.phys.AABB
import java.util.*

object ArrowSense : SimpleSkill() {
    override val skillName = "arrow_sense".toResource()

    val alerting = HashSet<UUID>()

    private val size = AABB(-15.0, -15.0, -15.0, 15.0, 15.0, 15.0)

    override fun execute(player: ServerPlayer) {
        val axisAlignedBB = size.move(player.position())
        val list = player.level().getEntities(
            player, axisAlignedBB
        ) { arrow: Entity ->
            return@getEntities arrow is AbstractArrow && arrow.owner != player && arrow.distanceToSqr(player) < 15 * 15 && player.hasLineOfSight(arrow) && !(arrow as ArrowAccessor).isInGround
        }
        if (list.isNotEmpty()) {
            if (!alerting.contains(player.uuid)) {
                alertPlayer(player, Color(200.toByte(), 25, 25, 128.toByte()))
                alerting.add(player.uuid)
            }
            player.playNotifySound(SoundEvents.NOTE_BLOCK_BELL.value(), SoundSource.HOSTILE, 1f, 1.3f)
        } else {
            if (alerting.contains(player.uuid)) {
                alertPlayer(player, Color(0, 0, 0, 0))
                alerting.remove(player.uuid)
            }
        }
    }
}