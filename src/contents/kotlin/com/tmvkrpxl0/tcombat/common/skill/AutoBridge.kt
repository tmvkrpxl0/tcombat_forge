package com.tmvkrpxl0.tcombat.common.skill

import com.tmvkrpxl0.tcombat.base.common.skill.SimpleSkill
import com.tmvkrpxl0.tcombat.base.common.util.getValue
import com.tmvkrpxl0.tcombat.base.common.util.toRadian
import com.tmvkrpxl0.tcombat.toResource
import net.minecraft.core.BlockPos
import net.minecraft.server.level.ServerPlayer
import net.minecraft.world.entity.player.Player
import net.minecraft.world.item.BucketItem
import net.minecraft.world.level.block.Blocks
import net.minecraft.world.level.material.Fluids
import net.minecraft.world.phys.Vec3
import net.neoforged.neoforge.common.NeoForgeMod
import net.neoforged.neoforge.fluids.FluidType
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.plus
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.times
import thedarkcolour.kotlinforforge.neoforge.forge.vectorutil.v3d.toVec3i
import kotlin.math.cos
import kotlin.math.sin

private val WATER_TYPE: FluidType by NeoForgeMod.WATER_TYPE
private val LAVA_TYPE: FluidType by NeoForgeMod.LAVA_TYPE

object AutoBridge : SimpleSkill() {
    override val skillName = "auto_bridge".toResource()

    override fun canExecute(player: Player): Unit? {
        if (super.canExecute(player) == null) {
            return null
        }

        val mainHand = player.mainHandItem
        val offHand = player.offhandItem

        if (mainHand.item !is BucketItem) return null
        if (offHand.item !is BucketItem) return null

        val mainBucket = mainHand.item as BucketItem
        val offBucket = offHand.item as BucketItem

        val fluid1 = if (mainBucket.content.fluidType == WATER_TYPE) 1 else if (mainBucket.content.fluidType == LAVA_TYPE) 2 else 0
        val fluid2 = if (offBucket.content.fluidType == WATER_TYPE) 1 else if (offBucket.content.fluidType == WATER_TYPE) 2 else 0

        if (fluid1 + fluid2 != 3) {
            return null
        }

        return Unit
    }

    private fun horizontalViewVector(yaw: Float): Vec3 {
        val yawRadian = (-yaw).toRadian()
        val f2 = cos(yawRadian)
        val f3 = sin(yawRadian)
        return Vec3(f3.toDouble(), 0.0, f2.toDouble())
    }

    fun findPlacement(player: Player, isPreview: Boolean = false): BlockPos {
        val floor = player.blockPosition().mutable()
        floor.y -= 1

        if (player.level().getBlockState(floor).getCollisionShape(player.level(), floor).isEmpty) {
            floor.y -= 1
        }

        var center = floor.center
        var horizontalLook = horizontalViewVector(player.yRot)
        if (isPreview) {
            horizontalLook *= 2.0
        }

        if (player.xRot < -10) { // UP
            horizontalLook = horizontalLook.add(0.0, 1.0, 0.0)
        } else if (player.xRot > 80) { // DOWN
            horizontalLook = horizontalLook.add(0.0, -1.0, 0.0)
        }

        center += horizontalLook
        return BlockPos(center.toVec3i())
    }

    override fun execute(player: ServerPlayer) {
        val level = player.serverLevel()

        val placingPos = findPlacement(player)
        val blockState = level.getBlockState(placingPos)

        val above = placingPos.above()
        val aboveState = level.getBlockState(above)

        if (blockState.canBeReplaced(Fluids.FLOWING_WATER) && aboveState.canBeReplaced(Fluids.FLOWING_WATER)) {
            level.setBlockAndUpdate(placingPos, Blocks.COBBLESTONE.defaultBlockState())
            level.levelEvent(1501, placingPos, 0)
        }
    }
}