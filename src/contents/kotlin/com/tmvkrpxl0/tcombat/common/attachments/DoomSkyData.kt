package com.tmvkrpxl0.tcombat.common.attachments

import com.tmvkrpxl0.tcombat.base.common.util.Color
import net.minecraft.core.HolderLookup
import net.minecraft.nbt.CompoundTag
import net.minecraft.server.level.ServerLevel
import net.minecraft.world.level.saveddata.SavedData

var ServerLevel.doomSky: Color
    get() = dataStorage.computeIfAbsent(DoomSkyData.FACTORY, "doom_sky").color
    set(value) {
        val current = dataStorage.computeIfAbsent(DoomSkyData.FACTORY, "doom_sky")
        current.color = value
        current.setDirty()
    }

data class DoomSkyData(var color: Color): SavedData() {
    companion object {
        val FACTORY = Factory({ DoomSkyData(Color(0, 0, 0, 0) )}, { tag, provider ->
            val r = tag.getByte("r")
            val g = tag.getByte("g")
            val b = tag.getByte("b")
            val a = tag.getByte("a")

            DoomSkyData(Color(r, g, b, a))
        })
    }
    override fun save(tag: CompoundTag, registries: HolderLookup.Provider): CompoundTag {
        tag.putByte("r", color.r)
        tag.putByte("g", color.g)
        tag.putByte("b", color.b)
        tag.putByte("a", color.a)

        return tag
    }
}