package com.tmvkrpxl0.tcombat.common.attachments

import com.mojang.serialization.DataResult
import com.tmvkrpxl0.tcombat.MODID
import com.tmvkrpxl0.tcombat.common.container.MobileDispenserInventory
import net.minecraft.core.NonNullList
import net.minecraft.core.component.DataComponentType
import net.minecraft.network.codec.ByteBufCodecs
import net.minecraft.network.codec.StreamCodec
import net.minecraft.world.item.ItemStack
import net.neoforged.bus.api.IEventBus
import net.neoforged.neoforge.attachment.AttachmentType
import net.neoforged.neoforge.registries.DeferredRegister
import net.neoforged.neoforge.registries.NeoForgeRegistries
import java.util.*
import java.util.function.Supplier

// It's Init, innit?!

private val ATTACHMENTS = DeferredRegister.create(NeoForgeRegistries.ATTACHMENT_TYPES, MODID)
private val COMPONENTS = DeferredRegister.createDataComponents(MODID)

val MOBILE_DISPENSER_INVENTORY: Supplier<DataComponentType<MobileDispenserInventory>> =
    COMPONENTS.register("mobile_dispenser", Supplier {
        val codec = ItemStack.OPTIONAL_CODEC.listOf(9, 9).comapFlatMap(
            {
                try {
                    val items = NonNullList.createWithCapacity<ItemStack>(9)
                    it.forEach(items::add)
                    DataResult.success(MobileDispenserInventory(items))
                } catch (e: NullPointerException) {
                    DataResult.error(e::message)
                }
            },
            MobileDispenserInventory::items
        )
        val streamCodec = ItemStack.OPTIONAL_STREAM_CODEC
            .apply(ByteBufCodecs.collection { count ->
                require(count == 9)
                NonNullList.createWithCapacity(count)
            })
            .map(::MobileDispenserInventory, MobileDispenserInventory::items)
        DataComponentType.builder<MobileDispenserInventory>()
            .persistent(codec)
            .networkSynchronized(streamCodec)
            .build()
    })

val ENTITY_ID: Supplier<DataComponentType<OptionalInt>> = COMPONENTS.register("entity_holder", Supplier {
    DataComponentType
        .builder<OptionalInt>()
        .networkSynchronized(
            StreamCodec.of(
                { buffer, int -> int.ifPresent(buffer::writeInt) },
                { buffer -> if (buffer.isReadable) OptionalInt.of(buffer.readInt()) else OptionalInt.empty() }
            )
        )
        .build()
})

val INSTANT_ARROW: Supplier<AttachmentType<Boolean>> = ATTACHMENTS.register("instant_arrow", Supplier {
    AttachmentType.builder(Supplier { false }).build()
})

fun registerAttachments(bus: IEventBus) {
    ATTACHMENTS.register(bus)
    COMPONENTS.register(bus)
}