package com.tmvkrpxl0.tcombat

import com.tmvkrpxl0.tcombat.base.common.skill.tickAutoExecution
import com.tmvkrpxl0.tcombat.client.renderers.autoBridgePreview
import com.tmvkrpxl0.tcombat.client.renderers.registerShader
import com.tmvkrpxl0.tcombat.client.renderers.setAutoBridgePreview
import com.tmvkrpxl0.tcombat.common.attachments.registerAttachments
import com.tmvkrpxl0.tcombat.common.datagen.setupDatagen
import com.tmvkrpxl0.tcombat.common.enchantment.registerEnchants
import com.tmvkrpxl0.tcombat.common.entity.registerEntities
import com.tmvkrpxl0.tcombat.common.item.arrows
import com.tmvkrpxl0.tcombat.common.item.buildContents
import com.tmvkrpxl0.tcombat.common.item.misc.MobileDispenserItem
import com.tmvkrpxl0.tcombat.common.item.registerItems
import com.tmvkrpxl0.tcombat.common.loot.registerLootConditions
import com.tmvkrpxl0.tcombat.common.network.registerPackets
import com.tmvkrpxl0.tcombat.common.skill.registerSkills
import net.minecraft.resources.ResourceLocation
import net.minecraft.world.level.block.DispenserBlock
import net.neoforged.fml.common.Mod
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent
import net.neoforged.neoforge.client.extensions.common.RegisterClientExtensionsEvent
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import thedarkcolour.kotlinforforge.neoforge.forge.FORGE_BUS
import thedarkcolour.kotlinforforge.neoforge.forge.MOD_BUS

// The value here should match an entry in the META-INF/mods.toml file
internal const val MODID = "tcombat"
val LOGGER: Logger = LogManager.getLogger("tmvkrpxl0's combat mod")

@Mod(MODID)
object TCombatMain {
    init {
        registerSkills(MOD_BUS)
        registerEntities(MOD_BUS)
        registerItems(MOD_BUS)
        registerEnchants(MOD_BUS)
        registerAttachments(MOD_BUS)
        registerLootConditions(MOD_BUS)

        MOD_BUS.addListener(::setupDatagen)
        MOD_BUS.addListener(::registerPackets)
        MOD_BUS.addListener(::buildContents)
        MOD_BUS.addListener(::registerShader)
        MOD_BUS.addListener<FMLCommonSetupEvent> {
            arrows().forEach(DispenserBlock::registerProjectileBehavior)
        }

        FORGE_BUS.addListener(::tickAutoExecution)
        FORGE_BUS.addListener(::autoBridgePreview)
        FORGE_BUS.addListener(::setAutoBridgePreview)

        LOGGER.info("TCombat activated!")
    }
}

internal fun String.toResource() = ResourceLocation.fromNamespaceAndPath(MODID, this)