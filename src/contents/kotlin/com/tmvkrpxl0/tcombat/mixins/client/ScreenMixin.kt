package com.tmvkrpxl0.tcombat.mixins.client

import com.tmvkrpxl0.tcombat.client.fadeLevel
import net.minecraft.util.FastColor

fun modifyRenderBackground(): Pair<Int, Int> {
    val multiplier = 1f - fadeLevel / 10f
    val from = FastColor.ARGB32.color((multiplier * 192).toInt(), 16, 16, 16)
    val to = FastColor.ARGB32.color((multiplier * 208).toInt(), 16, 16, 16)
    return Pair(from, to)
}