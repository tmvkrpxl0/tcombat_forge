float weights[5] = float[](0.05, 0.25, 0.4, 0.25, 0.05);

vec4 mainImage(vec2 fragCoord, vec2 resolution) {
    // Get the current fragment's UV coordinates
    vec2 uv = fragCoord/resolution;

    // Calculate the UV coordinates of the 4 surrounding fragments
    vec2 uv0 = vec2(floor(uv.x), floor(uv.y));
    vec2 uv1 = vec2(floor(uv.x)+1.0, floor(uv.y));
    vec2 uv2 = vec2(floor(uv.x), floor(uv.y)+1.0);
    vec2 uv3 = vec2(floor(uv.x)+1.0, floor(uv.y)+1.0);

    // Sample the input texture at the 4 surrounding fragments
    vec4 color0 = texture(iChannel0, uv0);
    vec4 color1 = texture(iChannel0, uv1);
    vec4 color2 = texture(iChannel0, uv2);
    vec4 color3 = texture(iChannel0, uv3);

    // Calculate the weighted average of the samples
    vec4 color = vec4(
        weights[0] * color0.r +
        weights[1] * color1.r +
        weights[2] * color2.r +
        weights[3] * color3.r +
        weights[4] * color0.r,

        weights[0] * color0.g +
        weights[1] * color1.g +
        weights[2] * color2.g +
        weights[3] * color3.g +
        weights[4] * color0.g,

        weights[0] * color0.b +
        weights[1] * color1.b +
        weights[2] * color2.b +
        weights[3] * color3.b +
        weights[4] * color0.b,

        weights[0] * color0.a +
        weights[1] * color1.a +
        weights[2] * color2.a +
        weights[3] * color3.a +
        weights[4] * color0.a
    );

    return color;
}