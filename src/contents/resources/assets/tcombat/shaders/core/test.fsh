#version 150

uniform vec4 ColorModulator;
uniform vec4 Color;
uniform vec2 ScreenSize;

out vec4 fragColor;

void main() {
    vec4 color = Color;
    if (color.a == 0.0) {
        discard;
    }
    fragColor = color * ColorModulator;
}
