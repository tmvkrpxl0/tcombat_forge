package com.tmvkrpxl0.tcombat.mixins.bindings.client;

import com.tmvkrpxl0.tcombat.mixins.client.ScreenMixinKt;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.inventory.EffectRenderingInventoryScreen;
import net.minecraft.client.gui.screens.inventory.InventoryScreen;
import net.minecraft.client.gui.screens.recipebook.RecipeUpdateListener;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.InventoryMenu;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(InventoryScreen.class)
public abstract class InventoryScreenBinding extends EffectRenderingInventoryScreen<InventoryMenu> implements RecipeUpdateListener {

    public InventoryScreenBinding(InventoryMenu menu, Inventory playerInventory, Component title) {
        super(menu, playerInventory, title);
    }

    @Override
    public void renderTransparentBackground(GuiGraphics guiGraphics) {
        var values = ScreenMixinKt.modifyRenderBackground();
        int from = values.getFirst();
        int to = values.getSecond();

        guiGraphics.fillGradient(0, 0, this.width, this.height, from, to);
    }
}
