package com.tmvkrpxl0.tcombat.mixins.bindings.common;


import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(AbstractArrow.class)
public interface ArrowAccessor {

    @Accessor("inGround")
    boolean isInGround();

    @Invoker("getPickupItem")
    ItemStack pickupItemAccess();
}
