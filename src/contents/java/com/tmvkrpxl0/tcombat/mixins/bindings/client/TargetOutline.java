package com.tmvkrpxl0.tcombat.mixins.bindings.client;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.sugar.Local;
import com.tmvkrpxl0.tcombat.base.common.data.AttachmentsKt;
import com.tmvkrpxl0.tcombat.base.common.data.Target;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderBuffers;
import net.minecraft.world.entity.Entity;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

@Mixin(LevelRenderer.class)
public abstract class TargetOutline {

    @Shadow @Final private RenderBuffers renderBuffers;

    @Shadow @Final private Minecraft minecraft;

    @Unique
    private boolean isTarget;

    @ModifyExpressionValue(
            method = "renderLevel(Lnet/minecraft/client/DeltaTracker;ZLnet/minecraft/client/Camera;Lnet/minecraft/client/renderer/GameRenderer;Lnet/minecraft/client/renderer/LightTexture;Lorg/joml/Matrix4f;Lorg/joml/Matrix4f;)V",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/world/entity/Entity;hasCustomOutlineRendering(Lnet/minecraft/world/entity/player/Player;)Z")
    )
    private boolean addOutline(boolean original, @Local(ordinal = 0) Entity entity, @Local(ordinal = 0) MultiBufferSource.BufferSource source) {
        Target targetInfo = minecraft.player.getData(AttachmentsKt.getTARGET());
        isTarget = targetInfo.getTargets().contains(entity);
        boolean result = original || isTarget;
        if (isTarget) {
            if (entity.equals(targetInfo.getPrimary())) {
                renderBuffers.outlineBufferSource().setColor(0xFF, 0x0, 0x0, 0xFF);
            } else {
                renderBuffers.outlineBufferSource().setColor(0x0, 0xFF, 0xFF, 0xFF);
            }
        }
        return result;
    }

    @ModifyVariable(
            method = "renderLevel(Lnet/minecraft/client/DeltaTracker;ZLnet/minecraft/client/Camera;Lnet/minecraft/client/renderer/GameRenderer;Lnet/minecraft/client/renderer/LightTexture;Lorg/joml/Matrix4f;Lorg/joml/Matrix4f;)V",
            at = @At(value = "STORE", ordinal = 1)
    )
    private MultiBufferSource changeBuffer(MultiBufferSource original, @Local(ordinal = 3) boolean flag2) {
        if (flag2 && isTarget) {
            return renderBuffers.outlineBufferSource();
        } else {
            return original;
        }
    }
}
