@file:Suppress("PropertyName")

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    eclipse
    idea
    `maven-publish`
    `java-library`
    kotlin("jvm") version "1.9.20"
    id("net.neoforged.gradle.userdev") version "7.0.+"
}

version = project.properties["version"]!!
group = project.properties["group_id"]!!

val modid: String by project
val mod_name: String by project
val license: String by project
val mapping_channel: String by project
val mapping_version: String by project
val minecraft_version: String by project
val minecraft_version_range: String by project
val neo_version: String by project
val neo_version_range: String by project
val loader_version_range: String by project
val author: String by project
val pack_format_number: String by project

java.toolchain.languageVersion.set(JavaLanguageVersion.of(17))

println("감지된 자바 버전: ${System.getProperty("java.version")}")
println("JVM 버전: ${System.getProperty("java.vm.version")}(${System.getProperty("java.vendor")})")
println("아키텍쳐: ${System.getProperty("os.arch")}")

jarJar.enable()

minecraft.accessTransformers.file("src/main/resources/META-INF/accesstransformer.cfg")

runs {
    // applies to all the run configs below
    configureEach {
        // Recommended logging data for a userdev environment
        // The markers can be added/remove as needed separated by commas.
        // "SCAN": For mods scan.
        // "REGISTRIES": For firing of registry events.
        // "REGISTRYDUMP": For getting the contents of all registries.
        systemProperty("forge.logging.markers", "REGISTRIES")

        // Recommended logging level for the console
        // You can set various levels here.
        // Please read: https://stackoverflow.com/questions/2031163/when-to-use-the-different-log-levels
        systemProperty("forge.logging.console.level", "debug")

        modSource(sourceSets.main.get())
    }

    create("client") {
        // Comma-separated list of namespaces to load gametests from. Empty = all namespaces.
        systemProperty("forge.enabledGameTestNamespaces", modid)
    }

    create("server") {
        systemProperty("forge.enabledGameTestNamespaces", modid)
        programArgument("--nogui")
    }

    // This run config launches GameTestServer and runs all registered gametests, then exits.
    // By default, the server will crash when no gametests are provided.
    // The gametest system is also enabled by default for other run configs under the /test command.
    create("gameTestServer") {
        systemProperty("forge.enabledGameTestNamespaces", modid)
    }

    create("data") {
        // example of overriding the workingDirectory set in configureEach above, uncomment if you want to use it
        // workingDirectory project.file('run-data')

        // Specify the modid for data generation, where to output the resulting resource, and where to look for existing resources.
        programArguments.addAll(
            "--mod", modid,
            "--all",
            "--output", file("src/generated/resources/").absolutePath,
            "--existing", file("src/main/resources/").absolutePath
        )
    }
}

repositories {
    mavenLocal()
    maven {
        setUrl("https://www.cursemaven.com")
        content {
            includeGroup("curse.maven")
        }
    }

    maven(url = "https://repo.essential.gg/repository/maven-public")

    maven {
        name = "Kotlin for Forge"
        setUrl("https://thedarkcolour.github.io/KotlinForForge/")
    }
}

dependencies {
    // Use the latest version of Minecraft Forge
    implementation("net.neoforged:neoforge:${neo_version}")

    implementation("thedarkcolour:kotlinforforge-neoforge:4.6.1")
}


// Include assets and data from data generators
sourceSets.main.configure {
    resources.srcDirs("src/generated/resources/")
}

tasks {
    @Suppress("UnstableApiUsage")
    withType<ProcessResources>().configureEach {
        val replacements = mutableMapOf(
            "minecraft_version" to minecraft_version,
            "minecraft_version_range" to minecraft_version_range,
            "neo_version" to neo_version,
            "neo_version_range" to neo_version_range,
            "loader_version_range" to loader_version_range,
            "modid" to modid,
            "mod_name" to mod_name,
            "license" to license,
            "mod_version" to version as String,
            "mod_authors" to author,
            "description" to description,
            "pack_format_number" to pack_format_number
        )
        inputs.properties(replacements)

        filesMatching(listOf("META-INF/mods.toml", "pack.mcmeta")) {
            expand(replacements + ("project" to project))
        }
    }

    withType<JavaCompile>().configureEach {
        options.encoding = "UTF-8"
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "17"
    }
}
