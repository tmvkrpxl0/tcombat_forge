@file:Suppress("PropertyName", "UnstableApiUsage")

import org.gradle.configurationcache.extensions.capitalized
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "2.0.0"
    id("net.neoforged.moddev") version "1.0.15"
}

version = project.properties["version"]!!
group = project.properties["group_id"]!!

val license: String by project
val mapping_channel: String by project
val imgui_version: String by project
val minecraft_version: String by project
val neo_version: String by project
val author: String by project
val pack_format_number: String by project
val main_id: String by project
val main_name: String by project
val main_description: String by project
val contents_id: String by project
val contents_name: String by project
val contents_description: String by project
val kff_version: String by project
val kff_range: String by project
val java_version: String by project
val parchment_version: String by project
val parchment_date: String by project

java.toolchain {
    languageVersion.set(JavaLanguageVersion.of(java_version.toInt()))
}
val isJbr = java.toolchain.vendor == JvmVendorSpec.JETBRAINS

println("방가방가~")
println("JVM 버전: ${System.getProperty("java.vm.version")}(${System.getProperty("java.vendor")})")
println("아키텍쳐: ${System.getProperty("os.arch")}")

val main: SourceSet = sourceSets.main.get()
val contents: SourceSet = sourceSets.create("contents") {
    compileClasspath += main.compileClasspath + main.output
    runtimeClasspath += main.runtimeClasspath + main.output
}

neoForge {
    version = neo_version
    setAccessTransformers("src/contents/resources/META-INF/accesstransformer.cfg")
    validateAccessTransformers = true

    val baseMod = mods.create(main_id) {
        sourceSet(main)
    }
    val contentsMod = mods.create(contents_id) {
        sourceSet(contents)
    }

    runs {
        all {
            systemProperty("forge.logging.markers", "REGISTRIES")

            if (isJbr) {
                jvmArgument("-XX:+AllowEnhancedClassRedefinition")
            }

            systemProperty("forge.logging.console.level", "debug")
        }

        // 0/0/0   BASE/AUTH/ALT
        repeat(8) {
            val nameBuilder = StringBuilder()
            nameBuilder.append("client")

            val isAlt = it.and(0b001) != 0
            val isAuth = it.and(0b010) != 0
            val isBaseOnly = it.and(0b100) != 0

            if (isAlt) {
                nameBuilder.append("Alt")
            }
            if (isAuth) {
                nameBuilder.append("Auth")
            }
            if (isBaseOnly) {
                nameBuilder.append("Base")
            }

            create(nameBuilder.toString()) {
                client()

                if (isAlt) {
                    gameDirectory = file("runs/clientAlt")
                }

                if (isAuth) {
                    mainClass = "net.covers1624.devlogin.DevLogin"
                    programArguments.addAll("--launch_target", "cpw.mods.bootstraplauncher.BootstrapLauncher")
                }

                if (isBaseOnly) {
                    systemProperty("forge.enabledGameTestNamespaces", main_id)
                    sourceSet = main
                } else {
                    systemProperty("forge.enabledGameTestNamespaces", "$main_id,$contents_id")
                    sourceSet = contents
                }
            }
        }

        create("server") {
            server()
            systemProperty("forge.enabledGameTestNamespaces", "$main_id,$contents_id")
            sourceSet = contents
            programArgument("--nogui")
        }

        create("gameTestServer") {
            type = "gameTestServer"
            sourceSet = contents
            systemProperty("forge.enabledGameTestNamespaces", "$main_id,$contents_id")
        }

        create("data") {
            data()
            sourceSet = contents
            programArguments.addAll(
                "--mod", contents_id,
                "--all",
                "--output", file("src/generated/resources/").absolutePath,
                "--existing", file("src/main/resources/").absolutePath,
                "--existing", file("src/contents/resources/").absolutePath
            )
        }
    }

    parchment {
        if (minecraft_version != parchment_version) {
            println("Mapping params using older version of minecraft. Things may break.")
        }

        minecraftVersion = parchment_version
        mappingsVersion = parchment_date
    }
}

val localRuntime = configurations.create("localRuntime")
configurations {
    runtimeClasspath {
        extendsFrom(localRuntime)
    }
}

main.resources { srcDir("src/generated/resources") }
contents.resources { srcDir("src/generated/resources") }

repositories {
    mavenLocal()

    exclusiveContent {
        forRepository {
            maven {
                setUrl("https://cursemaven.com")
            }
        }
        filter {
            includeGroup("curse.maven")
        }
    }

    maven("https://maven.covers1624.net/")

    // TODO switch to this thing instead of imgui
    maven(url = "https://repo.essential.gg/repository/maven-public")

    maven {
        name = "Kotlin for Forge"
        setUrl("https://thedarkcolour.github.io/KotlinForForge/")
    }
    mavenCentral()
}

dependencies {
    implementation("thedarkcolour:kotlinforforge-neoforge:$kff_version")

    fun String.imgui(group: String, name: String, version: String) {
        this(group = group, name = name, version = version) {
            exclude(group = "org.lwjgl")
        }
    }

    arrayOf("binding", "lwjgl3", "natives-windows", "natives-macos", "natives-linux").forEach { libName ->
        val implementation = implementation("io.github.spair:imgui-java-$libName") {
            exclude(group = "org.lwjgl")
        }
        jarJar(implementation) {
            version {
                strictly("[1.0, 2.0)")
                prefer(imgui_version)
            }
        }

        val dependency = create("io.github.spair:imgui-java-$libName:$imgui_version")
        neoForge.runs.filter { it.name.contains("client", ignoreCase = true) }.forEach {
            it.additionalRuntimeClasspathConfiguration.dependencies.add(dependency)
        }
    }

    val devLogin = localRuntime("net.covers1624:DevLogin:0.1.0.3")
    neoForge.runs.filter {
        name.contains("auth", ignoreCase = true)
    }.forEach {
        it.additionalRuntimeClasspathConfiguration.dependencies.add(devLogin)
    }

    localRuntime("curse.maven:atlasviewer-633577:5490697")
}

val templateSubstitutions = mutableMapOf(
    "minecraft_version" to minecraft_version,
    "neo_version" to neo_version,
    "contents_id" to contents_id,
    "contents_name" to contents_name,
    "license" to license,
    "mod_version" to version as String,
    "mod_authors" to author,
    "contents_description" to contents_description,
    "pack_format_number" to pack_format_number,
    "main_id" to main_id,
    "main_name" to main_name,
    "main_description" to main_description,
    "kff_range" to kff_range,
    "java_version" to java_version,
    "project" to project.name,
)

sourceSets.forEach { sourceSet ->
    val sourceSetName = sourceSet.name.capitalized()
    val task = tasks.register<ProcessResources>("generate${sourceSetName}Metadata") {
        inputs.properties(templateSubstitutions)
        expand(templateSubstitutions)

        from("src/${sourceSet.name}/templates")
        into("build/generated/sources/${sourceSet.name}/modMetadata")
    }

    sourceSet.resources.srcDirs(task)
    neoForge.ideSyncTask(task)
}

tasks {
    withType<JavaCompile>().configureEach {
        options.encoding = "UTF-8"
    }

    withType<KotlinCompile> {
        compilerOptions.jvmTarget = JvmTarget.fromTarget(java_version)
    }
}

idea {
    module {
        isDownloadSources = true
        isDownloadJavadoc = true
    }
}
